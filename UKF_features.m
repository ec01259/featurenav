function [Est] = UKF_features(Est0, f, G, O, Measures, pars, units,...
    file_features, file_features_Mars)
%==========================================================================
% [Est] = UKF(Est0,f,G_dG,R,Measures,par)
%
% Unscented Kalman Filter
%
% INPUT: Description Units
%
% Est0      - Apriori initial guess of
%               .X,     States 
%               .P0,    Covariance matrix
%               .t0,    initial time  
% 
% @f        - Dynamical model
% 
% @G        - Observation's model and partial derivatives wrt the states
% 
% O         - Weight matrix
% Nsigma    - Number of sigma points
% Measures  - Observations matrix: the first column contains observation
%               times, then every column has a different type of observaion
%               type
% par       - Structure containing parameters required by @f and @G_dG 
% units     - Structure containing lenght and time units 
%
% OUTPUT:
%
% Est       - Estimation of:
%               .X, States at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .X_t, States at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 04/2021
%
%==========================================================================


%   Apriori initial guess, deviation and covariance matrix.
    Xold    = Est0.X;
    Pold    = Est0.P0;
    
%   Column of the measures time.
    told    = Measures(1).t;

%   The measures must be organized in colums depending on the type of
%   measure; each row is reffered to a given measure's time.
    obs         = Measures;
   
    
%   n,l and m are respectively the number of states, number of different
%   types of measures and number of measures per each type.
    n       = size(Xold,1);
    m       = size(obs,1);
    q       = 3;

%   err is the vector of residuals, the others are needed to build the 
%   fitting residuals.
    tobs    = NaN(1,m);
    err     = NaN(size(O,1),m);
    prefit  = NaN(size(O,1),m);
    y_t     = NaN(size(O,1),m);
    X_t     = zeros(n,size(Measures,1));
    x_t     = zeros(n,size(Measures,1));
    P_t     = zeros(n,n,m);
    Pbar_t  = zeros(n,n,m);
    
%   Initialization of the filter
    k       = 3 - n;
    alpha   = pars.alpha;
    beta    = pars.beta;
    lambda  = alpha^2*(n+k) - n;
    gamma   = sqrt(n+lambda);

%   Sigma points' weights
    W0c     = zeros(2*n+1, 1);
    W0c(1)  = lambda/(lambda+n)+(1-alpha^2+beta);
    W0c(2:2*n+1)   = 1./(2*(n+lambda));

    W0m     = zeros(2*n+1, 1);
    W0m(1)  = lambda/(lambda+n);
    W0m(2:2*n+1)   = W0c(2:2*n+1);
       
%   For-loop on the observations
    for i = 1:m
        
%       Useful quantities's initialization
        et  = Est0.t0/units.tsf + Measures(i).t;
        
%       TIME UPDATE
%       Sigma points' definition
        S0 = real(sqrtm(Pold)); 
        X0 = [Xold, (repmat(Xold,1,n)-gamma.*S0),...
            (repmat(Xold,1,n)+gamma.*S0)];

%       Need to know how many observations are available
        [Y_obs, ~, Stat_ID_Range, Stat_ID_Rate, check_Lidar, check_Limb,...
            check_Deimos, check_ISL, Features_ID, Features_Mars_ID, IDX] = ...
            Measurement_read(Measures(i),O,units);

        if Measures(i).t == told

%           The sigma point dont need to be proppagated
            Xbar    = X0;

        elseif Measures(i).t > told

%           Propagation of the sigma points' trajectories
            St0     = reshape(X0,[n*(2*n+1),1]);
            tspan   = (told:Measures(i).t)*units.tsf;
            opt     = odeset('RelTol',1e-13,'AbsTol',1e-16,'event',@(t,X) landing_Phobos(t,X,pars,units));
            [~,X_sigma] = ode113(@(t,X) f(t,X,pars,units),tspan,St0,opt);
            Xbar = reshape(X_sigma(end,1:n*(2*n+1))',[n,(2*n+1)]);

        end

        fprintf('\nOsservazione n.%d di %d', i, m);


%       Apriori quantities
        Xmean_bar   = sum(W0m'.*Xbar,2);
        deltaT  = (Measures(i).t-told)*units.tsf;
        Q       = pars.sigma^2*eye(3);
        switch n
            case 21
                Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
                Q = Gamma*Q*Gamma' + [zeros(6,n); zeros(n-6,6), pars.sigmaPh^2*...
                    diag([deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, zeros(1,n-12)])];  
            case 23
                Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
                Q = Gamma*Q*Gamma' + [zeros(6,n); zeros(n-6,6), pars.sigmaPh^2*...
                    diag([deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, zeros(1,n-14)])];  
        end
    
        P_bar   = Q;


        for j = 1:2*n+1
            P_bar   = P_bar + W0c(j)*((Xbar(:,j) - Xmean_bar) * (Xbar(:,j) - Xmean_bar)');
        end

%       Sigma point redefinition
        S_bar   = real(sqrtm(P_bar));
        X0  = [Xmean_bar, (repmat(Xmean_bar,1,n)-gamma.*S_bar),...
            (repmat(Xmean_bar,1,n)+gamma.*S_bar)];
        
        Y   = zeros(size(Y_obs.meas,1),2*n);
        Y_feat = cell(2*n);
        Y_feat_Mars = cell(2*n);

        lost_track_Phobos   = 0;
        lost_track_Mars = 0;

        for j = 1:2*n+1
%           G(j-th sigma point)
            [Y_sigma, ~] = G(et, X0(:,j), Stat_ID_Range, Stat_ID_Rate,...
                check_Lidar, check_Limb, check_Deimos, check_ISL, Features_ID, Features_Mars_ID,...
                pars, units, O, file_features, file_features_Mars);
            Y(:,j)          = Y_sigma.meas;
            Y_feat{j}       = Y_sigma.cam;
            Y_feat_Mars{j}  = Y_sigma.cam_Mars;
            
            if ~isempty(Y_feat{j})
                Features_ID = intersect(Features_ID, Y_feat{j}(3,:));
            elseif isempty(Y_feat_Mars{j})
                lost_track_Phobos   = 1;
            end
            
            if ~isempty(Y_feat_Mars{j})
                Features_Mars_ID = intersect(Features_Mars_ID, Y_feat_Mars{j}(3,:));
            elseif isempty(Y_feat_Mars{j})
                lost_track_Mars = 1;
            end

        end

        Y_sigma = [];

       if (~lost_track_Phobos)||(~lost_track_Mars)
            for j = 1:2*n
                features = features_list(Y_feat{j}, Features_ID);
                features_Mars = features_list(Y_feat_Mars{j}, Features_Mars_ID);
                Y_sigma  = [Y_sigma, [Y(:,j); features; features_Mars]];
            end
        else
            Y_sigma  = Y;
        end

       

%       Mean predicted measurement
        [Y_mean, R]  = G(et, Xmean_bar, Stat_ID_Range, Stat_ID_Rate,...
                check_Lidar, check_Limb, check_Deimos, check_ISL, Features_ID, Features_Mars_ID,...
                pars, units, O, file_features, file_features_Mars);
        
        features    = features_list(Y_mean.cam, Features_ID);
        features_Mars   = features_list(Y_mean.cam_Mars, Features_Mars_ID);
        Y_mean      = [Y_mean.meas; features; features_Mars];

        if ~isempty(features)&&(~isempty(Y_obs.cam))
            features    = features_list(Y_obs.cam, Features_ID);
        elseif isempty(features)&&(~isempty(Y_obs.cam))
            fprintf('\nTracking delle features su Phobos perso!!');
        end

        if ~isempty(features_Mars)&&(~isempty(Y_obs.cam_Mars))
            features_Mars   = features_list(Y_obs.cam_Mars, Features_Mars_ID);
        elseif isempty(features_Mars)&&(~isempty(Y_obs.cam_Mars))
            fprintf('\nTracking delle features su Marte perso!!');
        end

        Y_obs       = [Y_obs.meas; features; features_Mars];

%       Innovation covariance and cross covariance

        R       = [R; O(8,8)*ones(size(features,1),1); O(8,8)*ones(size(features_Mars,1),1)];
        Py      = diag(R);
        Pxy     = zeros(n,size(Y_obs,1));

        if size(R,1)>0

            for j = 1:2*n
                Py  = Py + W0c(j)*(Y_sigma(:,j) - Y_mean)*(Y_sigma(:,j) - Y_mean)';
                Pxy = Pxy + W0c(j)*(X0(:,j) - Xmean_bar)*(Y_sigma(:,j) - Y_mean)';
            end
            
    %       Kalman gain
            K   = Pxy/Py;
    
    %       MEASUREMENT UPDATE
            y      = (Y_obs - Y_mean);
            x_est  = K*y;
            Xstar  = Xmean_bar + x_est;

            P      = (P_bar - K*Py*K');
            
    %       Next iteration preparation
            Xold   = Xstar;
            told   = Measures(i).t;
            Pold   = P;

        else
            fprintf('\nTracking delle features perso...');
            break
        end
       
%       Residuals
        [err(:,i), prefit(:,i)] = Residuals_withCamera(Y_obs, G, Xmean_bar,...
            Xmean_bar, Stat_ID_Range, Stat_ID_Rate, check_Lidar, check_Limb, ...
            check_Deimos, check_ISL, Features_ID, Features_Mars_ID, O, IDX, et, pars,...
            units, file_features, file_features_Mars);


%       Storing the results
        P_t(:,:,i)  = abs(P).*units.CovDim;
        Pbar_t(:,:,i)  = P_bar.*units.CovDim;
        X_t(:,i)    = Xold.*units.Dim;
        x_t(:,i)    = x_est.*units.Dim;
        tobs(i)     = Measures(i).t;
        
    end
    
    fprintf('\nAnalysis complete!\n');
    
%   Initial states' estimation, deviation and covariance matrix
    Est.X       = Xold.*units.Dim;
    Est.P       = P.*units.CovDim;
    
%   Residuals and fit-residuals at different observation times
    Est.t       = tobs;
    Est.err     = err;
    Est.pre     = prefit;
    Est.y_t     = y_t;
    Est.X_t     = X_t;
    Est.x_t     = x_t;
    Est.P_t     = P_t;
    Est.Pbar_t  = Pbar_t;
    
end