clear
close all
clc

%%  Setup

    restoredefaultpath
    addpath(genpath('../'));
    addpath(genpath('./MMX_BSP_Files_Relative/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../useful_functions/planet_textures/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../../mice/'));
    addpath(genpath('../../computer-vision/'));
    addpath(genpath('../../paper/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../../paper/MMX_Product/MMX_BSP_Files_GravLib/'));
    addpath(genpath('./Utilities_relative/'))
    addpath(genpath('../Utilities/'))
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_049_4x4_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_031_013_4x4_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_031_013_4x4_826891269_829483269_relative_right.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));


%%  Visibility analysis

%   Model parameters
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;

    data        = '2026-03-16 06:30:00 (UTC)';
    data        = cspice_str2et(data);
    hours       = 3600;
    Nhours      = 5.3;

    times       = data:600:data+Nhours*hours;

    MMX_t       = cspice_spkezr('-35',times,'IAU_Phobos','none','-401');
    R_t         = zeros(3,3,size(MMX_t,2));
    tform_t     = rigidtform3d;

    for i = 1:size(MMX_t,2)
        i_t = MMX_t(1:3,i)/norm(MMX_t(1:3,i));
        k_t = cross(i_t,MMX_t(4:6,i))/norm(cross(i_t,MMX_t(4:6,i)));
        j_t = cross(k_t,i_t);
        R_t(:,:,i) = [-j_t, k_t, -i_t];
        tform_t(i) = rigidtform3d(R_t(:,:,i),MMX_t(1:3,i));
    end


    MMX_t       = MMX_t(1:3,:);
    Sun_t       = cspice_spkezr('10',times,'IAU_Phobos','none','-401');
    Sun_t       = Sun_t(1:3,:)./vecnorm(Sun_t(1:3,:));

    visCheck    = dot(MMX_t,Sun_t)./vecnorm(MMX_t)>(cos(pi/2));
    MMX_tLight  = MMX_t(:,visCheck);
    MMX_tDark   = MMX_t(:,~visCheck);


    load('Principal_Phobos.mat');
    figure()
    plot3(MMX_tDark(1,1),MMX_tDark(2,1),MMX_tDark(3,1),'o','Color','blue');
    grid on; hold on; axis equal;
    plot3(MMX_tLight(1,:),MMX_tLight(2,:),MMX_tLight(3,:),'*','Color','red');
    plot3(MMX_tDark(1,end),MMX_tDark(2,end),MMX_tDark(3,end),'o','Color','blue');
    for i = 2:size(MMX_t,2)-1
        plotCamera('AbsolutePose',tform_t(i),'Size',.5,'Color','red')
    end 
    plotCamera('AbsolutePose',tform_t(1),'Color','blue','Size',.5)
    plotCamera('AbsolutePose',tform_t(end),'Color','blue','Size',.5)
    % planetPlot('Asteroid',[0;0;0],pars.Phobos,1);
    plot3(Point_Cloud(:,1),Point_Cloud(:,2),Point_Cloud(:,3),'.','Color','k');
    xlabel('Radial $[km]$','FontSize',26,'Interpreter','latex')
    ylabel('Along Track $[km]$','FontSize',26,'Interpreter','latex')
    zlabel('Z $[km]$','FontSize',26,'Interpreter','latex')
    legend('Last dark picture','Good pictures','First dark picture',...
        'FontSize',30,'Interpreter','latex')
