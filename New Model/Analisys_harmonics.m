clear
close all
clc

%%  Setup

    restoredefaultpath
    addpath(genpath('../'));
    addpath(genpath('./MMX_BSP_Files_Relative/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../useful_functions/planet_textures/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../../mice/'));
    addpath(genpath('../../computer-vision/'));
    addpath(genpath('../../paper/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../../paper/MMX_Product/MMX_BSP_Files_GravLib/'));
    addpath(genpath('./Utilities_relative/'))
    addpath(genpath('../Utilities/'))
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_031_4x4_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_031_013_4x4_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_031_013_4x4_826891269_829483269_relative_right.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));


%%  Load observations and path for syntethic pictures

    load("YObs_rel.mat");


%%  Initial conditions for the analysis

%   Model parsameters
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    day         = 86400;
    pars.et0    = data;
    [Ph,pars]   = Phobos_States_NewModel(data,pars);

%   Covariance analysis parsameters
    [pars, units] = CovarianceAnalysisParameters_harmonics(pars, units);
    
%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;
    Iz      = pars.IPhz_bar + pars.ni*Ph0(1)^2;
    K       = Iz*Ph0(4) + pars.IPhz_bar*Ph0(8);
    pars.K  = K;

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-35', data, 'IAU_Phobos', 'none', '-401');
    MMX0    = [pars.PB, zeros(3,3); zeros(3,3), pars.PB]*MMX0./units.sfVec;

%   Analysis initial state vector
    % St0     = [MMX0; Ph0(1:2); Ph0(7:8); pars.Clm; pars.Slm; pars.bias];
    St0     = [MMX0; Ph0(1:2); Ph0(7:8); Ph0(3:4); pars.Clm; pars.Slm; pars.bias];
    
    Est0.dx = zeros(size(St0,1),1);
    pars.flag_FILTERING = 0;
    Est0.X  = St0 + Est0.dx;
    Est0.P0 = pars.P0;
    Est0.t0 = data*units.tsf;

%%  Analysis

%   Features catalogues
    file_features = 'Principal_Phobos_ogni10.mat';
    file_features_Mars = 'Mars_4facets.mat';

%   Analysis 
    pars.alpha = 1;
    pars.beta  = 2;

    % [Est] = UKF_Tool(Est0,@Dynamics_MPHandMMX_Relative_harmonics,...
    %     @Cov_Dynamics_Rel_harmonics, @Observables_model_relative,...
    %     pars.R,YObs,pars,units,file_features,file_features_Mars);

    [Est] = UKF_Tool(Est0,@Dynamics_MPHandMMX_Relative_harmonics_theta,...
        @Cov_Dynamics_Rel_harmonics_theta, @Observables_model_relative,...
        pars.R,YObs,pars,units,file_features,file_features_Mars);
    

    % Results_relative_harmonics(Est, [4,4], pars, units)
    % Plot_MMX_ellipsoid(Est,pars,units)

    % save('./Results/QSOLb_7day_Deimos2_5feat.mat', 'Est')
