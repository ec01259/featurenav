clear 
close all
clc


%%  Definition of the points to start the continuation for the QSO

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../../paper/');
    addpath('../../paper/MMX_Fcn_Miscellaneous/');
    addpath('../../paper/MMX_Product/MMX_BSP_Files_GravLib/');
    addpath('../../paper/MMX_Fcn_CovarianceAnalyses/');
    addpath('../../paper/Reference_Definition/');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));


%   Initial state
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    pars.et0    = data;
    [Ph,pars]   = Phobos_States_NewModel(data,pars);

    day = 86400;
    t   = (0:100:2*day);

%%  Phobos

%   Is the IAU_Phobos complanar with the perifocal frame?
    ea1 = zeros(size(t));
    ea2 = zeros(size(t));
    ea3 = zeros(size(t));
    ea11 = zeros(size(t));
    ea21 = zeros(size(t));
    ea31 = zeros(size(t));
    PeriN  = pars.MARSIAU2perifocal;

    for i = 1:size(t,2)
        BN  = cspice_sxform('MARSIAU', 'IAU_Phobos', data+t(i));
        BN  = BN(1:3,1:3);
        PN  = pars.PB*BN(1:3,1:3);
        BPeri  = BN*PeriN';
        PPeri  = PN*PeriN';
        [ea1(i),ea2(i),ea3(i)]  = cspice_m2eul(BPeri,3,2,1);
        [ea11(i),ea21(i),ea31(i)]  = cspice_m2eul(PPeri,3,2,1);
    end

    figure()
    plot(t/3600,ea3)
    hold on; grid on;
    plot(t/3600,ea2)
    plot(t/3600,ea1)
    legend('Euler angle X','Euler angle Y','Euler angle Z')
    title('Phobos IAU euler angle wrt perifocal frame')
    xlabel('t [hours]')
    ylabel('[rad]')

    figure()
    plot(t/3600,ea31)
    hold on; grid on;
    plot(t/3600,ea21)
    plot(t/3600,ea11)
    legend('Euler angle X','Euler angle Y','Euler angle Z')
    title('Phobos-principal euler angle wrt perifocal frame')
    xlabel('t [hours]')
    ylabel('[rad]')


    
%%  Mars

%   Is the IAU_Mars complanar with the perifocal frame?
    ea12 = zeros(size(t));
    ea22 = zeros(size(t));
    ea32 = zeros(size(t));
    PeriN  = pars.MARSIAU2perifocal;

    for i = 1:size(t,2)
        BN  = cspice_sxform('MARSIAU', 'IAU_Mars', data+t(i));
        BN  = BN(1:3,1:3);
        BPeri  = BN*PeriN';
        [ea12(i),ea22(i),ea32(i)]  = cspice_m2eul(BPeri,3,2,1);
    end

    figure()
    plot(t/3600,ea32)
    hold on; grid on;
    plot(t/3600,ea22)
    plot(t/3600,ea12)
    legend('Euler angle X','Euler angle Y','Euler angle Z')
    title('Mars euler angle wrt perifocal frame')
    xlabel('t [hours]')
    ylabel('[rad]')
