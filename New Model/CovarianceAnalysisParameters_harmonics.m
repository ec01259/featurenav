function [pars, units] = CovarianceAnalysisParameters_harmonics(pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [pars, units] = CovarianceAnalysisParameters_harmonics(pars, units)
%
% This function adds some units and parameters useful to the covariance
% analysis
% 
% Inputs:
% pars          Structure which contains parameters
% units         Structure with units 
% 
% Outputs:
% pars.ObsNoise             Observations noises standard deviation
% pars.ObsNoise.range       Standard deviation of the relative distance measure
% pars.ObsNoise.range_rate  Standard deviation of the relative velocity measure
% pars.ObsNoise.lidar       Standard deviation of the lidar measure
% pars.ObsNoise.camera      Standard deviation of the camera measure
% pars.ObsNoise.pixel       Standard deviation of the pixel error
% pars.camera_intrinsic     Camera intrinsic matrix
% pars.FOV                  Camera Filed Of View
% pars.interval             Seconds between observations
% pars.Clm                  Vector with all the Clm coefficients
% pars.Slm                  Vector with all the Slm coefficients
% pars.nCoeff               Number of the harmonics coefficients
% pars.P0                   A-priori covariance
% pars.sigma                PN diffusion coefficient
%
% units.Dim                 Vector of normalizing units [sfVec, sfVec, ones(pars.nCoeff,1)]
% units.CovDim              Matrix of normalizing units
%
% Author: STAG Team
% Date: Jun 2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Parameters
    
% Observation noise
pars.ObsNoise.range     = 5e-3/units.lsf;         % [km]
pars.ObsNoise.range_rate= 5e-7/units.vsf;         % [km/s]
pars.ObsNoise.lidar     = 1e-2/units.lsf;         % [km]
pars.ObsNoise.ISL       = 1e-3/units.lsf;         % [km]

cam.f       = 15;                       %[mm]
cam.nPixX   = 3296;                     %[n. pixel]
cam.nPixY   = 2472;                     %[n. pixel]
cam.Res     = 100;
cam.SixeX   = 36;                       %[mm]
cam.SixeY   = 24;                       %[mm]
cam.pixARx  = 1;
cam.pixARy  = 1;
pars.camera_intrinsic = IntrinsicParameters(cam);
pars.cam = cam;
pars.FOV    = deg2rad(45);
pixels      = 2472;

pars.ObsNoise.camera    = pars.FOV/pixels;   % [rad/pixel]
pars.ObsNoise.pixel     = .2;               % [n. pixel]


% Useful to adimensionalize the observables
units.Observations = [units.lsf; units.vsf; units.lsf; units.vsf; units.lsf; units.vsf; units.lsf; 1; 1];

% Matrix of the observation noise
pars.R      = zeros(9,9);
pars.R(1,1) = pars.ObsNoise.range^2;
pars.R(2,2) = pars.ObsNoise.range_rate^2;
pars.R(3,3) = pars.ObsNoise.range^2;
pars.R(4,4) = pars.ObsNoise.range_rate^2;
pars.R(5,5) = pars.ObsNoise.range^2;
pars.R(6,6) = pars.ObsNoise.range_rate^2;
pars.R(7,7) = pars.ObsNoise.lidar^2;
pars.R(8,8) = pars.ObsNoise.pixel^2;
pars.R(9,9) = pars.ObsNoise.ISL^2;

% Seconds between observation
pars.interval_Range         = 3600;      % s
pars.interval_Range_rate    = 600;       % s
pars.interval_lidar         = 600;          % s
pars.interval_features      = 600;          % s
pars.interval_limbs         = 600;          % s
pars.interval_ISL           = 600;          % s

% Selection of the observables
pars.flag_DSN           = 0;
pars.flag_Lidar         = 1;
pars.flag_Limb          = 1;
pars.flag_features      = 1;
pars.flag_features_Mars = 0;
pars.flag_Deimos        = 1;
pars.flag_ISL           = 0;

% Useful Observables parameters
pars.cutoffLIDAR    = 200;  % km
pars.elevation      = 80;   % deg

% Rover position on Phobos surface
lat  = deg2rad(0);
lon  = deg2rad(180);

% Phobos radius as function of latitude and longitude
alpha = pars.Phobos.alpha;
beta  = pars.Phobos.beta;
gamma = pars.Phobos.gamma;
   
R_latlon = (alpha*beta*gamma)/sqrt(beta^2*gamma^2*cos(lon)^2 +...
   gamma^2*alpha^2*sin(lon)^2*cos(lat)^2 + alpha^2*beta^2*sin(lon)^2*sin(lat));

z_rover = R_latlon*sin(lat);
x_rover = R_latlon*cos(lat)*cos(lon);
y_rover = R_latlon*cos(lat)*sin(lon);

pars.X_rover = [x_rover; y_rover; z_rover];

% Gravitational harmonics' parameters
Clm = [];
Slm = [];

for i=1:size(pars.SH.Clm,1)
    Clm = [Clm; pars.SH.Clm(i,1:i)'];
end
for i=2:size(pars.SH.Clm,1)
    Slm = [Slm; pars.SH.Clm(i,2:i)'];
end

pars.Clm = Clm;
pars.Slm = Slm;

% Measurements biases
bias = [0; 0; 0; 0; 0; 0]...
    ./[units.lsf; units.vsf; units.lsf; 1; 1; units.lsf];

pars.bias = bias;

% Unit vector and parameters number useful for later use
pars.nCoeff     = size(pars.Clm,1) + size(pars.Slm,1);
pars.Coeff_Cov  = 10*abs([pars.Clm; pars.Slm]);
pars.Coeff_Cov((pars.Coeff_Cov)==0) = 1e-2;

pars.nBias      = size(bias,1);
% units.Dim       = [units.sfVec; units.lsf; units.vsf; 1; units.tsf;...
%      ones(pars.nCoeff,1); units.lsf; units.vsf;...
%      units.lsf; 1; 1];
units.Dim       = [units.sfVec; units.lsf; units.vsf; 1; units.tsf; 1; units.tsf;...
     ones(pars.nCoeff,1); units.lsf; units.vsf;...
     units.lsf; 1; 1; units.lsf];
units.CovDim    = (repmat(units.Dim, 1, length(units.Dim)).*...
    repmat(units.Dim', length(units.Dim), 1));
pars.sizeSt     = size(units.Dim,1);

% A-priori covariance
% pars.P0         = diag(([1*ones(3,1); .1e-4*ones(3,1);...    
%     1e-1; 1e-5; 1e-4; 1e-7; 1*ones(pars.nCoeff,1);...
%     1; 1e-3; 1; 1e-2; 1e-2; 1]./units.Dim).^2);
pars.P0         = diag(([1*ones(3,1); .1e-4*ones(3,1);...    
    1e-1; 1e-5; 1e-4; 1e-7; 1e-1; 1e-5; 1e-2*ones(pars.nCoeff,1);...
    1; 1e-3; 1; 1e-2; 1e-2; 1]./units.Dim).^2);
% pars.P0         = diag(([1*ones(3,1); .1e-4*ones(3,1);...    
%     1e-1; 1e-5; 1e-4; 1e-7; 1e-1; 1e-5; pars.Coeff_Cov;...
%     1; 1e-3; 1; 1e-2; 1e-2; 1]./units.Dim).^2);

% Process noise su MMX e Phobos
pars.sigma    = 1e-10/(units.vsf*units.tsf);
pars.sigmaPh  = 0/(units.vsf*units.tsf);
end