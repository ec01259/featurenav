function [SQRT_X, SQRT_V, meanX, sigmaX, maxX, minX, meanV, sigmaV, maxV, minV,...
    meanLib, maxLib, minLib, sigmaLib] = Envelopes_Plot_Rel(Est,fine)


sqx = squeeze(Est.P_t(1,1,1:end-1));
sqy = squeeze(Est.P_t(2,2,1:end-1));
sqz = squeeze(Est.P_t(3,3,1:end-1));
SQRT_X = 3*real(sqrt(sqx + sqy + sqz));
sqvx = squeeze(Est.P_t(4,4,1:end-1));
sqvy = squeeze(Est.P_t(5,5,1:end-1));
sqvz = squeeze(Est.P_t(6,6,1:end-1));
SQRT_V = 3*real(sqrt(sqvx + sqvy + sqvz));



SQRT_X2 = SQRT_X(fine:end);
meanX   = mean(SQRT_X2);
maxX    = max(SQRT_X2);
minX    = min(SQRT_X2);
sigmaX  = std(SQRT_X2);

SQRT_V2 = SQRT_V(fine:end);
meanV   = mean(SQRT_V2);
maxV    = max(SQRT_V2);
minV    = min(SQRT_V2);
sigmaV  = std(SQRT_V2);

Lib     = 3*real(sqrt(Est.P_t(9,9,fine:end)));
meanLib = mean(Lib);
maxLib  = max(Lib);
minLib  = min(Lib);
sigmaLib    = std(Lib);

end