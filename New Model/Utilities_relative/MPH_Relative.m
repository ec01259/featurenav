function dx = MPH_Relative(~,x,pars,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = MPH_Relative(t,x,parss,units)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .parss     Problem parsameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational parsameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
   
%%  Phobos's orbit

    RPh         = x(1);
    RPh_dot     = x(2);
    Xsi         = x(3);
    Xsi_dot     = x(4);

%%  Phobos's orbit
    
%   Potential's first order parstials
    dVdRPh      = pars.ni/RPh^2*(1 + 3/(2*RPh^2)*((pars.IMz_bar - pars.Is) -...
            .5*pars.IPhx_bar - .5*pars.IPhy_bar + pars.IPhz_bar + ...
            1.5*(pars.IPhy_bar - pars.IPhx_bar)*cos(2*Xsi)));
    dVdXsi      = 1.5*pars.ni/RPh^3 * (pars.IPhy_bar - pars.IPhx_bar)*sin(2*Xsi);
    Iz          = pars.IPhz_bar + pars.ni*RPh^2;

%   Phobos equations of motions
    RPh_ddot    = (pars.K - pars.IPhz_bar*Xsi_dot)^2*RPh/Iz^2 - dVdRPh/pars.ni;
    Xsi_ddot    = -(1 + pars.ni*RPh^2/pars.IPhz_bar)*dVdXsi/(pars.ni*RPh^2) +...
                    + 2*(pars.K - pars.IPhz_bar*Xsi_dot)*RPh_dot/(RPh*Iz);
        
    

%%  Output for the integrator
    
    dx = [RPh_dot; RPh_ddot; Xsi_dot; Xsi_ddot];
    
    
end