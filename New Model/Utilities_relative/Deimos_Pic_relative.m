function Deimos_obs = Deimos_Pic_relative(MMX, Deimos, Mars, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = Deimos_Pic_relative(MMX, Phobos, Deimos, Sun, Phi, pars, units)
%
% Identify wether Deimos is in the FOV and how far is it
%
% Input:
%     
% MMX           MMX state vector in the MARSIAU reference frame
% Phobos        Phobos state vector in the MARSIAU reference frame
% Deimos        Deimos state vector in the MARSIAU reference frame
% Sun           Sun state vector in the MARSIAU reference frame
% pars          parameters structure
% units         units structure
% 
% Output:
% 
% Limb_Range    Range derived from Mars limb size
% 
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%   MMX-Phobos direction in the rotating reference frame
    r_sb_Ph = MMX(1:3);

%   Mars pole position in the Phobos rotating frame
    Deimos_center   = Deimos(1:3);
    Deimos_pole     = [0; 15; 0] + Deimos(1:3);

%   Check if Mars is in the pic
    Mars_centre = Mars(1:3);

%   Phobos pole position in the Phobos rotating frame
    Phobos_pole     = [0; 0; -pars.Phobos.gamma];
    R_ini           = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Phobos_pole     = R_ini*Phobos_pole;
    Phobos_centre   = zeros(3,1);
    Phobos_centre   = R_ini*Phobos_centre;

    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);

%   Deimos
    DC  = PM*[-Deimos_center; 1];
    DC  = DC./repmat(DC(3),3,1);
    DP  = PM*[-Deimos_pole; 1];
    DP  = DP./repmat(DP(3),3,1);
    
%   Phobos
    PhP = PM*[-Phobos_pole; 1];
    PhP = PhP./repmat(PhP(3),3,1);
    PhC = PM*[-Phobos_centre; 1];
    PhC = PhC./repmat(PhC(3),3,1);

%   Mars
    MC  = PM*[-Mars_centre; 1];
    MC  = MC./repmat(MC(3),3,1);


    Phobos_range = norm(PhP - PhC);
    Phobos_ellipsoid = @(x,y) (x-PhC(1))^2 + (y-PhC(2))^2 - Phobos_range^2;


    if (any(DC<0))||(DC(1)>pars.cam.nPixX)||(DC(2)>pars.cam.nPixY)||...
            (dot(r_sb_Ph, Deimos_center)>0)||(Phobos_ellipsoid(DC(1),DC(2))<0)||...
            ((any(MC>0)&&(MC(2)<pars.cam.nPixY)&&(MC(1)<pars.cam.nPixX))&&(dot(r_sb_Ph, Mars_centre)>0))
        % if (Phobos_ellipsoid(DC(1),DC(2))<0)
        %     Plotfeatures_Pic([DC, PhC, MC], [DP, PhP], pars);
        % end
        Deimos_obs  = [];
    else
        Limb_radius = DP(1:2) - DC(1:2);
        Deimos_Limb = sqrt(Limb_radius(1)^2 + Limb_radius(2)^2) + random('Normal',0, pars.ObsNoise.pixel);
        Deimos_obs  = Deimos_Limb;
        % if Deimos_Limb < 1.5
        Deimos_obs  = DP(1:2);
        % end
        % Plotfeatures_Pic([DC, PhC, MC], [DP, PhP], pars);
    end


end