function [Deimos_LOS, Deimos_Limb] = Deimos_Pic_relative_model(MMX, Deimos, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = Deimos_Pic_relative_model(MMX, Phobos, Deimos, Sun, Phi, pars, units)
%
% Identify wether Deimos is in the FOV and how far is it
%
% Input:
%     
% MMX           MMX state vector in the MARSIAU reference frame
% Phobos        Phobos state vector in the MARSIAU reference frame
% Deimos        Deimos state vector in the MARSIAU reference frame
% Sun           Sun state vector in the MARSIAU reference frame
% pars          parameters structure
% units         units structure
% 
% Output:
% 
% Limb_Range    Range derived from Mars limb size
% 
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%   MMX-Phobos direction in the rotating reference frame
    r_sb_Ph     = MMX(1:3);
    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);

%   Mars pole position in the Phobos rotating frame
    Deimos_center   = Deimos(1:3);
    Deimos_pole     = [0; 15; 0] + Deimos(1:3);

%   Deimos
    DC  = PM*[-Deimos_center; 1];
    DC  = DC./repmat(DC(3),3,1);
    DP  = PM*[-Deimos_pole; 1];
    DP  = DP./repmat(DP(3),3,1);
    

    Limb_radius = DP(1:2) - DC(1:2);
    Deimos_Limb = sqrt(Limb_radius(1)^2 + Limb_radius(2)^2);
    Deimos_LOS  = DP(1:2);
    

end