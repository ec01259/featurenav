function [G, Rm] = Observables_model_relative(et, X, St_ID_Range, St_ID_Rate,...
    Lidar_check, Limb_check, Deimos_check, ISL_check, Feature_ID, Features_Mars_ID, pars, units,...
    O, file_features, file_features_Mars)
%==========================================================================
% [G, R] = Observables_model_with_Features(et,Obs,X,St_ID_Range,St_ID_Rate, Feature_ID, par,units)
%
% Compute the Range, Range Rate, Lidar's and camera's measures 
%
% INPUT: Description Units
%
% et        - Time Epoch                                                s
%
% stationID - Available Observables at the epoch t
%
% X         - State Vector defined in the Phobos rotating frame 
%               at the time being considered (42x1)
%               .MMX Position Vector (3x1)                              km
%               .MMX Velocity Vector (3x1)                              km/s
%               .Phobos Position Vector (3x1)                           km
%               .Phobos Velocity Vector (3x1)                           km/s
%               .Clm harmonics coefficients
%               .Slm Hamronics coefficients
%               .State Transition Matrix 
%
% par       -  Parameters structure 
% 
%
% OUTPUT:
%
% G         - G
% 
% DO NOT FORGET TO USE RESHAPE LATER IN THE PROJECT
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 06/2022
%
%==========================================================================
    
%   Dimensions of the problem
    n       = pars.d/2;
    d       = pars.d;
    PB      = [pars.PB, zeros(3,3); zeros(3,3), pars.PB];

%   Output initialization
    range_Camb      = [];
    range_rate_Camb = [];
    range_Gold      = [];
    range_rate_Gold = [];
    range_Mad       = [];
    range_rate_Mad  = [];
    lidar           = [];
    Limb            = [];
    Deimos          = [];
    ISLink          = [];

    Rm              = [];

%   Unpacking of the state vector
    X_MMX       = X(1:d);
    r_MMX       = X_MMX(1:3);
    v_MMX       = X_MMX(4:6);
    
    bias    = X(pars.sizeSt-5:end);
    RPh     = X(7);
    Xsi     = X(9);
    theta   = X(11);
    
%   Definition of Mars position WRT Earth J2000
    Rtheta  = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];
    r_Phobos    = pars.perifocal2MARSIAU*RPh*[cos(theta); sin(theta); 0];
    v_Phobos    = pars.perifocal2MARSIAU*(Rtheta'*([X(8); 0; 0]) + ...
           cross([0; 0; X(12)], RPh*[cos(theta); sin(theta); 0]));
    Phobos   = [r_Phobos.*units.lsf; v_Phobos.*units.vsf];

    Mars    = cspice_spkezr('499',et,'MARSIAU','none','EARTH')./units.sfVec;

    Rot     = cspice_sxform('IAU_PHOBOS','MARSIAU',et);
    X_MMX_MARSIAU = Rot*PB'*(X_MMX.*units.sfVec);
    r_MMX_MARSIAU = X_MMX_MARSIAU(1:3)./units.lsf;
    v_MMX_MARSIAU = X_MMX_MARSIAU(4:6)./units.vsf;
    
    
% -------------------------------------------------------------------------
%                       DEEP SPACE NETWORK
% -------------------------------------------------------------------------

%   Which station is working
%   Camberra's Position in the J2000 Earth-centered
    Camb    = cspice_spkezr('DSS-45', et, 'MARSIAU', 'none', 'EARTH');
    Camb    = Camb./units.sfVec;    
%   Goldstone's Position in the J2000 Earth-centered
    Gold    = cspice_spkezr('DSS-24', et, 'MARSIAU', 'none', 'EARTH');
    Gold    = Gold./units.sfVec;
%   Madrid's Position in the J2000 Earth-centered
    Mad     = cspice_spkezr('DSS-65', et, 'MARSIAU', 'none', 'EARTH');
    Mad     = Mad./units.sfVec;



    if sum((St_ID_Range == 1)==1)
%       Camberra antennas available
        r           = norm(Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Camb(1:n));
        range_Camb  = r + bias(1);
        Rm   = [Rm; O(1,1)];            
    end

    if sum((St_ID_Range == 2)==1)
%       Goldstone antennas available
        r           = norm(Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Gold(1:n));
        range_Gold  = r + bias(1);
        Rm   = [Rm; O(1,1)];     
    end

    if sum((St_ID_Range == 3)==1)
%       Madrid antennas available
        r   = norm(Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Mad(1:n));
        range_Mad = r + bias(1);
        Rm   = [Rm; O(1,1)];     
    end
    


    if sum((St_ID_Rate == 1)==1)
%       Camberra antennas available
        R       = norm(Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Camb(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_Phobos + v_MMX_MARSIAU - Camb(n+1:d)).*...
            (Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Camb(1:n)));
        range_rate_Camb = r_dot + bias(2);
        Rm   = [Rm; O(2,2)];     
    end
    
    if sum((St_ID_Rate == 2)==1)
%       Goldstone antennas available
        R   = norm(Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Gold(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_Phobos + v_MMX_MARSIAU - Gold(n+1:d)).*...
            (Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Gold(1:n)));
        range_rate_Gold = r_dot + bias(2);
        Rm   = [Rm; O(2,2)];     
    end

    if sum((St_ID_Rate == 3)==1)
%       Camberra antennas available
        R   = norm(Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Mad(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_Phobos + v_MMX_MARSIAU - Mad(n+1:d)).*...
            (Mars(1:n) + r_Phobos + r_MMX_MARSIAU - Mad(1:n)));
        range_rate_Mad = r_dot + bias(2);
        Rm   = [Rm; O(2,2)];     
    end
    
    


% -------------------------------------------------------------------------
%                               LIDAR
% -------------------------------------------------------------------------


    if Lidar_check

%      Posizione di MMX nel Phobos Body-fixed reference frame
       r_bf = r_MMX;
       lat  = asin(r_bf(3)/norm(r_bf));
       lon  = atan2(r_bf(2), r_bf(1));

%      Phobos radius as function of latitude and longitude
       alpha = pars.Phobos.alpha;
       beta  = pars.Phobos.beta;
       gamma = pars.Phobos.gamma;
       
       R_latlon = (alpha*beta*gamma)/sqrt(beta^2*gamma^2*cos(lon)^2 +...
           gamma^2*alpha^2*sin(lon)^2*cos(lat)^2 + alpha^2*beta^2*sin(lon)^2*sin(lat));

       lidar = norm(r_bf) - R_latlon/units.lsf + bias(3);
       
       Rm   = [Rm; O(7,7)];     

    end
    

% -------------------------------------------------------------------------
%                           FEATURES SU PHOBOS
% -------------------------------------------------------------------------

    if ~isempty(Feature_ID)

%       There should be features
        Sun = PB*cspice_spkezr('10', et, 'IAU_Phobos', 'none', '-401');
        [~, Y_pix] = visible_features_model_relative(X_MMX.*units.sfVec,...
          Sun, file_features, pars, units);
       
        if ~isempty(Y_pix)
             Y_pix = Y_pix + repmat([bias(4); bias(5); 0], 1, size(Y_pix,2));
        else
            fprintf('\nTracking delle features perso!!\n');
            return
        end
       
    else
        Y_pix = [];
    end
   



% -------------------------------------------------------------------------
%                               MARS LIMB
% -------------------------------------------------------------------------


    if Limb_check
%      There should be the Mars Limb
       % Mars = PB*cspice_spkezr('499', et, 'IAU_Phobos', 'none', '-401');
       Mars = RPh*[cos(-Xsi); sin(-Xsi); 0]*units.lsf;
       Limb = Mars_LimbRange_relative_model(X_MMX.*units.sfVec, Mars, pars, units);
       Limb = Limb + (bias(4) + bias(5)); 
       Rm   = [Rm; O(8,8)];    
    end



% -------------------------------------------------------------------------
%                           FEATURES SU MARTE
% -------------------------------------------------------------------------


    if ~isempty(Features_Mars_ID)

%       There should be features
        Sun = PB*cspice_spkezr('10', et, 'IAU_Phobos', 'none', '-401');
        [~, Y_pix_Mars] = visible_features_Mars_model(X_MMX.*units.sfVec,...
            Phobos, -Sun, Xsi, Phi_M, file_features_Mars, pars, units);
       
       if ~isempty(Y_pix_Mars)
            Y_pix_Mars = Y_pix_Mars + repmat([bias(4); bias(5); 0], 1, size(Y_pix_Mars,2));
       else
           % fprintf('\nTracking delle features su Marte perso!!');
           % return
       end
       
    else
        Y_pix_Mars = [];
    end


% -------------------------------------------------------------------------
%                           DEIMOS LOS/RANGE
% -------------------------------------------------------------------------


    if Deimos_check.flag

%      There should be Deimos in the pic
       Deimos = PB*cspice_spkezr('402', et, 'IAU_Phobos', 'none', '-401');
       [Deimos_LOS, Deimos_Limb] = Deimos_Pic_relative_model(X_MMX.*units.sfVec, Deimos, pars, units);

       % Mars = RPh*[cos(-Xsi); sin(-Xsi); 0];
       % Mars_MMX = Mars-X_MMX(1:3);
       % Deimos_MMX = Deimos(1:3)/units.lsf - X_MMX(1:3);
       % 
       % i_M = Mars_MMX(1:3)/norm(Mars_MMX(1:3));
       % i_D = Deimos_MMX(1:3)/norm(Deimos_MMX(1:3));
       % Deimos = acos(dot(i_M,i_D));


       if Deimos_check.n==1
            Deimos = Deimos_Limb + (bias(4) + bias(5));
       else
            Deimos = Deimos_LOS + [bias(4); bias(5)];
       end

       Rm   = [Rm; O(8,8)*ones(Deimos_check.n,1)];    
    end

    
%--------------------------------------------------------------------------
%               ROVER INTERSATELLITE LINK PHOBOS SURFACE
%--------------------------------------------------------------------------

%   Position of MMX wrt Phobos
    if ISL_check
       ISLink   = norm(X(1:3) - pars.X_rover/units.lsf)  + bias(6);
       Rm       = [Rm; O(9,9)];    
    end

%--------------------------------------------------------------------------
%                               RECAP
%--------------------------------------------------------------------------

%   Ready to exit
    G.meas  = [range_Camb; range_Gold; range_Mad; range_rate_Camb; range_rate_Gold; ...
         range_rate_Mad; lidar; Limb; Deimos; ISLink];
    G.cam   = Y_pix;
    G.cam_Mars  = Y_pix_Mars;
    G.meas(isnan(G.meas)) = [];

     
end