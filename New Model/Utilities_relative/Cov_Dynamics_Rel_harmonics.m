function dx = Cov_Dynamics_Rel_harmonics(~,x,pars,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = Cov_Dynamics_Rel(t,x,pars,units)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .pars     Problem Parameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational Parameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%%  Useful quantities

    
%%  Dimensions

    d           = pars.d;                    % Dimension of the problem
    nCoeff      = pars.nCoeff;               % N. of gravitational parameters
    nBias       = pars.nBias;

    size_St     = (d+4+nCoeff+nBias);
    

%%  Useful quantities

    for i = 0:2*size_St-1

%   Spacecraft state vector
    rsb = x(1+i*size_St:3+i*size_St);     % Pos. vector wrt central body
    vsb = x(4+i*size_St:6+i*size_St);     % Vel. vector wrt central body
    
%   Phobos states
    RPh         = x(7+i*size_St);
    RPh_dot     = x(8+i*size_St);
    Xsi         = x(9+i*size_St);
    Xsi_dot     = x(10+i*size_St);

%   Phobos moment of inertia
    IPhx_bar    = pars.IPhx_bar;
    IPhy_bar    = pars.IPhy_bar;
    IPhz_bar    = pars.IPhz_bar;

%   Position of Mars wrt Phobos
    rPh = RPh*[cos(-Xsi); sin(-Xsi); 0];
    
 %%  Phobos's orbit
    
%   Potential's first order parstials
    dVdRPh      = pars.ni/RPh^2*(1 + 3/(2*RPh^2)*((pars.IMz_bar - pars.Is) -...
            .5*IPhx_bar - .5*IPhy_bar + IPhz_bar + ...
            1.5*(IPhy_bar - IPhx_bar)*cos(2*Xsi)));
    dVdXsi      = 1.5*pars.ni/RPh^3 * (IPhy_bar - IPhx_bar)*sin(2*Xsi);
    Iz          = IPhz_bar + pars.ni*RPh^2;

%   Phobos equations of motions
    RPh_ddot    = (pars.K - IPhz_bar*Xsi_dot)^2*RPh/Iz^2 - dVdRPh/pars.ni;
    Xsi_ddot    = -(1 + pars.ni*RPh^2/IPhz_bar)*dVdXsi/(pars.ni*RPh^2) +...
                    + 2*(pars.K - IPhz_bar*Xsi_dot)*RPh_dot/(RPh*Iz);
        
          
%%      MMX 2BP with Mars + 3rd body Phobos 
    
    r       = rPh - rsb; 
    R       = norm(r);
    gM1     = pars.G*pars.MMars*r/R^3;
    gJ2M    = 1.5*pars.G*pars.MMars*pars.J2*pars.RMmean/R^5*...
        [(1-5*(r(3)/R)^2)*r(1); (1-5*(r(3)/R)^2)*r(2);...
        (3-5*(r(3)/R)^2)*r(3)];
    
    gT      = pars.G*pars.MMars*rPh/RPh^3;
   
    gM      = gM1 + gJ2M - gT;
    
%   The gravitational effect of Phobos on MMX includes the Ellipsoid
%   harmonics

    pars.SH_sigma = pars.SH;
    pars.SH_sigma.Clm = pars.SH.Clm;
    pars.SH_sigma.Slm = pars.SH.Slm;
    count = 0;

    for j=1:size(pars.SH.Clm,1)
        for k=1:j
            count = count+1;
            pars.SH_sigma.Clm(j,k) = x(10+count+i*size_St);
        end
    end
    
    for j=2:size(pars.SH.Clm,1)
        for k=2:j
            count = count+1;
            pars.SH_sigma.Slm(j,k) = x(10+count+i*size_St);
        end
    end

    [gPh,~,~,~]  = SHGM_Full(rsb, pars.SH_sigma);

%   Combined effect on MMX   
    theta_dot = (pars.K - IPhz_bar*Xsi_dot)/Iz;
    theta_ddot  = dVdXsi/(pars.ni*RPh^2) - 2*RPh_dot*theta_dot/RPh;
    W       = [0; 0; theta_dot + Xsi_dot];
    W_dot   = [0; 0; theta_ddot + Xsi_ddot];
    
    g_tot   = gPh + gM - cross(W_dot,rsb) - 2 * cross(W,vsb) - cross(W,cross(W,rsb));


%%      Output for the integrator
    
        dx(1+i*size_St:size_St+i*size_St,1) = [vsb; g_tot; RPh_dot;...
            RPh_ddot; Xsi_dot; Xsi_ddot; zeros(nCoeff,1); zeros(nBias,1)];
    
    end

end