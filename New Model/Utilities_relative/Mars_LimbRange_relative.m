function Limb_Range = Mars_LimbRange_relative(MMX, Mars, Sun, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = Mars_LimbRange(MMX, Sun, pars, units)
%
% Identify wether Mars is in the FOV and how far is it
%
% Input:
%     
% MMX           MMX state vector in the MARSIAU reference frame
% Phobos        Phobos state vector in the MARSIAU reference frame
% Sun           Sun state vector in the MARSIAU reference frame
% pars          parameters structure
% units         units structure
% 
% Output:
% 
% Limb_Range    Range derived from Mars limb size
% 
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%   MMX-Phobos direction in the rotating reference frame
    r_sb_Ph = MMX(1:3);

%   MMX-Mars direction in the rotating reference frame
    r_sM    = MMX(1:3) - Mars(1:3);
    I_sM    = r_sM/norm(r_sM);

%   Mars pole position in the Phobos rotating frame
    Mars_pole   = [0; 0; -pars.Mars.gamma];
    R_ini       = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Mars_pole   = Mars_pole - Mars(1:3);
    Mars_centre = - Mars(1:3);

%   Phobos pole position in the Phobos rotating frame
    Phobos_pole     = [0; 0; -pars.Phobos.gamma];
    Phobos_pole     = R_ini*Phobos_pole;
    Phobos_centre   = zeros(3,1);
    Phobos_centre   = R_ini*Phobos_centre;

    
    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);
    MP  = PM*[Mars_pole; 1];
    MP  = MP./repmat(MP(3),3,1);
    MC  = PM*[Mars_centre; 1];
    MC  = MC./repmat(MC(3),3,1);

    PhP = PM*[-Phobos_pole; 1];
    PhP = PhP./repmat(PhP(3),3,1);
    PhC = PM*[-Phobos_centre; 1];
    PhC = PhC./repmat(PhC(3),3,1);

    Phobos_range = norm(PhP - PhC);
    Phobos_ellipsoid = @(x,y) (x-PhC(1))^2 + (y-PhC(2))^2 - Phobos_range^2;

    
    if ((all(MC>0))&&(all(MP>0))&&((MC(1)<pars.cam.nPixX))&&...
            (MP(1)<pars.cam.nPixX)&&(MC(2)<pars.cam.nPixY)&&...
            (MP(2)<pars.cam.nPixY))&&(dot(r_sb_Ph, Mars_centre)<0)&&...
            (Phobos_ellipsoid(MP(1),MP(2))>0)

        Limb_radius = MP - MC;
        Limb_Range  = sqrt(Limb_radius(1)^2 + Limb_radius(2)^2) + random('Normal',0, pars.ObsNoise.pixel);
        Limb_Range  = Limb_Range - rem(Limb_Range,pars.ObsNoise.pixel);

        alpha       = atan2(-pars.Mars.gamma,norm(r_sM));
        Limb_Range  = pars.cam.f*tan(alpha)*pars.cam.nPixX/pars.cam.SixeX + random('Normal',0, pars.ObsNoise.pixel);
        
        % Plotfeatures_Pic([MC, PhC], [MP, PhP], pars);
        % pause(.3)
        
    else
        Limb_Range = [];
    end


end