clear
close all
clc

%%  Definition of the points to start the continuation for the QSO

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('../');
    addpath('../Initial_cond_perBSP/');
    addpath('../../../paper/MMX_Fcn_CovarianceAnalyses');
    addpath('../../../paper/MMX_Product/MMX_BSP_Files_GravLib/');
    addpath('../../../paper/Reference_Definition/');
    addpath('../../../paper/');
    addpath(genpath('../../../mice/'));
    addpath(genpath('../../../generic_kernels/'));
    addpath(genpath('../../../useful_functions/'));
    addpath(genpath('../../../dynamical_model/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh('MARPHOFRZ.txt');
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));


    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Geom.alpha  = alpha;
    Geom.beta   = beta;
    Geom.gamma  = gamma;
    muPh        = 7.1139e-4;                      %km^3/s^2
    muM         = 4.2828e4;                       %km^3/s^2
    pPh         = 9377.2;                         %km 
    omega       = sqrt((muM + muPh)/pPh^3);       %rad/s
    T           = 1/omega;
    load("Tpo.mat")
    % [pars, units] = MMX_DefineNewModelParametersAndUnits_Right;
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    pars.et0    = data;
    [Ph, pars]  = Phobos_States_NewModel(data,pars);
    

  % QSO-Lb
    load("SwingQSO-La_5rev_8x8.mat")

%   If MMX comes from the optimization
    X_MMX   = X_good(1:6,1);

%   If MMX comes from a .bsp file
    % cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
    % X_MMX   = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;


%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;
    Iz      = pars.IPhz_bar + pars.ni*Ph0(1)^2;
    K       = Iz*Ph0(4) + pars.IPhz_bar*Ph0(8);
    pars.K  = K;

%   Initial state vector
    St0     = [X_MMX; Ph0(1:2); Ph0(7:8)];

%   Integration
    day     = 86400;
    period_Def = 30*day;
    tspan   = [0, period_Def]*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Relative_harmonics(t,X,pars,units),tspan,St0,opt);
    PB      = [pars.PB, zeros(3,3); zeros(3,3), pars.PB];
    Xmmx    = PB'*X(:,1:6)'.*units.sfVec;
    t       = t/units.tsf;



%%  BSP Generation

    Model.epoch = data;
    Model.pars  = pars;
    Model.units = units;
    micePATH    = '~/Documents/MATLAB/mice/';
    
    TargetFolder = '../MMX_BSP_Files_Relative/';
    if (exist(TargetFolder, 'dir') ~= 7)
        command = sprintf('mkdir %s', TargetFolder);
        system(command);
    end
    
    MMX_GenerateBSPFiles_Relative(t, Xmmx, 'SwingQSO', [049, 07], period_Def, Model, micePATH)


%%  Test risultati

    addpath(genpath('../MMX_BSP_Files_Relative/'))
    cspice_furnsh(which('MMX_SwingQSO_049_007_4x4_826891269_829483269_relative_right.bsp'));
    MMX_BSP     = cspice_spkezr('-35', data+t', 'IAU_Phobos', 'none', '-401');
    err_MMX     = PB*Xmmx - PB*MMX_BSP;
    
    figure(1)
    subplot(2,3,1)
    plot(t/3600,err_MMX(1,:)*units.lsf)
    ylabel('$\Delta X [km]$',Interpreter='latex')
    grid on
    subplot(2,3,2)
    plot(t/3600,err_MMX(2,:)*units.lsf)
    ylabel('$\Delta Y [km]$',Interpreter='latex')
    grid on
    subplot(2,3,3)
    plot(t/3600,err_MMX(3,:)*units.lsf)
    ylabel('$\Delta Z [km]$',Interpreter='latex')
    grid on
    subplot(2,3,4)
    plot(t/3600,err_MMX(4,:)*units.vsf)
    ylabel('$\Delta Vx [km/s]$',Interpreter='latex')
    grid on
    grid on
    subplot(2,3,5)
    plot(t/3600,err_MMX(5,:)*units.vsf)
    ylabel('$\Delta Vy [km/s]$',Interpreter='latex')
    grid on
    subplot(2,3,6)
    plot(t/3600,err_MMX(6,:)*units.vsf)
    ylabel('$\Delta Vz [km/s]$',Interpreter='latex')
    grid on
    

    figure(2)
    % plot3(Xmmx(1,:), Xmmx(2,:), Xmmx(3,:));
    hold on;
    grid on;
    plot3(MMX_BSP(1,:), MMX_BSP(2,:), MMX_BSP(3,:));
    planetPlot('Asteroid',[0;0;0],Geom,1);
    xlabel('x [km]');
    ylabel('y [km]');
    zlabel('z [km]');
    axis equal


    