function MMX_GenerateBSPFiles_Relative(t, Xmmx, OrbitType, Amplitudes, PropTime, Model, micePATH)



%%  Load SPICE
    MMX_InitializeSPICE;
    et0 = Model.epoch;
    et1 = et0 + PropTime;



%%  Create MMX Ephemeris file
    if(strcmp(OrbitType, 'QSO'))
        amp = sprintf('%03.0f', Amplitudes(1));
    else
        amp = sprintf('%03.0f_%03.0f', Amplitudes(1), Amplitudes(2));
    end
    dates = sprintf('%09.0f_%09.0f', et0, et1);
    SHmodel = sprintf('%dx%d',size(Model.pars.SH.Clm,1)-1,size(Model.pars.SH.Clm,2)-1);
    bspfilename = ['MMX_',OrbitType,'_',amp,'_',SHmodel,'_',dates,'_relative_right.bsp'];
    
    if(exist(bspfilename,'file')~=2)
        ID = -35;
        MMX_MakeBSPFile_GravLib(bspfilename, ID, t, Xmmx, '-401', 'IAU_PHOBOS', Model, [micePATH, '/exe/']);
        movefile(bspfilename, '../MMX_BSP_Files_Relative');
    else
        fprintf('MMX ephemeris file already exists!\n');
    end


end
