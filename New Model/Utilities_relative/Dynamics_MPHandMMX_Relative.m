function dx = Dynamics_MPHandMMX_Relative(~,x,pars,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = Dynamics_MMXonly_Relative(t,x,parss,units)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .parss     Problem parsameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational parsameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%%  Useful quantities

    
%   Spacecraft state vector
    rsb = x(1:3);     % Pos. vector wrt central body
    vsb = x(4:6);     % Vel. vector wrt central body
   
%%  Phobos's orbit

    RPh         = x(7);
    RPh_dot     = x(8);
    theta_dot   = x(10);
    Phi_dot     = x(12);
    Xsi         = x(13);
    Xsi_dot     = x(14);
    
    IPhx_bar    = x(15);
    IPhy_bar    = x(16);
    IPhz_bar    = x(17);



%%  Phobos's orbit
    
%   Potential's first order parstials
    dVdRPh      = pars.ni/RPh^2*(1 + 3/(2*RPh^2)*((pars.IMz_bar - pars.Is) -...
            .5*IPhx_bar - .5*IPhy_bar + IPhz_bar + ...
            1.5*(IPhy_bar - IPhx_bar)*cos(2*Xsi)));
    dVdXsi      = 1.5*pars.ni/RPh^3 * (IPhy_bar - IPhx_bar)*sin(2*Xsi);

%   Phobos equations of motions
    RPh_ddot    = theta_dot^2*RPh - dVdRPh/pars.ni;
    theta_ddot  = dVdXsi/(pars.ni*RPh^2) - 2*RPh_dot*theta_dot/RPh;
    Phi_ddot    = - dVdXsi/(pars.ni*RPh^2) + 2*RPh_dot*theta_dot/RPh;
    Xsi_ddot    = - (1 + pars.ni*RPh^2/IPhz_bar)*dVdXsi/(pars.ni*RPh^2) +...
                    + 2*RPh_dot*theta_dot/RPh; 
        
    
%%  MMX 2BP with Mars + 3rd body Phobos 
    

%   Position of Mars wrt Phobos
    rPh = RPh*[cos(-Xsi); sin(-Xsi); 0];

%   The gravitational effect of Mars on MMX is 2BP+J2
    
    r       = rsb - rPh; 
    R       = norm(r);
    gM1     = - pars.G*pars.MMars*r/R^3;
    gJ2M    = - 1.5*pars.G*pars.MMars*pars.J2*pars.RMmean^2/R^5*...
        [(1-5*(r(3)/R)^2)*r(1); (1-5*(r(3)/R)^2)*r(2);...
        (3-5*(r(3)/R)^2)*r(3)];

    gM      = gM1 + gJ2M - pars.G*pars.MMars*rPh/norm(rPh)^3;
    
%   The gravitational effect of Phobos on MMX includes the Ellipsoid
%   harmonics

    C_20_Ph     = .25*(IPhy_bar - IPhx_bar);
    C_22_Ph     = -.5*(2*IPhz_bar - IPhx_bar - IPhy_bar);

    pars.SH.Clm  = [1, 0, 0; 0, 0, 0; C_20_Ph, 0, C_22_Ph]./pars.SH.Norm(1:3,1:3);
    [gPh,~,~,~] = CGM(rsb, pars.SH);


%   MMX motion    
    W       = [0; 0; theta_dot + Xsi_dot];
    W_dot   = [0; 0; theta_ddot + Xsi_ddot];
    
    r_dot   = vsb;
    v_dot   = gPh - cross(W_dot,rsb) - 2 * cross(W,vsb) - cross(W,cross(W,rsb)) + gM;


%%  Output for the integrator
    
    dx = [r_dot; v_dot; RPh_dot; RPh_ddot; theta_dot; theta_ddot;...
        Phi_dot; Phi_ddot; Xsi_dot; Xsi_ddot; 0; 0; 0];
    
    
end