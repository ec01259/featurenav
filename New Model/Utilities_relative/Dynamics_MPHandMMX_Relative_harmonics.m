function dx = Dynamics_MPHandMMX_Relative_harmonics(~,x,pars,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = Dynamics_MPHandMMX_Relative_harmonics(t,x,parss,units)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .parss     Problem parsameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational parsameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%%  Useful quantities

    
%   Spacecraft state vector
    rsb = x(1:3);     % Pos. vector wrt central body
    vsb = x(4:6);     % Vel. vector wrt central body

    Rsb = norm(rsb);                        % Norm of pos. vector wrt central body
   
%%  Phobos's orbit

    RPh         = x(7);
    RPh_dot     = x(8);
    Xsi         = x(9);
    Xsi_dot     = x(10);
    
    IPhx_bar    = pars.IPhx_bar;
    IPhy_bar    = pars.IPhy_bar;
    IPhz_bar    = pars.IPhz_bar;

%   Position of Mars wrt Phobos
    rPh = RPh*[cos(-Xsi); sin(-Xsi); 0];
    

%%  Phobos's orbit
    
%   Potential's first order parstials
    dVdRPh      = pars.ni/RPh^2*(1 + 3/(2*RPh^2)*((pars.IMz_bar - pars.Is) -...
            .5*IPhx_bar - .5*IPhy_bar + IPhz_bar + ...
            1.5*(IPhy_bar - IPhx_bar)*cos(2*Xsi)));
    dVdXsi      = 1.5*pars.ni/RPh^3 * (IPhy_bar - IPhx_bar)*sin(2*Xsi);
    Iz          = IPhz_bar + pars.ni*RPh^2;

%   Phobos equations of motions
    RPh_ddot    = (pars.K - IPhz_bar*Xsi_dot)^2*RPh/Iz^2 - dVdRPh/pars.ni;
    Xsi_ddot    = -(1 + pars.ni*RPh^2/IPhz_bar)*dVdXsi/(pars.ni*RPh^2) +...
                    + 2*(pars.K - IPhz_bar*Xsi_dot)*RPh_dot/(RPh*Iz);
        
    
%%  MMX 2BP with Mars + 3rd body Phobos 
    
%   The gravitational effect of Mars on MMX is 2BP+J2
    
    r       = rPh - rsb; 
    R       = norm(r);
    gM1     = pars.G*pars.MMars*r/R^3;
    gJ2M    = 1.5*pars.G*pars.MMars*pars.J2*pars.RMmean^2/R^5*...
        [(1-5*(r(3)/R)^2)*r(1); (1-5*(r(3)/R)^2)*r(2);...
        (3-5*(r(3)/R)^2)*r(3)];
    
    gT = pars.G*pars.MMars*rPh/RPh^3;
   
    gM      = gM1 + gJ2M - gT;
    
%   The gravitational effect of Phobos on MMX includes the Ellipsoid
%   harmonics

    % [gPh,~,~,~]  = CGM(rsb, pars.SH);
    [gPh,~,~,~]  = SHGM_Full(rsb, pars.SH);

%   Combined effect on MMX   
    theta_dot = (pars.K - IPhz_bar*Xsi_dot)/Iz;
    theta_ddot  = dVdXsi/(pars.ni*RPh^2) - 2*RPh_dot*theta_dot/RPh;
    W       = [0; 0; theta_dot + Xsi_dot];
    W_dot   = [0; 0; theta_ddot + Xsi_ddot];
    
    g_tot   = gPh + gM - cross(W_dot,rsb) - 2 * cross(W,vsb) - cross(W,cross(W,rsb));


%%  Output for the integrator
    
    % dx = [vsb; g_tot; RPh_dot; RPh_ddot; Xsi_dot; Xsi_ddot; 0; 0; 0];
    dx = [vsb; g_tot; RPh_dot; RPh_ddot; Xsi_dot; Xsi_ddot];
    
    
end