function stop = Plot_InitialCondition(x,~,~,pars,units)


    % dynamics = @Dynamics_MPHandMMX_Relative_ridotta;
    dynamics = @Dynamics_MPHandMMX_Relative_harmonics;
    
    stop = false;

    [Ph, pars]  = RealPhobos_States_NewModel(pars.et0,pars);
    Ph          = Ph./units.sfVec2;
    Geom = pars.Geom;

    RelTol  = 1e-13;
    AbsTol  = 1e-16;
    St0     = [Ph(1:2); Ph(7:8)];
    tspanp      = x(end,:); 
    opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X_Ph0]   = ode113(@(t,X) MPH_Relative(t,X,pars),tspanp,St0,opt);
    X_Ph0 = X_Ph0(:,1:4)';


    for j = 1:size(x,2)
    
        X_0opt  = [x(1:6,j); X_Ph0(1:4,j)]; 
        tspanp  = [0,x(8,j)];
        tspanm  = [0,x(7,j)];
        opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,pars,units));
        [~,Xp] = ode113(@(t,X) dynamics(t,X,pars,units),tspanp,X_0opt,opt);
        [~,Xm] = ode113(@(t,X) -dynamics(t,X,pars,units),tspanm,X_0opt,opt);
    
        X_QSO           = zeros(3,size(Xp,1)+size(Xm,1));
    
    %   Braccio in avanti dal patch point
    
        for i = 1:size(Xp,1)
    
            X_QSO(:,i+size(Xm,1)) = Xp(i,1:3);
    
        end
    
    %   Braccio indietro dal patch point
    
        for i = 1:size(Xm,1)
    
            X_QSO(:,size(Xm,1)-i+1) = Xm(i,1:3);
    
        end
    
    
        X_QSO_patch   = zeros(3,size(x,2));
    
        for i = 1:size(x,2)
    
            X_QSO_patch(:,i) = x(1:3,i);
    
        end
    

        figure(10)
        plot3(X_QSO(1,:)*units.lsf,X_QSO(2,:)*units.lsf,X_QSO(3,:)*units.lsf);
        hold on;
        grid on;
        plot3(X_QSO_patch(1,j)*units.lsf,X_QSO_patch(2,j)*units.lsf,X_QSO_patch(3,j)*units.lsf,'*');
        planetPlot('Asteroid',[0;0;0],Geom,1);
        xlabel('x [km]');
        ylabel('y [km]');
        zlabel('z [km]');
        axis equal

    
    end 

    hold off
        
end