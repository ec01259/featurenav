clear
close all
clc

%%  Definition of the points to start the continuation for the QSO

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath(genpath('../'));
    addpath(genpath('../../../paper/'));
    addpath(genpath('../../../paper/MMX_Fcn_Miscellaneous/'));
    addpath(genpath('../../../paper/MMX_Product/'));
    addpath(genpath('../../../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../../../paper/Reference_Definition/'));
    addpath(genpath('../../../mice/'));
    addpath(genpath('../../../generic_kernels/'));
    addpath(genpath('../../../useful_functions/'));
    addpath(genpath('../../../dynamical_model/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));

%%  Patch points

    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Geom.alpha  = alpha;
    Geom.beta   = beta;
    Geom.gamma  = gamma;
    muPh        = 7.1139e-4;                      %km^3/s^2
    muM         = 4.2828e4;                       %km^3/s^2
    pPh         = 9377.2;                         %km 
    omega       = sqrt((muM + muPh)/pPh^3);     %rad/s
    eps         = (muPh/muM)^(1/3);
    
    L           = eps*(sqrt(muM)/omega)^(2/3);
    T           = 1/omega;
    
%   Adimensionalized


    Phobos      = struct('alpha',alpha/L,'beta',beta/L,'gamma',gamma/L,...
                    'mu',muPh/L^3*T^2,'omega',omega*T);  

%%    Da runnare una sola volta

    % r0 = [100;0;0]/L;           % km
    % V0 = [0;-2*r0(1);0];        % km/s
    % 
    % par.ds          = 1e-3;     % user-defined step-length parameter
    % par.dsMax       = 1e-1;     % maximum value for step-length parameter...see line 94
    % par.Iter        = 15;       % Max. no. of Newton's Method iterations
    % par.Nmax        = 170;      % No. of desired orbits
    % par.Opt         = 5;        % Optimal no. of Newton's Iteration - Useful for adaptive step-length...see line 94
    % par.Tol         = 1e-10;     % Newton's method tolerance
    % 
    % X0              = [r0;V0];
    % T0              = 2*pi;
    % dX0_tilde_ds    = [-1;0;0;0;2;0]/sqrt(5);
    % dT_tilde_ds     = 0;
    % dlam_tilde_ds   = 0;
    % 
    % [Xpo,Tpo,Lampo,p,q,Lam,bif] = Predictor_Corrector(X0,T0,@HillProb_Sphere_Nd,@landing_Nd,dX0_tilde_ds,dT_tilde_ds,dlam_tilde_ds,Phobos,par);
    % save('Xpo','Xpo')
    % save('Tpo','Tpo')

%%  QSOs Patch points


% %   Portforlio di orbite
%     load("Xpo.mat")
%     load("Tpo.mat")
%     n_rev = 5;
% 
% %   QSO-H
%     % idx = 1;
% 
% %   QSO-M
%     % idx = 53;
% 
% %   QSO-La
%     idx = 79;
% 
% % %   QSO-Lb
%     % idx = 86;
% 
% %   QSO-Lc
%     % idx = 90;
% 
% %   Definition patch points
%     Phi0        = eye(6);
%     X0          = Xpo(:,idx);
%     theta0      = zeros(6,1);
%     X0          = reshape([X0 Phi0 theta0],[48,1]);
%     tspan       = [0 n_rev*Tpo(idx)];
%     ODE.T       = 1;
%     ODE.lambda  = 0;
%     RelTol      = 1e-13;
%     AbsTol      = 1e-16;
%     opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event', @(t,X) Patch_points(t,X));
%     [t,X,te,Xe] = ode113(@(t,X) HillProb_Sphere_Nd(t,X,Phobos,ODE),tspan,X0,opt);
%     X           = X(:,1:6).*[1,1,1,T,T,T]*L;
% 
% %     Xe          = (Xe(:,1:6).*[1,1,1,1/T,1/T,1/T]*L)';
% %     te          = te(:,1)*T;
% 
%     Xe          = (Xe(1:2:end,1:6).*[1,1,1,1/T,1/T,1/T]*L)';
%     te          = te(1:2:end,1)*T;
% 
% %   Patch points
%     X_patch     = Xe(1:3,:);
%     V_patch     = Xe(4:6,:);
%     t_patch     = te;
% 
% %   Plot
%     figure()
%     hold on
%     grid on
%     axis equal
%     planetPlot('asteroid',[0,0,0],Geom,1);
%     xlabel('X')
%     ylabel('Y')
%     plot3(X(:,1),X(:,2),X(:,3));
%     plot3(Xe(1,:),Xe(2,:),Xe(3,:),'r*')




%%  3D Patch points

%   % Qualche costante utile
%     alpha       = 13.03;                          %km
%     beta        = 11.4;                           %km
%     gamma       = 9.14;                           %km
%     Geom.alpha  = alpha;
%     Geom.beta   = beta;
%     Geom.gamma  = gamma;
% 
% %   Qualche parametro e units utile
%     [pars, units] = MMX_DefineModelParametersAndUnits([2,2]);
% 
%     inp.Model.dynamics  = @CRTBPsh;
%     inp.Model.pars      = pars;
%     inp.Model.units     = units;
%     inp.Model.epoch     = '2026-03-16, 00:00:00 (UTC)';
%     inp.OrbitType       = '3D-QSO';
%     inp.Event           = @Null_Radial_velocity;
% 
% %   QSO-H
%     inp.Amplitudes      = [198, 0];
% %   QSO-M
%     % inp.Amplitudes      = [94, 0];
% %   QSO-La
%     % inp.Amplitudes      = [49, 0];
% %   QSO-Lb
%     % inp.Amplitudes      = [31, 0];
% %   QSO-Lb
%     % inp.Amplitudes      = [27, 0];
% 
% 
% %   Propagazione e caricamento dei dati della orbita di interesse
%     filename    = sprintf('3DQSOs_%d_%dx%d.mat',round(inp.Amplitudes(1)),...
%         size(inp.Model.pars.SH.Clm,1)-1, size(inp.Model.pars.SH.Clm,2)-1);
%     load(filename, 'Xqp', 'Wqp', 'GMOS', 'Model');
% 
% 
% %   Integrazione e identificazione dei patch points    
%     idx = 10;
%     Ic      = Xqp(1:6,idx);
%     Model.pars.T    = Wqp(1,idx)*units.tsf;
%     N_rev   = 5;
%     Time    = [0, N_rev*Model.pars.T/units.tsf];
%     AbsTol  = 1e-16;
%     RelTol  = 1e-14;
%     opt     = odeset('RelTol', RelTol, 'AbsTol', AbsTol, 'event',...
%         @Null_Radial_velocity);
%     [t,x,te,xe] = ode113(@eom, Time, Ic, opt, Model);
%     t_patch     = te(1:2:end)*units.tsf;
%     X_patch     = xe(1:2:end,1:3)'.*units.lsf;
%     V_patch     = xe(1:2:end,4:6)'.*units.vsf;


%%  SWING Patch points

%   Qualche costante utile
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Geom.alpha  = alpha;
    Geom.beta   = beta;
    Geom.gamma  = gamma;

%   Qualche parametro e units utile
    [pars, units] = MMX_DefineModelParametersAndUnits([1,1]);

    inp.Model.dynamics  = @CRTBPsh;
    inp.Model.pars      = pars;
    inp.Model.units     = units;
    inp.Model.epoch     = '2026-03-16, 00:00:00 (UTC)';
    inp.OrbitType       = 'Swing-QSO';
    inp.Event           = @Null_Radial_velocity;

%   QSO-H
    % inp.Amplitudes      = [198, 0];
%   QSO-M
    % inp.Amplitudes      = [94, 0];
%   QSO-La
    inp.Amplitudes      = [49, 0];
%   QSO-Lb
    % inp.Amplitudes      = [31, 0];
%   QSO-Lc
    % inp.Amplitudes      = [27, 0];


%   Propagazione e caricamento dei dati della orbita di interesse
    filename    = sprintf('SwingQSOs_0%d_%dx%d.mat',round(inp.Amplitudes(1)),...
        size(inp.Model.pars.SH.Clm,1)-1, size(inp.Model.pars.SH.Clm,2)-1);
    load(filename, 'Xqp', 'Wqp', 'Model');

%   Integrazione e identificazione dei patch points
    idx     = 7;
    Ic      = Xqp(1:6,idx);
    Model.pars.T    = Wqp(1,idx)*units.tsf;
    N_rev   = 5;
    Time    = [0, N_rev*Model.pars.T/units.tsf];
    AbsTol  = 1e-16;
    RelTol  = 1e-14;
    opt     = odeset('RelTol', RelTol, 'AbsTol', AbsTol, 'event',...
        @Null_Radial_velocity);
    [t,x,te,xe] = ode113(@eom, Time, Ic, opt, Model);

    % t_patch     = te(1:2:end)*units.tsf;
    % X_patch     = xe(1:2:end,1:3)'.*units.lsf;
    % V_patch     = xe(1:2:end,4:6)'.*units.vsf;

    t_patch     = te*units.tsf;
    X_patch     = xe(:,1:3)'.*units.lsf;
    V_patch     = xe(:,4:6)'.*units.vsf;

%   Plot orbita e patch points
    h = figure();
    PlotTrajectories(h, x, inp.Model);
    hold on;
    plot3(X_patch(1,:),X_patch(2,:),X_patch(3,:),'ro')
    set(0,'DefaultFigureVisible', 'on')



%%  Phobos states

%   Now, this points must be defined with respect to the new model. So we
%   integrate Phobos with the new model, and add the MMX postition with 
%   respect to this new Phobos

%   kernel with the real position of Phobos
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));

%   Model parameters
    % [pars, units] = MMX_DefineNewModelParametersAndUnits_Right;
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;

%   Initial state
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    pars.et0    = data;
    [Ph,pars]   = Phobos_States_NewModel(data,pars);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;
    Iz      = pars.IPhz_bar + pars.ni*Ph0(1)^2;
    K       = Iz*Ph0(4) + pars.IPhz_bar*Ph0(8);
    pars.K  = K;
    

%   Initial Phobos's state vector
    St0     = [Ph(1:2); Ph(7:8)];

%   Integration
    tspanp      = t_patch*units.tsf; 
    opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [tp,XPhp]   = ode113(@(t,X) MPH_Relative(t,X,pars),tspanp,St0,opt);
    tp          = tp/units.tsf;

 

%%  Setup della optimization per la definizione dell'orbita periodica nel nuovo modello

%   Definition of the initial vector
    X0      = [X_patch/units.lsf; V_patch/units.vsf;...
                (t_patch(2)-t_patch(1))/2*ones(2,size(t_patch,1))*units.tsf;
                t_patch'*units.tsf];
    X_Ph0   = XPhp(:,1:end)';
    pars.Geom   = Geom;
    pars.X0     = X_patch/units.lsf;

%%  Propagazione della vecchia initial condition
    
  [~] = Plot_InitialCondition(X0,[],[],pars,units);


%%  Ottimizzazione 


    options = optimoptions('fmincon','Display','iter','Algorithm','interior-point',...
        'ConstraintTolerance',1e-9,...
        'MaxFunctionEvaluations',1e4,'EnableFeasibilityMode',true,...
        'OutputFcn',@(x,optimValues,state)Plot_InitialCondition(x,optimValues,state,pars,units));
    X_good  = fmincon(@(x) CostFunction(x,pars,units),X0,[],[],[],[],[],[], ...
        @(x) Constraints(x,pars,units,X_patch),options);
    

%   File da salvare
    filename = 'SwingQSO-La_5rev_4x4.mat';
    save(filename,'X_good')
    movefile(filename, '../Initial_cond_perBSP/');


%%  Plot della nuova initial condition found

    [~] = Plot_InitialCondition(X_good,[],[],pars,units);

