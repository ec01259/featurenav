function [value,isterminal,direction] = landing_Phobos_relative_UKF_ridotto(~,X,pars,units)


    d           = pars.d;                    % Dimension of the problem
    size_St     = pars.sizeSt;

    for i = 0:2*size_St-1

%       Landing on Phobos? Integration is blocked if the spacecraft touch the ellipsoid 
        alpha   = pars.Phobos.alpha/units.lsf;                 % Phobos' largest semi-axis (km) 
        beta    = pars.Phobos.beta/units.lsf;                  % Phobos' intermediate semi-axis (km)
        gamma   = pars.Phobos.gamma/units.lsf;                 % Phobos' smallest semi-axis (km)
        
        x       = X(1+i*size_St:3+i*size_St);
        value1  = x(1)^2/alpha^2 + x(2)^2/beta^2 +...
            x(3)^2/gamma^2 - 1;
    
%       Landing on Mars?
        alpha   = pars.Mars.alpha/units.lsf;                 % Mars' largest semi-axis (km) 
        beta    = pars.Mars.beta/units.lsf;                  % Mars' intermediate semi-axis (km)
        gamma   = pars.Mars.gamma/units.lsf;                 % Mars' smallest semi-axis (km)
        
        rPh     = X(7+i*size_St)*[cos(-X(9+i*size_St)); sin(-X(9+i*size_St)); 0];
        value2  = (x(1) - rPh(1))^2/alpha^2 + (x(2) - rPh(2))^2/beta^2 +...
            (x(3) - rPh(3))^2/gamma^2 - 1;
        
        d = value1*value2;

        if d<=0
            fprintf('\nOne of the sigma point fell on Phobos or Mars...')
            break;
        end

    end

    value = d;
    isterminal = 1;
    direction = 0;

    

end