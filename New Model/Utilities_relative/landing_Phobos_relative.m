function [value,isterminal,direction] = landing_Phobos_relative(~,X,par,units)


%   Landing on Phobos? Integration is blocked if the spacecraft touch the ellipsoid 
    alpha   = par.Phobos.alpha/units.lsf;                 % Phobos' largest semi-axis (km) 
    beta    = par.Phobos.beta/units.lsf;                  % Phobos' intermediate semi-axis (km)
    gamma   = par.Phobos.gamma/units.lsf;                 % Phobos' smallest semi-axis (km)
    
    x       = X(1:3) ;
    value1  = x(1)^2/alpha^2 + x(2)^2/beta^2 +...
        x(3)^2/gamma^2 - 1;

%   Landing on Mars?
    alpha   = par.Mars.alpha/units.lsf;                 % Mars' largest semi-axis (km) 
    beta    = par.Mars.beta/units.lsf;                  % Mars' intermediate semi-axis (km)
    gamma   = par.Mars.gamma/units.lsf;                 % Mars' smallest semi-axis (km)
    
    rPh     = X(7)*[cos(-X(13)); sin(-X(13)); 0];
    value2  = (x(1) - rPh(1))^2/alpha^2 + (x(2) - rPh(2))^2/beta^2 +...
        (x(3) - rPh(3))^2/gamma^2 - 1;
    
    value = value1*value2;
    isterminal = 1;
    direction = 0;

end