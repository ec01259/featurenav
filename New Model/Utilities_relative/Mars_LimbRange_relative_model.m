function Limb_Range = Mars_LimbRange_relative_model(MMX, Mars, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = Mars_LimbRange_model(MMX, Phobos, Sun, Phi, pars, units)
%
% Identify wether Mars is in the FOV and how far is it
%
% Input:
%     
% MMX           MMX state vector in the MARSIAU reference frame
% Phobos        Phobos state vector in the MARSIAU reference frame
% Sun           Sun state vector in the MARSIAU reference frame
% pars          parameters structure
% units         units structure
% 
% Output:
% 
% Limb_Range    Range derived from Mars limb size
% 
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%   MMX-Phobos direction in the rotating reference frame
    r_sb_Ph = MMX(1:3);
    r_sM    = r_sb_Ph - Mars(1:3);

%   Mars pole position in the Phobos rotating frame
    Mars_pole   = [0; 0; -pars.Mars.gamma];
    R_ini       = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Mars_pole   = Mars_pole - Mars(1:3);
    Mars_centre = - Mars(1:3);


    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);
    MP  = PM*[Mars_pole; 1];
    MP  = MP./repmat(MP(3),3,1);
    MC  = PM*[Mars_centre; 1];
    MC  = MC./repmat(MC(3),3,1);

    Limb_radius = MP - MC;
    Limb_Range  = sqrt(Limb_radius(1)^2 + Limb_radius(2)^2);

    alpha       = atan2(-pars.Mars.gamma,norm(r_sM));
    Limb_Range  = pars.cam.f*tan(alpha)*pars.cam.nPixX/pars.cam.SixeX;
    % Plotfeatures_Pic(MC, MP, pars);



end