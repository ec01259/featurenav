function YObs = Observables_relative(date_0, date_end, file_features,...
    file_features_Mars, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% YObs = Observations_with_features(date_0, date_end, file_features,...
    % file_features_Mars, pars, units)
%
% Creation of the structure containing the timestamp, of the observables
% and the available observables to be processed
%
% Input:
%     
% date0                 Date of the beginning of the analysis
% date_end              Date of the end of the analysis
% file_features         Name of the file containing the Phobos features positions in
%                       the body-fixed reference frame
% file_features_Mars    Name of the file containing the Mars features positions in
%                       the body-fixed reference frame
% pars                  Struct with useful parameters
% units                 Struct with useful units 
%
% Output:
% 
% Obs                   Struct of observations and time of observations
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%   Generation properties
    
    elevation       = pars.elevation;
    LIDARlimit      = pars.cutoffLIDAR;

    interval_Range      = pars.interval_Range;
    interval_Range_rate = pars.interval_Range_rate;
    interval_lidar      = pars.interval_lidar;
    interval_features   = pars.interval_features;
    interval_limbs      = pars.interval_limbs;
    interval_ISL        = pars.interval_ISL;
    
%   Noise in the Observations
    sigma_range      = pars.ObsNoise.range*units.lsf;
    sigma_range_rate = pars.ObsNoise.range_rate*units.vsf;
    sigma_lidar      = pars.ObsNoise.lidar*units.lsf;
    sigma_ISL        = pars.ObsNoise.ISL*units.lsf;
    
%   Minimum interval between two different observables of the same kind
    min_interval    = min([interval_Range; interval_Range_rate;...
        interval_lidar; interval_ISL ;interval_features; interval_limbs]);


%%  Needed to find the Phobos libration angle at different timestamps

    [Ph, pars]  = Phobos_States_NewModel(date_0,pars);
    Ph          = Ph./units.sfVec2;
    St0     = [Ph; reshape(eye(pars.d2),[pars.d2^2,1])];
    tspan   = (0:min_interval:date_end-date_0)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X]   = ode113(@(t,X) MP_CoupledLibration(t,X,pars),tspan,St0,opt);
    
    t       = (0:min_interval:date_end-date_0);
    RPh_t   = X(:,1);
    Xsi_t   = X(:,7);
    Phi_t   = X(:,5);

%%  Observations

    et0        = date_0;
    et_end     = date_end;

%   Inizzializzazione struttura
    YObs       = [];
    
    for i = et0:min_interval:et_end
        
        got_it  = 0; 
        Xsi     = Xsi_t(t == (i-et0));
        RPh     = RPh_t(t == (i-et0));
        Phi_M   = Phi_t(t == (i-et0));
        PB      = [pars.PB, zeros(3,3); zeros(3,3), pars.PB];

%--------------------------------------------------------------------------
%       LIDAR OBSERVABLES WRT PHOBOS SURFACE
%--------------------------------------------------------------------------

%       Position of MMX wrt Phobos
        MMX     = PB*cspice_spkezr('-35', i, 'IAU_Phobos', 'none', '-401');

        if (norm(MMX(1:3)) < LIDARlimit) && (rem((i-et0),interval_lidar)==0) && pars.flag_Lidar
            
           r_bf = MMX(1:3);
           
           lat  = asin(r_bf(3)/norm(r_bf));
           lon  = atan2(r_bf(2), r_bf(1));

%          Phobos radius as function of latitude and longitude
           alpha = pars.Phobos.alpha;
           beta  = pars.Phobos.beta;
           gamma = pars.Phobos.gamma;
           
           R_latlon = (alpha*beta*gamma)/sqrt(beta^2*gamma^2*cos(lon)^2 +...
               gamma^2*alpha^2*sin(lon)^2*cos(lat)^2 + alpha^2*beta^2*sin(lon)^2*sin(lat));

           Lidar = norm(r_bf) - R_latlon + random('Normal',0, sigma_lidar);

           got_it   = 1;
        else
           Lidar    = [];
        end




%--------------------------------------------------------------------------
%       ROVER INTERSATELLITE LINK PHOBOS SURFACE
%--------------------------------------------------------------------------

%      Position of MMX wrt Phobos
        MMX     = PB*cspice_spkezr('-35', i, 'IAU_Phobos', 'none', '-401');
        D       = MMX(1:3) - pars.X_rover;
        v       = pars.X_rover/norm(pars.X_rover);

        if (dot(v,D)/norm(D)>(cos(elevation*pi/180)))&&(rem((i-et0),interval_ISL)==0) && pars.flag_ISL         
           
           ISLink = norm(MMX(1:3) - pars.X_rover) + random('Normal',0, sigma_ISL);

           got_it   = 1;
        else
           ISLink    = [];
        end


%--------------------------------------------------------------------------
%       DEEP SPACE NETWORK
%--------------------------------------------------------------------------

%       Position of MMX wrt J2000 Earth-centered
        MMX = cspice_spkezr('-35', i, 'J2000', 'none', 'EARTH');
        
%       Check on MMX being behind Mars or Phobos
        check   = cspice_occult('-35','POINT',' ',...
         '499','ELLIPSOID','IAU_MARS','none','EARTH',i);
        
%       Position of Camberra's GS
        [X_st1, ~]  = cspice_spkezr('DSS-45', i, 'J2000', 'none', 'EARTH');
        D           = MMX(1:3) - X_st1(1:3);
        v1          = X_st1(1:3)/norm(X_st1(1:3));
        
        
        if (check >= 0)&&(dot(v1,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range)==0) && pars.flag_DSN
            R_Cam   = norm(MMX(1:3) - X_st1(1:3)) + random('Normal',0, sigma_range);
        else
            R_Cam   = NaN;
        end

        if (check >= 0)&&(dot(v1,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range_rate)==0) && pars.flag_DSN
            r       = norm(MMX(1:3) - X_st1(1:3)) + random('Normal',0, sigma_range);
            R_dot_Cam = 1/r*sum((MMX(4:6) - X_st1(4:6)).*(MMX(1:3) - X_st1(1:3))) + random('Normal',0, sigma_range_rate);
            got_it  = 1;            
        else
            R_dot_Cam = NaN;
        end
        
           
%       Position of Goldstone's GS
        [X_st2, ~]  = cspice_spkezr('DSS-24', i, 'J2000', 'none', 'EARTH');
        D           = MMX(1:3) - X_st2(1:3);
        v2          = X_st2(1:3)/norm(X_st2(1:3));
        
        if (check >= 0)&&(dot(v2,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range)==0) && pars.flag_DSN
            R_Gold  = norm(MMX(1:3) - X_st2(1:3)) + random('Normal',0, sigma_range);
        else
            R_Gold  = NaN;
        end

        if (check >= 0)&&(dot(v2,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range_rate)==0) && pars.flag_DSN
            r       = norm(MMX(1:3) - X_st2(1:3)) + random('Normal',0, sigma_range);
            R_dot_Gold  = 1/r*sum((MMX(4:6) - X_st2(4:6)).*(MMX(1:3) - X_st2(1:3))) + random('Normal',0, sigma_range_rate);
            got_it  = 1;            
        else
            R_dot_Gold  = NaN;
        end

%       Position of Madrid's GS
        [X_st3, ~]  = cspice_spkezr('DSS-65', i, 'J2000', 'none', 'EARTH');
        D           = MMX(1:3) - X_st3(1:3);
        v3          = X_st3(1:3)/norm(X_st3(1:3));
        
        if (check >= 0)&&(dot(v3,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range)==0) && pars.flag_DSN
            R_Mad    = norm(MMX(1:3) - X_st3(1:3)) + random('Normal',0, sigma_range);
        else
            R_Mad    = NaN;
        end   

        if (check >= 0)&&(dot(v3,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range_rate)==0) && pars.flag_DSN
            r       = norm(MMX(1:3) - X_st3(1:3)) + random('Normal',0, sigma_range);
            R_dot_Mad   = 1/r*sum((MMX(4:6) - X_st3(4:6)).*(MMX(1:3) - X_st3(1:3))) + random('Normal',0, sigma_range_rate);
            got_it  = 1;            
        else
            R_dot_Mad   = NaN;
        end   
        

%--------------------------------------------------------------------------
%       CAMERA OBSERVABLE
%--------------------------------------------------------------------------

        
%       Is Mars between Phobos and the Sun?
        check3   = cspice_occult('-401','POINT',' ',...
         '499','ELLIPSOID','IAU_MARS','none','SUN',i);
        
        Sun     = PB*cspice_spkezr('10', i, 'IAU_Phobos', 'none', '-401');
        MMX     = PB*cspice_spkezr('-35', i, 'IAU_Phobos', 'none', '-401');

%       Check if Phobos is in light or not
        R_PS = -Sun(1:3)/norm(Sun(1:3));
        
        if (dot(MMX(1:3),R_PS)>0)&&(check3>=0)&&(rem(round(i-et0),interval_features)==0)&&pars.flag_features       


%           Identification of the features
            [~, Y_pix] = visible_features_relative(MMX, Sun, file_features, pars, units);

            if ~isempty(Y_pix)
                    got_it = 1;
            end

        else
            Y_LOS   = [];
            Y_pix   = [];
        end

        if (rem(round(i-et0),interval_limbs)==0)&&pars.flag_Limb

%           Mars position in the Phobos fixed refeence frame
            Mars    = PB*cspice_spkezr('499', i, 'IAU_Phobos', 'none', '-401');
            % Mars    = RPh*[cos(-Xsi); sin(-Xsi); 0]*units.lsf;

%           Is there Mars's Limb? 
            Limb    = Mars_LimbRange_relative(MMX, Mars, Sun, pars, units);
         
            if ~isempty(Limb)
                    got_it = 1;
            end

        else
            Limb = [];
        end


        if (rem(round(i-et0),interval_features)==0)&&pars.flag_features_Mars       

%           Mars position in the Phobos fixed refeence frame
            Mars    = PB*cspice_spkezr('499', i, 'IAU_Phobos', 'none', '-401');
            % Mars    = RPh*[cos(-Xsi); sin(-Xsi); 0]*units.lsf;

%           Identification of the features
            [~, Y_pix_Mars] = visible_features_Mars_relative(MMX, Mars, Sun,...
                Phi_M, file_features_Mars, pars, units);

            if ~isempty(Y_pix)
                    got_it = 1;
            end

        else
            Y_LOS_Mars  = [];
            Y_pix_Mars  = [];
        end


        if (rem(round(i-et0),interval_limbs)==0)&&pars.flag_Deimos
            
            
            Mars    = PB*cspice_spkezr('499', i, 'IAU_Phobos', 'none', '-401');
            % Mars    = RPh*[cos(-Xsi); sin(-Xsi); 0]*units.lsf;
            Deimos  = PB*cspice_spkezr('402', i, 'IAU_Phobos', 'none', '-401');
%           Is Deimos in the picture? 
            Deimos_obs  = Deimos_Pic_relative(MMX, Deimos, Mars, pars, units);
         
            % Mars_MMX = PB*cspice_spkezr('499', i, 'IAU_Phobos', 'none', '-35');
            % Deimos_MMX = PB*cspice_spkezr('402', i, 'IAU_Phobos', 'none', '-35');
            % 
            % i_M = Mars_MMX(1:3)/norm(Mars_MMX(1:3));
            % i_D = Deimos_MMX(1:3)/norm(Deimos_MMX(1:3));
            % Deimos_obs = acos(dot(i_M,i_D));

            if ~isempty(Deimos_obs)
                    got_it = 1;
            end
        else
            Deimos_obs = [];
        end
        

%--------------------------------------------------------------------------
%       NEW LINE SETUP
%--------------------------------------------------------------------------


        if (got_it == 1)
            Obs.t   = i-et0; 
            Range   = [R_Cam; R_Gold; R_Mad];
            if sum(~isnan(Range))>0
                Obs.Range = [R_Cam; R_Gold; R_Mad];
            else
                Obs.Range = [];
            end
            Rate    = [R_dot_Cam; R_dot_Gold; R_dot_Mad];
            if sum(~isnan(Rate))>0
                Obs.Rate = [R_dot_Cam; R_dot_Gold; R_dot_Mad];
            else
                Obs.Rate = [];
            end 
   
            Obs.Lidar       = Lidar;
            Obs.Features    = Y_pix;
            Obs.Limb        = Limb;
            Obs.Features_Mars   = Y_pix_Mars;
            Obs.Deimos      = Deimos_obs;
            Obs.ISL         = ISLink;
            YObs   = [YObs; Obs];
        end
    end
    
end