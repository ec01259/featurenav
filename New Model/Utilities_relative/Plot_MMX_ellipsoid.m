function Plot_MMX_ellipsoid(Est,pars,~)

    visibility = pars.Phobos.alpha*5e-3;

    % figure(10)
    % for i = 20:size(Est.t,2)
    %     plot3(Est.X_t(1,:),Est.X_t(2,:),Est.X_t(3,:));
    %     hold on;
    %     grid on;
    %     planetPlot('Asteroid',[0;0;0],pars.Phobos,1);
    %     xlim([-5*pars.Phobos.alpha,5*pars.Phobos.alpha]);
    %     ylim([-5*pars.Phobos.alpha,5*pars.Phobos.alpha]);
    %     zlim([-5*pars.Phobos.alpha,5*pars.Phobos.alpha]);
    %     xlabel('x [km]');
    %     ylabel('y [km]');
    %     zlabel('z [km]');
    %     axis equal;
    %     if 3*(sqrt(Est.P_t(1,1,i)))<visibility
    %         plot3(Est.X_t(1,i),Est.X_t(2,i),Est.X_t(3,i),'o');
    %     else
    %         ellipsoid(Est.X_t(1,i),Est.X_t(2,i),Est.X_t(3,i),...
    %             3*(sqrt(Est.P_t(1,1,i))),3*(sqrt(Est.P_t(2,2,i))),3*(sqrt(Est.P_t(3,3,i))));
    %     end
    %     pause(.2);
    %     hold off
    % end

    figure(11)
    plot3(Est.X_t(1,:),Est.X_t(2,:),Est.X_t(3,:));
    hold on;
    grid on;
    planetPlot('Asteroid',[0;0;0],pars.Phobos,1);
    xlim([-5*pars.Phobos.alpha,5*pars.Phobos.alpha]);
    ylim([-5*pars.Phobos.alpha,5*pars.Phobos.alpha]);
    zlim([-5*pars.Phobos.alpha,5*pars.Phobos.alpha]);
    xlabel('x [km]');
    ylabel('y [km]');
    zlabel('z [km]');
    axis equal;
    for i = 20:size(Est.t,2)
        if 3*(sqrt(Est.P_t(1,1,i)))<visibility
            plot3(Est.X_t(1,i),Est.X_t(2,i),Est.X_t(3,i),'o','Color','r');
        else
            ellipsoid(Est.X_t(1,i),Est.X_t(2,i),Est.X_t(3,i),...
                3*(sqrt(Est.P_t(1,1,i))),3*(sqrt(Est.P_t(2,2,i))),3*(sqrt(Est.P_t(3,3,i))));
        end
    end


end