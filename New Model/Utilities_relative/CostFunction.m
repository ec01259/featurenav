function DeltaV = CostFunction(X,pars,units)

    % dynamics = @Dynamics_MPHandMMX_Relative_ridotta;
    dynamics = @Dynamics_MPHandMMX_Relative_harmonics;

    [Ph, pars]  = Phobos_States_NewModel(pars.et0, pars);
    Ph          = Ph./units.sfVec2;

    RelTol  = 1e-13;
    AbsTol  = 1e-16;
    St0     = [Ph(1:2); Ph(7:8)];
    tspanp  = X(end,:); 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X_Ph0]   = ode113(@(t,X) MPH_Relative(t,X,pars),tspanp,St0,opt);
    X_Ph0 = X_Ph0(:,1:4)';

%   L'ottimizzazione minimizza il DeltaV richiesto nei punti in cui le
%   propagazioni si incontrano per far incontrare le traiettorie

    DeltaV  = 0;
    opt     = odeset('RelTol',1e-13,'AbsTol',1e-16,'event',@(t,X) landing_Phobos(t,X,pars,units));
    
    for i = 2:size(X,2)
        
        St1meno     = [X(1:6,i-1); X_Ph0(1:end,i-1)];
        tspan1meno  = [0, X(end-1,i-1)];
        [~,X1meno]  = ode113(@(t,X) dynamics(t,X,pars,units),tspan1meno,St1meno,opt);
    
        St1plus     = [X(1:6,i); X_Ph0(1:end,i)];
        tspan1plus  = [0, X(end-2,i)];
        [~,X1plus]  = ode113(@(t,X) -dynamics(t,X,pars,units),tspan1plus,St1plus,opt);
    
        DeltaV      = DeltaV + norm(X1meno(end,4:6)' - X1plus(end,4:6)')*units.vsf;
        
    end

end