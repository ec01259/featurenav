function [c, ceq] = Constraints(X,pars,units,X_patch)

    % dynamics = @Dynamics_MPHandMMX_Relative_ridotta;
    dynamics = @Dynamics_MPHandMMX_Relative_harmonics;
    
    [Ph, pars]  = Phobos_States_NewModel(pars.et0,pars);
    Ph          = Ph./units.sfVec2;

    RelTol  = 1e-13;
    AbsTol  = 1e-16;
    St0     = [Ph(1:2); Ph(7:8)];
    tspanp  = X(end,:); 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X_Ph0]   = ode113(@(t,X) MPH_Relative(t,X,pars),tspanp,St0,opt);
    X_Ph0 = X_Ph0(:,1:4)';

%   Non-linear constraint imposing that the two propagations of the two 
%   branches match 

%   Init dei constraints
    DeltaX          = zeros(6*(size(X,2)-2),1);
    Perp            = zeros((size(X,2))-2,1);
    times           = zeros((size(X,2)-1),1);
    boxPrimo_ultimo = zeros(6,1);
    box             = zeros(3*size(X,2),1);

    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,pars,units));
    

    for i = 2:size(X,2)
        
        St1meno     = [X(1:6,i-1); X_Ph0(1:end,i-1)];
        tspan1meno  = [0, X(end-1,i-1)];
        [~,X1meno]  = ode113(@(t,X) dynamics(t,X,pars,units),tspan1meno,St1meno,opt);
    
        St1plus     = [X(1:6,i); X_Ph0(1:end,i)];
        tspan1plus  = [0, X(end-2,i)];
        [~,X1plus]  = ode113(@(t,X) -dynamics(t,X,pars,units),tspan1plus,St1plus,opt);
    
        DeltaX(1+(i-2)*6:(6+(i-2)*6)) = (X1meno(end,1:6)' - X1plus(end,1:6)');
        
    end

    
%   Constraint imposing that position vector and velocity vector are 
%   perpendicular at patching points


    for i = 1:size(X,2)

        Perp(i) = dot(X(1:3,i), X(4:6,i));

        if i==1
            boxPrimo_ultimo(1:3) = abs(X_patch(:,i) - X(1:3,i)*units.lsf) - .1;
        elseif i==size(X,2)
            boxPrimo_ultimo(4:6) = abs(X_patch(:,i) - X(1:3,i)*units.lsf) - 1;
        end

        box(1+(i-1)*3:3+(i-1)*3) = abs(X_patch(:,i) - X(1:3,i)*units.lsf) - 1;

    end

 
%   Constraint imposing that forward and backward propagation times are
%   meeting somewhere between half orbit revolution time

    for i = 2:size(X,2)
        
        times(i-1) = (X(end,i) - X(end,i-1) - (X(end-2,i) + X(end-1,i-1)));

    end

    c   = [];
    ceq = [DeltaX; times];

end