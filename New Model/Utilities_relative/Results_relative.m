function Results_relative(Est, pars, units)
%==========================================================================
% Results_relative(Est, pars, units)
%
% This function plots the results of the covariance analysis
%
% INPUT: Description Units
%
% Est       - Estimation of:
%               .X, States at final t
%               .x, States' deviation vector at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .err, Post-fit residuals
%               .pre, Pre-fit residuals
%               .y_t, Pre-fit residuals
%               .X_t, States at different t
%               .x_t, States deviation vector at different t
%               .Phi_t, STM at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
% YObs_Full     Vector of observations and time of observations
%
% pars       - Structure containing problem's parameters
%
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 09/2021
%
%==========================================================================

    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

    t_obs   = Est.t;


%--------------------------------------------------------------------------


%   Plot of the pre and post fit residuals
    
    figure(1)
    subplot(2,4,1)
    plot(t_obs/3600,Est.pre(1,:)*units.lsf,'x','Color','b')
    grid on;
    hold on;
    plot(t_obs/3600,Est.pre(3,:)*units.lsf,'x','Color','b')
    plot(t_obs/3600,Est.pre(5,:)*units.lsf,'x','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Range Pre-fit residual')
    subplot(2,4,2)
    plot(t_obs/3600,Est.pre(2,:)*units.vsf,'x','Color','r')
    grid on;
    hold on;
    plot(t_obs/3600,Est.pre(4,:)*units.vsf,'x','Color','r')
    plot(t_obs/3600,Est.pre(6,:)*units.vsf,'x','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    xlabel('$[hour]$')
    ylabel('$[km/s]$')    
    title('RangeRate Pre-fit residual') 
    subplot(2,4,3)
    plot(t_obs/3600,Est.pre(7,:)*units.lsf,'x','Color','g')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    plot(t_obs/3600,-3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Lidar Pre-fit residual') 
    subplot(2,4,4)
    plot(t_obs/3600,Est.pre(8,:),'x','Color','m')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.pixel*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    plot(t_obs/3600,-3*pars.ObsNoise.pixel*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    xlabel('$[hour]$')
    ylabel('$[N. pixel]$')
    title('Camera Pre-fit residual') 

    subplot(2,4,5)
    plot(t_obs/3600,Est.err(1,:)*units.lsf,'x','Color','b')
    grid on;
    hold on;
    plot(t_obs/3600,Est.err(3,:)*units.lsf,'x','Color','b')
    plot(t_obs/3600,Est.err(5,:)*units.lsf,'x','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.err(1,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.err(1,:),2)),'.-','Color','b')
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Range post-fit residual')
    subplot(2,4,6)
    plot(t_obs/3600,Est.err(2,:)*units.vsf,'x','Color','r')
    grid on;
    hold on;
    plot(t_obs/3600,Est.err(4,:)*units.vsf,'x','Color','r')
    plot(t_obs/3600,Est.err(6,:)*units.vsf,'x','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.err(2,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.err(2,:),2)),'.-','Color','r')
    xlabel('$[hour]$')
    ylabel('$[km/s]$')    
    title('RangeRate post-fit residual') 
    subplot(2,4,7)
    plot(t_obs/3600,Est.err(7,:)*units.lsf,'x','Color','g')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.err(7,:),2)),'.-','Color','g')
    plot(t_obs/3600,-3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.err(7,:),2)),'.-','Color','g')
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Lidar post-fit residual') 
    subplot(2,4,8)
    plot(t_obs/3600,Est.err(8,:),'x','Color','m')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.pixel*ones(size(Est.err(8,:),2)),'.-','Color','m')
    plot(t_obs/3600,-3*pars.ObsNoise.pixel*ones(size(Est.err(8,:),2)),'.-','Color','m')
    xlabel('$[hour]$')
    ylabel('$[N. pixel]$')
    title('Camera post-fit residual') 


    
%--------------------------------------------------------------------------

    
%   Square-Root of the MMX covariance error

    sqx = squeeze(Est.P_t(1,1,1:end-1));
    sqy = squeeze(Est.P_t(2,2,1:end-1));
    sqz = squeeze(Est.P_t(3,3,1:end-1));
    SQRT_X = 3*sqrt(sqx + sqy + sqz);
    sqvx = squeeze(Est.P_t(4,4,1:end-1));
    sqvy = squeeze(Est.P_t(5,5,1:end-1));
    sqvz = squeeze(Est.P_t(6,6,1:end-1));
    SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);

   
%   3sigma Envelopes
    figure()
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
    xlabel('time $[hour]$','FontSize',26)
    ylabel('$[km]$','FontSize',26)
    title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(4,4,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(5,5,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(6,6,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
    xlabel('time $[hour]$','FontSize',26)
    ylabel('$[km/s]$','FontSize',26)
    title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',26)


    P_rel_RAC   = zeros(3,3,size(Est.X_t(9,:),2));
    V_rel_RAC   = zeros(3,3,size(Est.X_t(9,:),2));
    
    for i = 1:size(Est.X_t(1,:),2)

        P_rel_RAC(:,:,i) = Radial_Along_Cross_Covariance(Est.P_t(1:3,1:3,i), Est.X_t(1:6,i), pars, units);
        V_rel_RAC(:,:,i) = Radial_Along_Cross_Covariance(Est.P_t(4:6,4:6,i), Est.X_t(1:6,i), pars, units);

    end


    figure()
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_rel_RAC(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_RAC(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_RAC(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('MMX position $3\sigma$ in RAC directions','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{radial}$','$3\sigma_{along}$','$3\sigma_{cross}$','$3 RMS$','Interpreter','latex','FontSize',26)
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(V_rel_RAC(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(V_rel_RAC(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(V_rel_RAC(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    title('MMX position $3\sigma$ in RAC directions','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{radial}$','$3\sigma_{along}$','$3\sigma_{cross}$','$3 RMS$','Interpreter','latex','FontSize',26)
    

%--------------------------------------------------------------------------


%   Phobos's states uncertainties evolution
%   Phobos cartesian covaraiance

    


    if pars.flag_FILTERING == 1

        Phobos_real = cspice_spkezr('-401', pars.et0+t_obs, 'MARSIAU', 'none', 'MARS');
        err = abs(Phobos_real - X_Phobos);
    
    
        [Ph, pars]  = Phobos_States_NewModel(pars.et0,pars);
        Ph          = Ph./units.sfVec2;
        St0     = [MMX_real(:,1)./units.sfVec; Ph; pars.I2];
        tspan   = t_obs*units.tsf;
        RelTol  = 1e-13;
        AbsTol  = 1e-16; 
        opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,pars,units));
        [~,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,pars,units),tspan,St0,opt);
    
        
        if size(Est.X_t,1) == 20

            Phi_t   = X(:,13:14)'.*[1; units.tsf];
            err_Phi = abs(Phi_t - Est.X_t(11:12,:));
        
        %   3sigma Envelopes and error
            figure()
            subplot(2,4,1)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(1,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$x_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,2)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(2,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$y_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,3)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(3,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$z_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,4)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(11,11,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(1,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad]$')
            title('$\Phi_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,5)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhVXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(4,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vx_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,6)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(5,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vy_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,7)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(6,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vz_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,8)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(12,12,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(2,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad/s]$')
            title('$\dot{\Phi}_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            
        else

            Phi_t   = X(:,13:14)'.*[1; units.tsf];
            err_Phi = abs(Phi_t - Est.X_t(13:14,:));
        
        %   3sigma Envelopes and error
            figure()
            subplot(2,4,1)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(1,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$x_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,2)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(2,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$y_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,3)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(3,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$z_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,4)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(13,13,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(1,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad]$')
            title('$\Phi_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,5)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhVXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(4,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vx_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,6)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(5,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vy_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,7)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(6,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vz_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,8)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(14,14,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(2,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad/s]$')
            title('$\dot{\Phi}_{Ph}$ $3\sigma$ envelope and error','FontSize',30)

        end

     else
    
        

        %   3sigma Envelopes
            figure()
            subplot(2,2,1)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$R_{Ph}$ [km]')
            subplot(2,2,2)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(9,9,1:end-1)))),'Color','m','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\Phi_{Ph}$ [rad]')
            
            subplot(2,2,3)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(8,8,1:end-1)))),'Color','b','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{R}_{Ph}$ [km/s]')
            subplot(2,2,4)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(10,10,1:end-1)))),'Color','m','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{\Phi}_{Ph}$ [rad/s]')
        
         

     end


 %--------------------------------------------------------------------------

 
   
%   Harmonics 3sigma Envelopes

%     place0 = size(Est.X,1)-pars.nCoeff-pars.nBias;


    corr_label = {'$x$','$y$','$z$','$\dot{x}$','$\dot{y}$','$\dot{z}$',...
        '$R_{Ph}$','$\dot{R}_{Ph}$', '$\Phi_{Ph}$','$\dot{\Phi}_{Ph}$'};

%   Moments of inertia
%     figure()
%     subplot(1,3,1)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(15,15,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
    corr_label{end+1} = ['$I_{PhX}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhX}$','Interpreter','latex','FontSize',26)
%     subplot(1,3,2)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(16,16,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
    corr_label{end+1} = ['$I_{PhY}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhY}$','Interpreter','latex','FontSize',26)
%     subplot(1,3,3)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(16,16,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
    corr_label{end+1} = ['$I_{PhZ}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhZ}$','Interpreter','latex','FontSize',26)

 
%--------------------------------------------------------------------------


%   Biases

    place0 = size(Est.X,1)-pars.nBias;
    corr_label(end+1) = {'$\rho$'};
    corr_label(end+1) = {'$\dot{\rho}$'};
    corr_label(end+1) = {'$LIDAR_b$'};

    figure()
    subplot(1,pars.nBias,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+1,place0+1,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\rho$'),'Color','b');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    subplot(1,pars.nBias,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+2,place0+2,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\dot{\rho}$'),'Color','r');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    subplot(1,pars.nBias,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+3,place0+3,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$LIDAR_b$'),'Color','g');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')

    subplot(1,pars.nBias,4)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+4,place0+4,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$Features_x$'),'Color','m');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[pix]$')

    subplot(1,pars.nBias,5)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+5,place0+5,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$Features_y$'),'Color','k');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[pix]$')



%--------------------------------------------------------------------------



%   Harmonics Coefficients



    P_t_I2x  = squeeze(Est.P_t(13,13,1:end-1));
    P_t_I2y  = squeeze(Est.P_t(14,14,1:end-1));
    P_t_I2z  = squeeze(Est.P_t(15,15,1:end-1));
    Cov_I2xI2y = squeeze(Est.P_t(13,14,1:end-1));
    Cov_I2yI2z = squeeze(Est.P_t(14,15,1:end-1));
    Cov_I2xI2z = squeeze(Est.P_t(13,15,1:end-1));

    P_t_C_20 = .25*(P_t_I2y + P_t_I2x - 2*Cov_I2xI2y);
    P_t_C_22 = .5*(4*P_t_I2z + P_t_I2y + P_t_I2x - 4*Cov_I2xI2z - 4*Cov_I2yI2z + 2*Cov_I2xI2y);
    

    figure()
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20)),'LineWidth',1,'Color','b');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    legend('$C_{22}$','Interpreter','latex','FontSize',26)
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22)),'LineWidth',1,'Color','r');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    legend('$C_{20}$','Interpreter','latex','FontSize',26)

        
  
%--------------------------------------------------------------------------


%   Correlations coefficients

    % [~,corr] = readCovMatrix(real(Est.P));
    % 
    % figure();
    % imagesc(real(corr))
    % title('Correlation Coefficients','FontSize',16);
    % set(gca,'FontSize',16);
    % colormap(hot);
    % colorbar;
    % set(gca,'TickLabelInterpreter','latex');
    % set(gca,'XTick',(1:size(Est.X,1)));
    % set(gca,'XTickLabel',corr_label);
    % set(gca,'YTick',(1:size(Est.X,1)));
    % set(gca,'YTickLabel',corr_label);
    % axis square;
    % freezeColors;


