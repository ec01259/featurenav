clear 
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath(genpath('../'));
    addpath(genpath('./MMX_BSP_Files_Relative/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../useful_functions/planet_textures/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../../mice/'));
    addpath(genpath('../../computer-vision/'));
    addpath(genpath('../../paper/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../../paper/MMX_Product/MMX_BSP_Files_GravLib/'));
    addpath(genpath('./Utilities_relative/'))
    addpath(genpath('../Utilities/'))
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_031_4x4_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_031_013_4x4_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_031_013_4x4_826891269_829483269_relative_right.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    % cspice_furnsh(which('MMX_QSO_031_2x2_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_027_014_2x2_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_031_011_2x2_826891269_829483269_relative_right.bsp'));

%%  Initial conditions per la analisi

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    Ndays       = 1;
    t           = Ndays*86400;
    date_end    = data+t;

%   Model parameters
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;

%   Covariance analysis parameters
    pars.et0      = data;
    % [pars, units] = CovarianceAnalysisParameters_relative(pars, units);
    [pars, units] = CovarianceAnalysisParameters_harmonics(pars, units);
    file_features = 'Principal_Phobos_ogni10.mat';
    file_features_Mars = 'Mars_4facets.mat';
      
    
%%  Creazione lista osservabili

    YObs = Observables_relative(data, date_end, file_features,...
        file_features_Mars, pars, units);
    
    % save('/Users/ec01259/Documents/pythoncodes/tapyr/YObs');
    save('YObs_rel');