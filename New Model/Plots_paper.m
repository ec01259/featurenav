clear
close all
clc

addpath('./Results/');
addpath('../../useful_functions/');
addpath(genpath('../../mice/'));
addpath(genpath('../../generic_kernels/'));
addpath(genpath('../../paper/MMX_Fcn_CovarianceAnalyses/'));
MMX_InitializeSPICE
cspice_furnsh(which('mar097.bsp'));
set(0,'DefaultTextInterpreter','latex');
set(0,'DefaultAxesFontSize', 16);



%% Confronto DNS/noDSN e N. features

name1   = 'QSOLb_1day_Full_Allfeat.mat';
load(name1);
Est1    = Est;
name2   = 'QSOLb_1day_Full_2feat.mat';
load(name2);
Est2    = Est;
name3   = 'QSOLb_1day_Full_5feat.mat';
load(name3);
Est3    = Est;
name4   = 'QSOLb_1day_Full_10feat.mat';
load(name4);
Est4    = Est;

name5   = 'QSOLb_1day_NoDSN_Allfeat.mat';
load(name5);
Est5    = Est;
name6   = 'QSOLb_1day_NoDSN_2feat.mat';
load(name6);
Est6    = Est;
name7   = 'QSOLb_1day_NoDSN_5feat.mat';
load(name7);
Est7    = Est;
name8   = 'QSOLb_1day_NoDSN_10feat.mat';
load(name8);
Est8    = Est;
name9   = 'QSOLb_7day_Full_5feat.mat';
load(name9);
Est9    = Est;
name10  = 'QSOLb_7day_Deimos2_5feat.mat';
load(name10);
Est10   = Est;

name11  = 'QSOLb_1day_NoLimb_Allfeat.mat';
load(name11);
Est11   = Est;
name12  = 'QSOLb_1day_NoLimb_2feat.mat';
load(name12);
Est12   = Est;
name13  = 'QSOLb_1day_NoLimb_5feat.mat';
load(name13);
Est13   = Est;
name14  = 'QSOLb_1day_NoLimb_10feat.mat';
load(name14);
Est14   = Est;

name15   = 'QSOLb_1day_NoDeimos_Allfeat.mat';
load(name15);
Est15    = Est;
name16   = 'QSOLb_1day_NoDeimos_2feat.mat';
load(name16);
Est16    = Est;
name17   = 'QSOLb_1day_NoDeimos_5feat.mat';
load(name17);
Est17    = Est;
name18   = 'QSOLb_1day_NoDeimos_10feat.mat';
load(name18);
Est18    = Est;

name19   = 'QSOLb_1day_NoLidar_Allfeat.mat';
load(name19);
Est19    = Est;
name20   = 'QSOLb_1day_NoLidar_2feat.mat';
load(name20);
Est20    = Est;
name21   = 'QSOLb_1day_NoLidar_5feat.mat';
load(name21);
Est21    = Est;
name22   = 'QSOLb_1day_NoLidar_10feat.mat';
load(name22);
Est22    = Est;

%   Plot State vector Covariance RMS
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;
    [pars, units] = CovarianceAnalysisParameters_harmonics(pars, units);
    pars.flag_FILTERING = 0;
    % Results_relative_harmonics(Est1, [4,4], pars, units)
    % Results_relative_harmonics(Est4, [4,4], pars, units)
    % Results_relative_harmonics(Est5, [4,4], pars, units)
    % Results_relative_harmonics(Est9, [4,4], pars, units)
    % Results_relative_harmonics(Est10, [4,4], pars, units)
    % Results_relative_harmonics(Est21, [4,4], pars, units)


%   Comparison N.feat
    coeff   = 0.97;
    fine    = round(coeff*size(Est4.t(1,:),2));
    fine2   = round(coeff*size(Est22.t(1,:),2));

%   All observables
    [SQRT_X1, SQRT_V1, mean1, std1, max1, min1, meanV1, stdV1, maxV1, minV1,...
        meanLib1, maxLib1, minLib1, stdLib1] = Envelopes_Plot_Rel(Est1, fine);
    [SQRT_X2, SQRT_V2, mean2, std2, max2, min2, meanV2, stdV2, maxV2, minV2,...
        meanLib2, maxLib2, minLib2, stdLib2] = Envelopes_Plot_Rel(Est2, fine);
    [SQRT_X3, SQRT_V3, mean3, std3, max3, min3, meanV3, stdV3, maxV3, minV3,...
        meanLib3, maxLib3, minLib3, stdLib3] = Envelopes_Plot_Rel(Est3, fine);
    [SQRT_X4, SQRT_V4, mean4, std4, max4, min4, meanV4, stdV4, maxV4, minV4,...
        meanLib4, maxLib4, minLib4, stdLib4] = Envelopes_Plot_Rel(Est4, fine);

%   No DSN
    [SQRT_X5, SQRT_V5, mean5, std5, max5, min5, meanV5, stdV5, maxV5, minV5,...
        meanLib5, maxLib5, minLib5, stdLib5] = Envelopes_Plot_Rel(Est5, fine);
    [SQRT_X6, SQRT_V6, mean6, std6, max6, min6, meanV6, stdV6, maxV6, minV6,...
        meanLib6, maxLib6, minLib6, stdLib6] = Envelopes_Plot_Rel(Est6, fine);
    [SQRT_X7, SQRT_V7, mean7, std7, max7, min7, meanV7, stdV7, maxV7, minV7,...
        meanLib7, maxLib7, minLib7, stdLib7] = Envelopes_Plot_Rel(Est7, fine);
    [SQRT_X8, SQRT_V8, mean8, std8, max8, min8, meanV8, stdV8, maxV8, minV8,...
        meanLib8, maxLib8, minLib8, stdLib8] = Envelopes_Plot_Rel(Est8, fine);

%   No Mars Limb    
    [SQRT_X11, SQRT_V11, mean11, std11, max11, min11, meanV11, stdV11, maxV11, minV11,...
        meanLib11, maxLib11, minLib11, stdLib11] = Envelopes_Plot_Rel(Est11, fine);
    [SQRT_X12, SQRT_V12, mean12, std12, max12, min12, meanV12, stdV12, maxV12, minV12,...
        meanLib12, maxLib12, minLib12, stdLib12] = Envelopes_Plot_Rel(Est12, fine);
    [SQRT_X13, SQRT_V13, mean13, std13, max13, min13, meanV13, stdV13, maxV13, minV13,...
        meanLib13, maxLib13, minLib13, stdLib13] = Envelopes_Plot_Rel(Est13, fine);
    [SQRT_X14, SQRT_V14, mean14, std14, max14, min14, meanV14, stdV14, maxV14, minV14,...
        meanLib14, maxLib14, minLib14, stdLib14] = Envelopes_Plot_Rel(Est14, fine);

%   No Deimos
    [SQRT_X15, SQRT_V15, mean15, std15, max15, min15, meanV15, stdV15, maxV15, minV15,...
        meanLib15, maxLib15, minLib15, stdLib15] = Envelopes_Plot_Rel(Est15, fine);
    [SQRT_X16, SQRT_V16, mean16, std16, max16, min16, meanV16, stdV16, maxV16, minV16,...
        meanLib16, maxLib16, minLib16, stdLib16] = Envelopes_Plot_Rel(Est16, fine);
    [SQRT_X17, SQRT_V17, mean17, std17, max17, min17, meanV17, stdV17, maxV17, minV17,...
        meanLib17, maxLib17, minLib17, stdLib17] = Envelopes_Plot_Rel(Est17, fine);
    [SQRT_X18, SQRT_V18, mean18, std18, max18, min18, meanV18, stdV18, maxV18, minV18,...
        meanLib18, maxLib18, minLib18, stdLib18] = Envelopes_Plot_Rel(Est18, fine);


%   No Lidar
    [SQRT_X19, SQRT_V19, mean19, std19, max19, min19, meanV19, stdV19, maxV19, minV19,...
        meanLib19, maxLib19, minLib19, stdLib19] = Envelopes_Plot_Rel(Est19, fine2);
    [SQRT_X20, SQRT_V20, mean20, std20, max20, min20, meanV20, stdV20, maxV20, minV20,...
        meanLib20, maxLib20, minLib20, stdLib20] = Envelopes_Plot_Rel(Est20, fine2);
    [SQRT_X21, SQRT_V21, mean21, std21, max21, min21, meanV21, stdV21, maxV21, minV21,...
        meanLib21, maxLib21, minLib21, stdLib21] = Envelopes_Plot_Rel(Est21, fine2);
    [SQRT_X22, SQRT_V22, mean22, std22, max22, min22, meanV22, stdV22, maxV22, minV22,...
        meanLib22, maxLib22, minLib22, stdLib22] = Envelopes_Plot_Rel(Est22, fine2);


%**************************************************************************
%                               Tradeoff
%**************************************************************************


x       = [1e2; 2e2; 5e2; 1e3];
y       = [mean4; mean3; mean2; mean1];
ypos    = [max4-mean4; max3-mean3; max2-mean2; max1-mean1];
yneg    = [mean4-min4; mean3-min3; mean2-min2; mean1-min1];
yV      = [meanV4; meanV3; meanV2; meanV1];
yposV   = [maxV4-meanV4; maxV3-meanV3; maxV2-meanV2; maxV1-meanV1];
ynegV   = [meanV4-minV4; meanV3-minV3; meanV2-minV2; meanV1-minV1];

yNoDSN      = [mean8; mean7; mean6; mean5];
yposNoDSN   = [max8-mean8; max7-mean7; max6-mean6; max5-mean5];
ynegNoDSN   = [mean8-min8; mean7-min7; mean6-min6; mean5-min5];
yVNoDSN     = [meanV8; meanV7; meanV6; meanV5];
yposVNoDSN  = [maxV8-meanV8; maxV7-meanV7; maxV6-meanV6; maxV5-meanV5];
ynegVNoDSN  = [meanV8-minV8; meanV7-minV7; meanV6-minV6; meanV5-minV5];


yNoLimb     = [mean14; mean13; mean12; mean11];
yposNoLimb  = [max14-mean14; max13-mean13; max12-mean12; max11-mean11];
ynegNoLimb  = [mean14-min14; mean13-min13; mean12-min12; mean11-min11];
yVNoLimb    = [meanV14; meanV13; meanV12; meanV11];
yposVNoLimb = [maxV14-meanV14; maxV13-meanV13; maxV12-meanV12; maxV11-meanV11];
ynegVNoLimb = [meanV14-minV14; meanV13-minV13; meanV12-minV12; meanV11-minV11];


yNoDeimos       = [mean18; mean17; mean16; mean15];
yposNoDeimos    = [max18-mean18; max17-mean17; max16-mean16; max15-mean15];
ynegNoDeimos    = [mean18-min18; mean17-min17; mean16-min16; mean15-min15];
yVNoDeimos      = [meanV18; meanV17; meanV16; meanV15];
yposVNoDeimos   = [maxV18-meanV18; maxV17-meanV17; maxV16-meanV16; maxV15-meanV15];
ynegVNoDeimos   = [meanV18-minV18; meanV17-minV17; meanV16-minV16; meanV15-minV15];


yNoLidar        = [mean22; mean21; mean20; mean19];
yposNoLidar     = [max22-mean22; max21-mean21; max20-mean20; max19-mean19];
ynegNoLidar     = [mean22-min22; mean21-min21; mean20-min20; mean19-min19];
yVNoLidar       = [meanV22; meanV21; meanV20; meanV19];
yposVNoLidar    = [maxV22-meanV22; maxV21-meanV21; maxV20-meanV20; maxV19-meanV19];
ynegVNoLidar    = [meanV22-minV22; meanV21-minV21; meanV20-minV20; meanV19-minV19];


labels  = {'$100$','$200$','$500$','$1000$'};


% Same thing with the libration angle
xLib       = [1e2; 2e2; 5e2; 1e3];
yLib       = [meanLib4; meanLib3; meanLib2; meanLib1].*180/pi;
yposLib    = [stdLib4; stdLib3; stdLib2; stdLib1].*180/pi;
ynegLib    = [stdLib4; stdLib3; stdLib2; stdLib1].*180/pi;

yLib_NoDSN      = [meanLib8; meanLib7; meanLib6; meanLib5].*180/pi;
yposLib_NoDSN   = [stdLib8; stdLib7; stdLib6; stdLib5].*180/pi;
ynegLib_NoDSN   = [stdLib8; stdLib7; stdLib6; stdLib5].*180/pi;

yLib_NoLimb     = [meanLib14; meanLib13; meanLib12; meanLib11].*180/pi;
yposLib_NoLimb  = [stdLib14; stdLib13; stdLib12; stdLib11].*180/pi;
ynegLib_NoLimb  = [stdLib14; stdLib13; stdLib12; stdLib11].*180/pi;

yLib_NoDeimos       = [meanLib18; meanLib17; meanLib16; meanLib15].*180/pi;
yposLib_NoDeimos    = [stdLib18; stdLib17; stdLib16; stdLib15].*180/pi;
ynegLib_NoDeimos    = [stdLib18; stdLib17; stdLib16; stdLib15].*180/pi;

yLib_NoLidar        = [meanLib22; meanLib21; meanLib20; meanLib19].*180/pi;
yposLib_NoLidar     = [stdLib22; stdLib21; stdLib20; stdLib19].*180/pi;
ynegLib_NoLidar     = [stdLib22; stdLib21; stdLib20; stdLib19].*180/pi;


labelsLib  = labels;


figure()
subplot(1,2,1)
errorbar(x,y,yneg,ypos,'-s','LineWidth',2,'MarkerSize',5)
hold on
errorbar(x,yNoDSN,ynegNoDSN,yposNoDSN,'-s',...
    'LineWidth',2,'MarkerSize',5)
errorbar(x,yNoLimb,ynegNoLimb,yposNoLimb,'-s',...
    'LineWidth',2,'MarkerSize',5)
errorbar(x,yNoDeimos,ynegNoDeimos,yposNoDeimos,'-s',...
    'LineWidth',2,'MarkerSize',5)
% errorbar(x,yNoLidar,ynegNoLidar,yposNoLidar,'-s',...
%     'LineWidth',2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log');
set(gca,'XTick',[1e2; 2e2; 5e2; 1e3]);
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
% legend('Full','No DSN','No Limb','No Deimos','No Lidar',...
%     'Interpreter','latex','Fontsize',26)
legend('Full','No DSN','No Limb','No Deimos',...
    'Interpreter','latex','Fontsize',26)
ylabel('$[km]$','Fontsize',26)
xlim([5e1,2e3])
grid on
subplot(1,2,2)
errorbar(x,yV,ynegV,yposV,'-s','LineWidth',2,'MarkerSize',5)
hold on
errorbar(x,yVNoDSN,ynegVNoDSN,yposVNoDSN,'-s',...
    'LineWidth',2,'MarkerSize',5)
errorbar(x,yVNoLimb,ynegVNoLimb,yposVNoLimb,'-s',...
    'LineWidth',2,'MarkerSize',5)
errorbar(x,yVNoDeimos,ynegVNoDeimos,yposVNoDeimos,'-s',...
    'LineWidth',2,'MarkerSize',5)
% errorbar(x,yVNoLidar,ynegVNoLidar,yposVNoLidar,'-s',...
%     'LineWidth',2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log')
set(gca,'XTick',[1e2; 2e2; 5e2; 1e3]);
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
% legend('Full','No DSN','No Limb','No Deimos','No Lidar',...
%     'Interpreter','latex','Fontsize',26)
legend('Full','No DSN','No Limb','No Deimos',...
    'Interpreter','latex','Fontsize',26)
ylabel('$[km/s]$','Fontsize',26)
xlim([5e1,2e3])


% Libration

figure()
errorbar(xLib,yLib,ynegLib,yposLib,'-s','LineWidth',2,'MarkerSize',5)
hold on
errorbar(x,yLib_NoDSN,ynegLib_NoDSN,yposLib_NoDSN,'-s',...
    'LineWidth',2,'MarkerSize',5)
errorbar(x,yLib_NoLimb,ynegLib_NoLimb,yposLib_NoLimb,'-s',...
    'LineWidth',2,'MarkerSize',5)
errorbar(x,yLib_NoDeimos,ynegLib_NoDeimos,yposLib_NoDeimos,'-s',...
    'LineWidth',2,'MarkerSize',5)
% errorbar(x,yLib_NoLidar,ynegLib_NoLidar,yposLib_NoLidar,'-s',...
%     'LineWidth',2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log')
set(gca,'XTick',[1e2; 2e2; 5e2; 1e3]);
set(gca,'XTickLabel',labelsLib,'Fontsize',30);
set(gca,'XTickLabelRotation',45)
ylabel('$[deg]$','Fontsize',30)
% legend('Full','No DSN','No Limb','No Deimos','No Lidar',...
%     'Interpreter','latex','Fontsize',26)
legend('Full','No DSN','No Limb','No Deimos',...
    'Interpreter','latex','Fontsize',26)
xlim([5e1,2e3])
grid on



%% Confronto geometrie

name11   = 'QSOLb_20day_NoDSN_5feat.mat';
load(name11);
Est11    = Est;
name12   = '3DQSOLb_20day_NoDSN_5feat.mat';
load(name12);
Est12    = Est;
name13   = 'SwingQSOLb_20day_NoDSN_5feat.mat';
load(name13);
Est13    = Est;


name14   = 'QSOLb_20day_Full_5feat.mat';
load(name14);
Est14    = Est;
name15   = '3DQSOLb_20day_Full_5feat.mat';
load(name15);
Est15    = Est;
name16   = 'SwingQSOLb_20day_Full_5feat.mat';
load(name16);
Est16    = Est;


t_obs   = Est11.t(1,:);
coeff   = 0.9;
fine    = round(coeff*size(t_obs,2));
[pars, units] = MMX_DefineNewModelParametersAndUnits_Harm;
Clm = pars.SH.Clm;
Clm(pars.SH.Clm == 0) = 1e-2;
Slm = pars.SH.Slm;
Slm(pars.SH.Slm == 0) = 1e-2;

% Full envelopes
pars.flag_FILTERING = 0;
[pars, units] = CovarianceAnalysisParameters_harmonics(pars, units);
% Resurelts_relative_harmonics(Est12, [4,4], pars, units)


% ****************************** NO DSN ***********************************

% Calculation of the envelopes data
[mean_Clm_QSO_NoDSN,sigma_Clm_QSO_NoDSN,max_Clm_QSO_NoDSN,min_Clm_QSO_NoDSN,mean_Slm_QSO_NoDSN,sigma_Slm_QSO_NoDSN,...
    max_Slm_QSO_NoDSN,min_Slm_QSO_NoDSN] = Harmonics_Envelopes_Plot_Rel(Est11,[4,4],fine);
[mean_Clm_3DQSO_NoDSN,sigma_Clm_3DQSO_NoDSN,max_Clm_3DQSO_NoDSN,min_Clm_3DQSO_NoDSN,mean_Slm_3DQSO_NoDSN,sigma_Slm_3DQSO_NoDSN,...
    max_Slm_3DQSO_NoDSN,min_Slm_3DQSO_NoDSN] = Harmonics_Envelopes_Plot_Rel(Est12,[4,4],fine);
[mean_Clm_SwingQSO_NoDSN,sigma_Clm_SwingQSO_NoDSN,max_Clm_SwingQSO_NoDSN,min_Clm_SwingQSO_NoDSN,mean_Slm_SwingQSO_NoDSN,sigma_Slm_SwingQSO_NoDSN,...
    max_Slm_SwingQSO_NoDSN,min_Slm_SwingQSO_NoDSN] = Harmonics_Envelopes_Plot_Rel(Est13,[4,4],fine);


% C_nm
% mean_Clm_NoDSN    = cat(3,mean_Clm_QSO_NoDSN,mean_Clm_SwingQSO_NoDSN,mean_Clm_3DQSO_NoDSN);
% mean_Clm_neg_NoDSN = cat(3,mean_Clm_QSO_NoDSN-min_Clm_QSO_NoDSN,mean_Clm_SwingQSO_NoDSN-min_Clm_SwingQSO_NoDSN,...
%     mean_Clm_3DQSO_NoDSN-min_Clm_3DQSO_NoDSN);
% mean_Clm_pos_NoDSN = cat(3,max_Clm_QSO_NoDSN-mean_Clm_QSO_NoDSN,max_Clm_SwingQSO_NoDSN-mean_Clm_SwingQSO_NoDSN,...
%     max_Clm_3DQSO_NoDSN-mean_Clm_3DQSO_NoDSN);

% C_nm percentuale
mean_Clm_NoDSN    = cat(3,mean_Clm_QSO_NoDSN,mean_Clm_SwingQSO_NoDSN,mean_Clm_3DQSO_NoDSN)./abs(Clm);
mean_Clm_neg_NoDSN = cat(3,mean_Clm_QSO_NoDSN-min_Clm_QSO_NoDSN,mean_Clm_SwingQSO_NoDSN-min_Clm_SwingQSO_NoDSN,...
    mean_Clm_3DQSO_NoDSN-min_Clm_3DQSO_NoDSN)./abs(Clm);
mean_Clm_pos_NoDSN = cat(3,max_Clm_QSO_NoDSN-mean_Clm_QSO_NoDSN,max_Clm_SwingQSO_NoDSN-mean_Clm_SwingQSO_NoDSN,...
    max_Clm_3DQSO_NoDSN-mean_Clm_3DQSO_NoDSN)./abs(Clm);

% S_nm
% mean_Slm_NoDSN = cat(3,mean_Slm_QSO_NoDSN,mean_Slm_SwingQSO_NoDSN,mean_Slm_3DQSO_NoDSN);
% mean_Slm_neg_NoDSN = cat(3,mean_Slm_QSO_NoDSN-min_Slm_QSO_NoDSN,mean_Slm_SwingQSO_NoDSN-min_Slm_SwingQSO_NoDSN,...
%     mean_Slm_3DQSO_NoDSN-min_Slm_3DQSO_NoDSN);
% mean_Slm_pos_NoDSN = cat(3,max_Slm_QSO_NoDSN-mean_Slm_QSO_NoDSN,max_Slm_SwingQSO_NoDSN-mean_Slm_SwingQSO_NoDSN,...
%     max_Slm_3DQSO_NoDSN-mean_Slm_3DQSO_NoDSN);

% S_nm percentuale
mean_Slm_NoDSN = cat(3,mean_Slm_QSO_NoDSN,mean_Slm_SwingQSO_NoDSN,mean_Slm_3DQSO_NoDSN)./abs(Slm);
mean_Slm_neg_NoDSN = cat(3,mean_Slm_QSO_NoDSN-min_Slm_QSO_NoDSN,mean_Slm_SwingQSO_NoDSN-min_Slm_SwingQSO_NoDSN,...
    mean_Slm_3DQSO_NoDSN-min_Slm_3DQSO_NoDSN)./abs(Slm);
mean_Slm_pos_NoDSN = cat(3,max_Slm_QSO_NoDSN-mean_Slm_QSO_NoDSN,max_Slm_SwingQSO_NoDSN-mean_Slm_SwingQSO_NoDSN,...
    max_Slm_3DQSO_NoDSN-mean_Slm_3DQSO_NoDSN)./abs(Slm);


% ************************** ALL OBSERVABLES ******************************

% Calculation of the envelopes data
[mean_Clm_QSO,sigma_Clm_QSO,max_Clm_QSO,min_Clm_QSO,mean_Slm_QSO,sigma_Slm_QSO,...
    max_Slm_QSO,min_Slm_QSO] = Harmonics_Envelopes_Plot_Rel(Est14,[4,4],fine);
[mean_Clm_3DQSO,sigma_Clm_3DQSO,max_Clm_3DQSO,min_Clm_3DQSO,mean_Slm_3DQSO,sigma_Slm_3DQSO,...
    max_Slm_3DQSO,min_Slm_3DQSO] = Harmonics_Envelopes_Plot_Rel(Est15,[4,4],fine);
[mean_Clm_SwingQSO,sigma_Clm_SwingQSO,max_Clm_SwingQSO,min_Clm_SwingQSO,mean_Slm_SwingQSO,sigma_Slm_SwingQSO,...
    max_Slm_SwingQSO,min_Slm_SwingQSO] = Harmonics_Envelopes_Plot_Rel(Est16,[4,4],fine);

% C_nm
% mean_Clm    = cat(3,mean_Clm_QSO,mean_Clm_SwingQSO,mean_Clm_3DQSO);
% mean_Clm_neg = cat(3,mean_Clm_QSO-min_Clm_QSO,mean_Clm_SwingQSO-min_Clm_SwingQSO,...
%     mean_Clm_3DQSO-min_Clm_3DQSO);
% mean_Clm_pos = cat(3,max_Clm_QSO-mean_Clm_QSO,max_Clm_SwingQSO-mean_Clm_SwingQSO,...
%     max_Clm_3DQSO-mean_Clm_3DQSO);

% C_nm percentuale
mean_Clm    = cat(3,mean_Clm_QSO,mean_Clm_SwingQSO,mean_Clm_3DQSO)./abs(Clm);
mean_Clm_neg = cat(3,mean_Clm_QSO-min_Clm_QSO,mean_Clm_SwingQSO-min_Clm_SwingQSO,...
    mean_Clm_3DQSO-min_Clm_3DQSO)./abs(Clm);
mean_Clm_pos = cat(3,max_Clm_QSO-mean_Clm_QSO,max_Clm_SwingQSO-mean_Clm_SwingQSO,...
    max_Clm_3DQSO-mean_Clm_3DQSO)./abs(Clm);

% S_nm
% mean_Slm = cat(3,mean_Slm_QSO,mean_Slm_SwingQSO,mean_Slm_3DQSO);
% mean_Slm_neg = cat(3,mean_Slm_QSO-min_Slm_QSO,mean_Slm_SwingQSO-min_Slm_SwingQSO,...
%     mean_Slm_3DQSO-min_Slm_3DQSO);
% mean_Slm_pos = cat(3,max_Slm_QSO-mean_Slm_QSO,max_Slm_SwingQSO-mean_Slm_SwingQSO,...
%     max_Slm_3DQSO-mean_Slm_3DQSO);

% S_nm percentuale
mean_Slm = cat(3,mean_Slm_QSO,mean_Slm_SwingQSO,mean_Slm_3DQSO)./abs(Slm);
mean_Slm_neg = cat(3,mean_Slm_QSO-min_Slm_QSO,mean_Slm_SwingQSO-min_Slm_SwingQSO,...
    mean_Slm_3DQSO-min_Slm_3DQSO)./abs(Slm);
mean_Slm_pos = cat(3,max_Slm_QSO-mean_Slm_QSO,max_Slm_SwingQSO-mean_Slm_SwingQSO,...
    max_Slm_3DQSO-mean_Slm_3DQSO)./abs(Slm);


x = categorical({'QSO','Swing-QSO','3D-QSO'});
x = reordercats(x,{'QSO','Swing-QSO','3D-QSO'});

figure()
sgtitle('$C_{n,m}$ Coefficient''s $3\sigma$ envelopes','Fontsize',40,'Interpreter','latex')
for i = 1:5
    subplot(1,5,i)
    for j = 1:i
        errorbar(x,squeeze(mean_Clm(i,j,:)),squeeze(mean_Clm_neg(i,j,:)),...
            squeeze(mean_Clm_pos(i,j,:)),'-s','LineWidth',2,'MarkerSize',5,...
            'DisplayName',['$C_{',num2str(i-1),num2str(j-1),'}$'])
        grid on;
        hold on;
    end
    if i==1 && j==1
        ylabel('$3\sigma_{C_{n,m}}/|C_{n,m}|$','FontSize',26,'Interpreter','latex')
    end
    plot(0.5:3.5,[1,1,1,1],'Color','r','LineWidth',2.5,'DisplayName','$|C_{n,m}|=1$')
    set(gca,'YScale','log','TickLabelInterpreter','latex','FontSize',26)
    legend('Interpreter','latex','FontSize',26)

end

figure()
sgtitle('$S_{n,m}$ Coefficient''s $3\sigma$ envelopes','Fontsize',40,'Interpreter','latex')
for i = 2:5
    subplot(1,4,i-1)
    for j = 2:i
        errorbar(x,squeeze(mean_Slm(i,j,:)),squeeze(mean_Slm_neg(i,j,:)),...
            squeeze(mean_Slm_pos(i,j,:)),'-s','LineWidth',2,'MarkerSize',5,...
            'DisplayName',['$S_{',num2str(i-1),num2str(j-1),'}$'])
        grid on;
        hold on;
    end
    if i==2 && j==2
        ylabel('$3\sigma_{S_{n,m}}/|S_{n,m}|$','FontSize',26,'Interpreter','latex')
    end
    plot(0.5:3.5,[1,1,1,1],'Color','r','LineWidth',2.5,'DisplayName','$|S_{n,m}|=1$')
    set(gca,'YScale','log','TickLabelInterpreter','latex','FontSize',26)
    legend('Interpreter','latex','FontSize',26)
end


figure()
sgtitle('$C_{n,m}$ Coefficient''s $3\sigma$ envelopes without DSN','Fontsize',40,'Interpreter','latex')
for i = 1:5
    subplot(1,5,i)
    for j = 1:i
        errorbar(x,squeeze(mean_Clm_NoDSN(i,j,:)),squeeze(mean_Clm_neg_NoDSN(i,j,:)),...
            squeeze(mean_Clm_pos_NoDSN(i,j,:)),'-s','LineWidth',2,'MarkerSize',5,...
            'DisplayName',['$C_{',num2str(i-1),num2str(j-1),'}$'])
        grid on;
        hold on;
    end
    if i==1 && j==1
        ylabel('$3\sigma_{C_{n,m}}/|C_{n,m}|$','FontSize',26,'Interpreter','latex')
    end
    plot(0.5:3.5,[1,1,1,1],'Color','r','LineWidth',2.5,'DisplayName','$|C_{n,m}|=1$')
    set(gca,'YScale','log','TickLabelInterpreter','latex','FontSize',26)
    legend('Interpreter','latex','FontSize',26)

end

figure()
sgtitle('$S_{n,m}$ Coefficient''s $3\sigma$ envelopes without DSN','Fontsize',40,'Interpreter','latex')
for i = 2:5
    subplot(1,4,i-1)
    for j = 2:i
        errorbar(x,squeeze(mean_Slm_NoDSN(i,j,:)),squeeze(mean_Slm_neg_NoDSN(i,j,:)),...
            squeeze(mean_Slm_pos_NoDSN(i,j,:)),'-s','LineWidth',2,'MarkerSize',5,...
            'DisplayName',['$S_{',num2str(i-1),num2str(j-1),'}$'])
        grid on;
        hold on;
    end
    if i==2 && j==2
        ylabel('$3\sigma_{S_{n,m}}/|S_{n,m}|$','FontSize',26,'Interpreter','latex')
    end
    plot(0.5:3.5,[1,1,1,1],'Color','r','LineWidth',2.5,'DisplayName','$|S_{n,m}|=1$')
    set(gca,'YScale','log','TickLabelInterpreter','latex','FontSize',26)
    legend('Interpreter','latex','FontSize',26)
end
