function [mean_Clm,sigma_Clm,max_Clm,min_Clm,mean_Slm,sigma_Slm,...
    max_Slm,min_Slm] = Harmonics_Envelopes_Plot_Rel(Est,PhobosGravityField,fine)


    mean_Clm    = zeros(PhobosGravityField+1);
    sigma_Clm   = zeros(PhobosGravityField+1);
    max_Clm     = zeros(PhobosGravityField+1);
    min_Clm     = zeros(PhobosGravityField+1);
            
    mean_Slm    = zeros(PhobosGravityField);
    sigma_Slm   = zeros(PhobosGravityField);
    max_Slm     = zeros(PhobosGravityField);
    min_Slm     = zeros(PhobosGravityField);
    
    if (size(Est.X_t,1)==41)||(size(Est.X_t,1)==97)
            place0 = 10;
    elseif (size(Est.X_t,1)==43)||(size(Est.X_t,1)==99)
            place0 = 12;
    end
    
    for i = 0:PhobosGravityField(1)
        for j = 0:i
            place0 = place0+1;
            Clm_i   = 3.*squeeze(real(sqrt(Est.P_t(place0,place0,fine:end-1))));
            mean_Clm(i+1,j+1)   = mean(Clm_i);
            sigma_Clm(i+1,j+1)  = std(Clm_i);
            max_Clm(i+1,j+1)  = max(Clm_i);
            min_Clm(i+1,j+1)  = min(Clm_i);
            
        end
    end
    
    for i = 1:PhobosGravityField(1)
            for j = 1:i
                place0 = place0+1;
                Slm_i   = 3.*squeeze(real(sqrt(Est.P_t(place0,place0,fine:end-1))));
                mean_Slm(i+1,j+1)   = mean(Slm_i);
                sigma_Slm(i+1,j+1)  = std(Slm_i);
                max_Slm(i+1,j+1)  = max(Slm_i);
                min_Slm(i+1,j+1)  = min(Slm_i);
            
            end
    end

    

end