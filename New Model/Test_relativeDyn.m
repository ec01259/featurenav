clear
close all
clc

%%  Test model With MMX

    warning('on', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('./MMX_BSP_Files_Relative/');
    addpath('../../useful_functions/');
    addpath('../../useful_functions/planet_textures/');
    addpath('../../dynamical_model/');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../computer-vision/'));
    addpath(genpath('../../paper/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../../paper/MMX_Product/MMX_BSP_Files_GravLib/'));
    cspice_kclear
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_031_009_2x2_826891269_828619269.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_094_006_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    cspice_furnsh(which('MMX_QSO_031_2x2_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_027_014_2x2_826891269_829483269_relative_right.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_027_011_2x2_826891269_829483269_relative_right.bsp'));

%   Model parsameters
    [pars, units] = MMX_DefineNewModelParametersAndUnits_Right;
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Phobos      = struct('alpha',alpha/units.lsf,'beta',beta/units.lsf,'gamma',gamma/units.lsf);
    Geom.alpha  = alpha;
    Geom.beta   = beta;
    Geom.gamma  = gamma;
    
%%  Integration Phobos's new model + MMX

%   Initial MMX's state vector
    data        = '2026-03-16 00:00:00 (UTC)';
    freq        = 300;
    data        = cspice_str2et(data);
    day         = 86400;
    pars.et0    = data;
    [Ph,pars]   = Phobos_States_NewModel(data,pars);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;
    Iz      = pars.IPhz_bar + pars.ni*Ph0(1)^2;
    K       = Iz*Ph0(4) + pars.IPhz_bar*Ph0(8);
    pars.K  = K;
    
%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;
    MMX0(1:3) = pars.MARSIAU2perifocal*MMX0(1:3);
    MMX0(4:6) = pars.MARSIAU2perifocal*MMX0(4:6);
    theta   = Ph0(3);
    Xsi     = Ph0(7);

    % From perifocal 2 PA
    Rtheta  = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];
    Rxsi    = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
    Rtot    = Rxsi*Rtheta;
    Phobos0 = cspice_spkezr('-401', data, 'MARSIAU', 'none', '499')./units.sfVec;
    r_Phobos    = pars.MARSIAU2perifocal*Phobos0(1:3);
    v_Phobos    = pars.MARSIAU2perifocal*Phobos0(4:6);

    % NICOLA'S VERSION
    mirror      = [-1, 0, 0; 0, -1, 0; 0, 0, 1];
    MMX0(4:6)   = mirror*(Rtot*(MMX0(4:6) - v_Phobos - ...
        cross([0; 0; Ph0(4) + Ph0(8)], MMX0(1:3) - r_Phobos)));
    MMX0(1:3)   = mirror*Rtot*(MMX0(1:3) - r_Phobos);
    

    % MMX0    = cspice_spkezr('-35', data, 'IAU_PHOBOS', 'none', '-401')./units.sfVec;
    % MMX0(1:3)   = pars.PB*MMX0(1:3);
    % MMX0(4:6)   = pars.PB*MMX0(4:6);

    MMX1    = cspice_spkezr('-35', data, 'IAU_PHOBOS', 'none', '-401')./units.sfVec;
    MMX1(1:3)   = pars.PB*MMX1(1:3);
    MMX1(4:6)   = pars.PB*MMX1(4:6);

     
%   Analysis initial state vector
    St0     = [MMX0; Ph0; pars.IPhx_bar; pars.IPhy_bar; pars.IPhz_bar];
    St02    = [MMX1; Ph0(1:2); Ph0(7:8); pars.IPhx_bar; pars.IPhy_bar; pars.IPhz_bar];

%   Integration
    tspan   = (0:freq:1*day)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event', @(t,X) landing_Phobos_relative(t,X,pars,units));
    [t,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Relative_tests(t,X,pars,units),tspan,St0,opt);
    t       = t/units.tsf;
    [t1,X1] = ode113(@(t,X) Dynamics_MPHandMMX_Relative_ridotta(t,X,pars,units),tspan,St02,opt);
    


%%  Plot

    
%   QSO orbit
    figure(1)
    plot3(X(:,1)*units.lsf,X(:,2)*units.lsf,X(:,3)*units.lsf,'LineWidth',1.3)
    hold on;
    grid on;
    plot3(X1(:,1)*units.lsf,X1(:,2)*units.lsf,X1(:,3)*units.lsf,'LineWidth',1.3)
    planetPlot('asteroid',[0;0;0],pars.Phobos,1);
    view(2)
    axis equal
    ylabel('Along Track [km]','FontSize',26,'Interpreter','latex')
    xlabel('Radial [km]','FontSize',26,'Interpreter','latex')
    zlabel('Z [km]','FontSize',26,'Interpreter','latex')
    legend('Dynamics 1','Dynamics 2')

    X_real = cspice_spkezr('-35', data+t', 'IAU_PHOBOS', 'none', '-401');
    X_real = (pars.PB*X_real(1:3,:))';
    figure(2)
    plot3(X(:,1)*units.lsf,X(:,2)*units.lsf,X(:,3)*units.lsf,'LineWidth',1.3)
    hold on;
    grid on;
    plot3(X_real(:,1),X_real(:,2),X_real(:,3),'LineWidth',1.3)
    planetPlot('asteroid',[0;0;0],pars.Phobos,1);
    view(2)
    axis equal
    ylabel('Along Track [km]','FontSize',26,'Interpreter','latex')
    xlabel('Radial [km]','FontSize',26,'Interpreter','latex')
    zlabel('Z [km]','FontSize',26,'Interpreter','latex')
    legend('Propagated','BSP')
    

%%  Phobos

    % figure()
    % subplot(2,2,1)
    % plot(t, X(:,7)*units.lsf,"LineWidth",1.3)
    % hold on
    % grid on
    % plot(t, X1(:,7)*units.lsf,"LineWidth",1.3)
    % xlabel('Epoch [s]','FontSize',26,'Interpreter','latex')
    % ylabel('$R_{Ph}$ [km]','FontSize',26,'Interpreter','latex')
    % legend('Mars centered', 'Phobos centered','FontSize',26,'Interpreter','latex')
    % subplot(2,2,2)
    % plot(t, X(:,13),"LineWidth",1.3)
    % hold on
    % grid on
    % plot(t, X1(:,9),"LineWidth",1.3)
    % xlabel('Epoch [s]','FontSize',26,'Interpreter','latex')
    % ylabel('$\Phi_{Ph}$ [rad]','FontSize',26,'Interpreter','latex')
    % legend('Mars centered', 'Phobos centered','FontSize',26,'Interpreter','latex')
    % subplot(2,2,3)
    % plot(t, X(:,8)*units.lsf,"LineWidth",1.3)
    % hold on
    % grid on
    % plot(t, X1(:,8)*units.lsf,"LineWidth",1.3)
    % xlabel('Epoch [s]','FontSize',26,'Interpreter','latex')
    % ylabel('$\dot{R}_{Ph}$ [km/s]','FontSize',26,'Interpreter','latex')
    % legend('Mars centered', 'Phobos centered','FontSize',26,'Interpreter','latex')
    % subplot(2,2,4)
    % plot(t, X(:,14),"LineWidth",1.3)
    % hold on
    % grid on
    % plot(t, X1(:,10),"LineWidth",1.3)
    % xlabel('Epoch [s]','FontSize',26,'Interpreter','latex')
    % ylabel('$\dot{\Phi}_{Ph}$ [rad/s]','FontSize',26,'Interpreter','latex')
    % legend('Mars centered', 'Phobos centered','FontSize',26,'Interpreter','latex')
    