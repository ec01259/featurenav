clear
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath(genpath('../useful_functions/'));
    addpath(genpath('../dynamical_model/'));
    addpath(genpath('../mice/'));
    addpath(genpath('../computer-vision/'));
    addpath(genpath('../paper/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../paper/MMX_Product/MMX_BSP_Files_GravLib/'));
    addpath(genpath('./Utilities/'))
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('MMX_QSO_049_2x2_826891269_828619269.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_031_009_2x2_826891269_828619269.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_031_011_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));


%%  Load observations and path for syntethic pictures

    load("YObs.mat");


%%  Initial conditions for the analysis

%   Model parsameters
    [pars, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    day         = 86400;
    pars.et0    = data;
    [Ph,pars]   = Phobos_States_NewModel(data,pars);

%   Covariance analysis parsameters
    [pars, units] = CovarianceAnalysisParameters(pars, units);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0    = MMX0./units.sfVec;

%   Analysis initial state vector
    St0     = [MMX0; Ph0; pars.I2; pars.bias];
    % St0     = [MMX0; Ph0(1:4); Ph0(7:8); pars.I2; pars.bias];
    
    Est0.dx = zeros(size(St0,1),1);
    pars.flag_FILTERING = 0;
    % Est0.dx = St0.*randn(size(St0))*1e-6;
    % pars.flag_FILTERING = 1;
    Est0.X  = St0 + Est0.dx;
    Est0.P0 = pars.P0;
    Est0.t0 = data*units.tsf;

%%  Analysis

    
    file_features = 'Principal_Phobos_ogni10.mat';
    file_features_Mars = 'Mars_5facets.mat';

%   Sembra non osservabile con features e LIDAR only. Come mi aspetterei...
%   Se prendi solo il lidar, gli envelopes si comportano come mi aspetterei
%   ma con l'aggiunta delle features crascha perchè non trova features 
%   comuni tra i vari sigmapoints. 
    
    pars.alpha = 1;
    pars.beta  = 2;
    [Est] = UKF_Tool(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @Observables_model_Absolute,...
        pars.R,YObs,pars,units,file_features,file_features_Mars);
    % [Est] = UKF_Tool(Est0,@Dynamics_MPHandMMX_Inertia,...
    %     @Sigma_dynamics_NoPhi, @Observables_model_Absolute,...
    %     pars.R,YObs,pars,units,file_features,file_features_Mars);

%   Questo è l'UKF puro, funziona giocando con l'alpha! Ma non tutti gli
%   stati sono visibilissimi. Vedo il libration ma non la sua velocita'

    % pars.alpha = 1e0;
    % pars.beta  = 2;
    % [Est] = UKF_features(Est0, @SigmaPoints_Dynamics_Good,...
    %     @Observables_model_Absolute,pars.R,YObs,pars,units,file_features,file_features_Mars);

    % pars.alpha = 1e0;    
    % pars.beta  = 2;
    % [Est] = UKF_features(Est0, @SigmaPoints_Dynamics_Good_NoPhi,...
    %     @Observables_model_Absolute,...
    %     pars.R,YObs,pars,units,file_features,file_features_Mars);

    % Results(Est, pars, units)
    
    save('./Result_thesis/QSOLa_6days_NoDSN&Limb_10feat.mat', 'Est')

