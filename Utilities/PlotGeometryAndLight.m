function PlotGeometryAndLight(points, point_in_light, visible, r_sb_Ph, r_PhSun, pars, units)


%   Rotation matrix from Phobos to the camera lens
    i_feat  = r_sb_Ph/norm(r_sb_Ph);
    v       = [0; 0; 1];
    j       = cross(v, i_feat)/norm(cross(v, i_feat));
    k       = cross(i_feat, j)/norm(cross(i_feat, j));
    
    T       = [i_feat, j, k]';

%   In the CV world si usa questa convenzione, l'asse Z della camera è
%   quello che esce dal piatto
    R_bCam2CV   = [0, 0, 1; 0, 1, 0; -1, 0, 0];
    R_CV        = R_bCam2CV*T;


    figure(1)
    hold off
    plot3(points(1,:), points(2,:), points(3,:),'.','Color','g','MarkerSize',10)
    hold on
    grid on
    axis equal
    plot3(point_in_light(1,:), point_in_light(2,:), point_in_light(3,:), 'x','Color','b','MarkerSize',10)
    if ~isempty(visible)
        plot3(visible(1,:), visible(2,:), visible(3,:), 'o','Color','r','MarkerSize',10)
        for n = 1:size(visible,2)    
            r_sf    = r_sb_Ph - visible(:,n);
            LOS     = -r_sf/norm(r_sf);
            quiver3(r_sb_Ph(1),r_sb_Ph(2),r_sb_Ph(3),LOS(1)*units.lsf,LOS(2)*units.lsf,LOS(3)*units.lsf);
        end
    end
    xlabel('Along Track [km]','FontSize',26,'Interpreter','latex')
    ylabel('Radial [km]','FontSize',26,'Interpreter','latex')
    zlabel('Z [km]','FontSize',26,'Interpreter','latex')
    quiver3(-2*r_PhSun(1)*units.lsf,-2*r_PhSun(2)*units.lsf,-2*r_PhSun(3)*units.lsf,...
        r_PhSun(1)*units.lsf,r_PhSun(2)*units.lsf,r_PhSun(3)*units.lsf,'LineWidth',1.5,'Color','g')
    plot3(-2*r_PhSun(1)*units.lsf,-2*r_PhSun(2)*units.lsf,-2*r_PhSun(3)*units.lsf, '*','Color','b','MarkerSize',30)
    quiver3(r_sb_Ph(1),r_sb_Ph(2),r_sb_Ph(3),...
        -r_sb_Ph(1)/units.tsf,-r_sb_Ph(2)/units.tsf,-r_sb_Ph(3)/units.tsf,'LineWidth',1.5,'Color','r')
    plotCamera('location',r_sb_Ph,'orientation',R_CV,'Color','r','Size',1)
    legend('Features','Features in Light','Visible and processed','FontSize',26,'Interpreter','latex')

end