function Limb_Range = Mars_LimbRange(MMX, Phobos, Sun, Xsi, theta, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = Mars_LimbRange(MMX, Phobos, Sun, Phi, pars, units)
%
% Identify wether Mars is in the FOV and how far is it
%
% Input:
%     
% MMX           MMX state vector in the MARSIAU reference frame
% Phobos        Phobos state vector in the MARSIAU reference frame
% Sun           Sun state vector in the MARSIAU reference frame
% pars          parameters structure
% units         units structure
% 
% Output:
% 
% Limb_Range    Range derived from Mars limb size
% 
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   


    MMX     = pars.MARSIAU2perifocal*MMX(1:3);
    Phobos  = [pars.MARSIAU2perifocal, zeros(3,3); zeros(3,3), pars.MARSIAU2perifocal]*Phobos;
    Sun     = pars.MARSIAU2perifocal*Sun(1:3);

%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    RLibration  = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    Rtheta      = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];

%   Position of MMX wrt Phobos-fixed frame
    r_sb    = MMX(1:3) - Phobos(1:3);
    r_sb_Ph = RLibration*Rtheta*r_sb;
    R_sb_Ph = norm(r_sb_Ph);

%   MMX-Phobos direction in the Phobos-fixed reference frame
    I_sb    = r_sb_Ph/R_sb_Ph;

%   MMX-Mars direction in the rotating reference frame
    r_sM    = RLibration*Rtheta*MMX(1:3);
    I_sM    = r_sM/norm(r_sM);

%   Mars pole position in the Phobos rotating frame
    Mars_pole   = [0; 0; -pars.Mars.gamma];
    R_ini       = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Mars_pole   = R_ini*Rtheta'*RLibration'*Mars_pole - Phobos(1:3);
    Mars_centre = - Phobos(1:3);

%   Phobos pole position in the Phobos rotating frame
    Phobos_pole     = [0; 0; -pars.Phobos.gamma];
    Phobos_pole     = R_ini*Phobos_pole;
    Phobos_centre   = zeros(3,1);
    Phobos_centre   = R_ini*Phobos_centre;

%   Sunlight direction in the rotating reference frame
    Sun = Rtheta*Sun(1:3);
    I_Sun = Sun/norm(Sun);

    angle_MMXSun = acos(dot(I_sM,I_Sun));
    r_sf = r_sb_Ph - Mars_centre;
    i_feat = -r_sf/norm(r_sf);
    
    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);
    MP  = PM*[Mars_pole; 1];
    MP  = MP./repmat(MP(3),3,1);
    MC  = PM*[Mars_centre; 1];
    MC  = MC./repmat(MC(3),3,1);

    PhP = PM*[-Phobos_pole; 1];
    PhP = PhP./repmat(PhP(3),3,1);
    PhC = PM*[-Phobos_centre; 1];
    PhC = PhC./repmat(PhC(3),3,1);

    Phobos_range = norm(PhP - PhC);
    Phobos_ellipsoid = @(x,y) (x-PhC(1))^2 + (y-PhC(2))^2 - Phobos_range^2;

    
    if ((all(MC>0))&&(all(MP>0))&&((MC(1)<pars.cam.nPixX))&&...
            (MP(1)<pars.cam.nPixX)&&(MC(2)<pars.cam.nPixY)&&...
            (MP(2)<pars.cam.nPixY))&&(dot(r_sb_Ph, Mars_centre)<0)&&...
            (Phobos_ellipsoid(MP(1),MP(2))>0)

        % Limb_radius = MP - MC;
        % Limb_Range  = sqrt(Limb_radius(1)^2 + Limb_radius(2)^2) + random('Normal',0, pars.ObsNoise.pixel);

        alpha       = atan2(-pars.Mars.gamma,norm(r_sM));
        Limb_Range  = pars.cam.f*tan(alpha)*pars.cam.nPixX/pars.cam.SixeX + random('Normal',0, pars.ObsNoise.pixel);
        % Plotfeatures_Pic([MC, PhC], [MP, PhP], pars);
        
    else
        Limb_Range = [];
    end


end