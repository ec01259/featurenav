function Plotfeatures_Pic(varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plotfeatures_Pic(Y_pix, Y_pix_light, Y_pix_tot, pars)
%
% Plot the interested features in the image plate
%
% Input:
%     
% Y_pix         Visible features
% Y_pix_light   Features in light but maybe hidden
% Y_pix_tot     All the body's features
% pars          Structure with problem's parameters
%
%
% 
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



    switch size(varargin,2)
        case 2
            Y_pix = varargin{1};
            pars = varargin{2};
            
            figure(99)
            grid on
            axis equal
            if ~isempty(Y_pix)
                plot(Y_pix(1,:), Y_pix(2,:), 'o', 'Color', 'r');
            end
            hold on
            xlabel('X','FontSize',16,'Interpreter','latex')
            ylabel('Y','FontSize',16,'Interpreter','latex')
            xlim([0, 2*pars.camera_intrinsic(3,1)])
            ylim([0, 2*pars.camera_intrinsic(3,2)])
            hold off
            pause(.5)

        case 3
            Y_pix = varargin{1};
            Y_pix_light = varargin{2};
            pars = varargin{3};
            
            figure(99)
            grid on
            axis equal
            plot(Y_pix_light(1,:), Y_pix_light(2,:), '*', 'Color', 'g');
            hold on
            if ~isempty(Y_pix)
                plot(Y_pix(1,:), Y_pix(2,:), 'o', 'Color', 'r');
            end            
            xlabel('X','FontSize',16,'Interpreter','latex')
            ylabel('Y','FontSize',16,'Interpreter','latex')
            xlim([0, 2*pars.camera_intrinsic(3,1)])
            ylim([0, 2*pars.camera_intrinsic(3,2)])
            hold off
            pause(.5)

        case 4
            Y_pix = varargin{1};
            Y_pix_light = varargin{2};
            Y_pix_tot = varargin{3};
            pars = varargin{4};
            
            figure(99)
            grid on
            axis equal
            plot(Y_pix_light(1,:), Y_pix_light(2,:), '*', 'Color', 'g');
            hold on
            if ~isempty(Y_pix)
                plot(Y_pix(1,:), Y_pix(2,:), 'o', 'Color', 'r');
            end
            plot(Y_pix_tot(1,:), Y_pix_tot(2,:), '.', 'Color', 'b');
            xlabel('X','FontSize',16,'Interpreter','latex')
            ylabel('Y','FontSize',16,'Interpreter','latex')
            xlim([0, 2*pars.camera_intrinsic(3,1)])
            ylim([0, 2*pars.camera_intrinsic(3,2)])
            hold off
            pause(.5)
    
    end

end