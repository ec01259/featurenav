function Limb_Range = Mars_LimbRange_model(MMX, Phobos, Sun, Xsi, theta, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = Mars_LimbRange_model(MMX, Phobos, Sun, Phi, pars, units)
%
% Identify wether Mars is in the FOV and how far is it
%
% Input:
%     
% MMX           MMX state vector in the MARSIAU reference frame
% Phobos        Phobos state vector in the MARSIAU reference frame
% Sun           Sun state vector in the MARSIAU reference frame
% pars          parameters structure
% units         units structure
% 
% Output:
% 
% Limb_Range    Range derived from Mars limb size
% 
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    MMX     = pars.MARSIAU2perifocal*MMX(1:3);
    Phobos  = [pars.MARSIAU2perifocal, zeros(3,3); zeros(3,3), pars.MARSIAU2perifocal]*Phobos;
    Sun     = pars.MARSIAU2perifocal*Sun(1:3);

%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    RLibration  = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    Rtheta      = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];

%   Position of MMX wrt Phobos-fixed frame
    r_sb    = MMX(1:3) - Phobos(1:3);
    r_sb_Ph = RLibration*Rtheta*r_sb;
    R_sb_Ph = norm(r_sb_Ph);

%   MMX-Phobos direction in the Phobos-fixed reference frame
    I_sb    = r_sb_Ph/R_sb_Ph;

%   MMX-Mars direction in the rotating reference frame
    r_sM    = RLibration*Rtheta*MMX(1:3);
    I_sM    = r_sM/norm(r_sM);

%   Mars pole position in the Phobos rotating frame
    Mars_pole   = [0; 0; -pars.Mars.gamma];
    R_ini       = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Mars_pole   = R_ini*Rtheta'*RLibration'*Mars_pole - Phobos(1:3);
    Mars_centre = - Phobos(1:3);


%   Sunlight direction in the rotating reference frame
    Sun = Rtheta*Sun(1:3);
    I_Sun = Sun/norm(Sun);


    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);
    MP  = PM*[Mars_pole; 1];
    MP  = MP./repmat(MP(3),3,1);
    MC  = PM*[Mars_centre; 1];
    MC  = MC./repmat(MC(3),3,1);

    % Limb_radius = MP - MC;
    % Limb_Range  = sqrt(Limb_radius(1)^2 + Limb_radius(2)^2);

    alpha       = atan2(-pars.Mars.gamma,norm(r_sM));
    Limb_Range  = pars.cam.f*tan(alpha)*pars.cam.nPixX/pars.cam.SixeX;
        
    % Plotfeatures_Pic(MC, MP, pars);



end