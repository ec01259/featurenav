function [G, Rm] = Observables_model_Absolute(et, X, St_ID_Range, St_ID_Rate,...
    Lidar_check, Limb_check, Deimos_check, ISL_check, Feature_ID, Features_Mars_ID, pars, units,...
    O, file_features, file_features_Mars)
%==========================================================================
% [G, R] = Observables_model_with_Features(et,Obs,X,St_ID_Range,St_ID_Rate, Feature_ID, par,units)
%
% Compute the Range, Range Rate, Lidar's and camera's measures 
%
% INPUT: Description Units
%
% et        - Time Epoch                                                s
%
% stationID - Available Observables at the epoch t
%
% X         - State Vector defined in the Phobos rotating frame 
%               at the time being considered (42x1)
%               .MMX Position Vector (3x1)                              km
%               .MMX Velocity Vector (3x1)                              km/s
%               .Phobos Position Vector (3x1)                           km
%               .Phobos Velocity Vector (3x1)                           km/s
%               .Clm harmonics coefficients
%               .Slm Hamronics coefficients
%               .State Transition Matrix 
%
% par       -  Parameters structure 
% 
%
% OUTPUT:
%
% G         - G
% 
% DO NOT FORGET TO USE RESHAPE LATER IN THE PROJECT
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 06/2022
%
%==========================================================================
    
   
%   Output initialization
    range_Camb      = [];
    range_rate_Camb = [];
    range_Gold      = [];
    range_rate_Gold = [];
    range_Mad       = [];
    range_rate_Mad  = [];
    lidar           = [];
    Limb            = [];
    Deimos          = [];
    ISLink          = [];

    Rm              = [];

%   Dimensions of the problem
    d       = 6;
    n       = d/2;

%   Unpacking of the state vector
%   MMX
    X_MMX   = X(1:6);
    r_MMX   = X_MMX(1:3);
    v_MMX   = X_MMX(4:6);

%   Phobos
    RPh     = X(7);
    RPh_dot = X(8);
    theta   = X(9);
    theta_dot   = X(10);

    switch size(X,1)
        case 21
            Xsi     = X(11);
            Phi_M   = pars.PhiM;
        case 23
            Xsi     = X(13);
            Phi_M   = X(11);
    end

%   Useful rotation matrix
    RXsi = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
    Rtheta  = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];

%   Phobos Cartesian coordinates
    r_Phobos    = pars.perifocal2MARSIAU*RPh*[cos(theta); sin(theta); 0];
    v_Phobos    = pars.perifocal2MARSIAU*(Rtheta'*([RPh_dot; 0; 0]) + ...
           cross([0; 0; theta_dot], RPh*[cos(theta); sin(theta); 0]));
    Phobos   = [r_Phobos.*units.lsf; v_Phobos.*units.vsf];

%   Biases
    place0  = size(X,1)-pars.nBias;
    bias    = X(place0+1:end);



% -------------------------------------------------------------------------
%                       DEEP SPACE NETWORK
% -------------------------------------------------------------------------

%   Definition of Mars position WRT Earth J2000
    Mars    = cspice_spkezr('499',et,'MARSIAU','none','399');
    Mars    = Mars./units.sfVec;
    
%   Which station is working
%   Camberra's Position in the J2000 Earth-centered
    Camb    = cspice_spkezr('DSS-45', et, 'MARSIAU', 'none', 'EARTH');
    Camb    = Camb./units.sfVec;    
%   Goldstone's Position in the J2000 Earth-centered
    Gold    = cspice_spkezr('DSS-24', et, 'MARSIAU', 'none', 'EARTH');
    Gold    = Gold./units.sfVec;
%   Madrid's Position in the J2000 Earth-centered
    Mad     = cspice_spkezr('DSS-65', et, 'MARSIAU', 'none', 'EARTH');
    Mad     = Mad./units.sfVec;



    if sum((St_ID_Range == 1)==1)
%       Camberra antennas available
        r           = norm(Mars(1:n) + r_MMX - Camb(1:n));
        range_Camb  = r + bias(1);
        Rm   = [Rm; O(1,1)];            
    end

    if sum((St_ID_Range == 2)==1)
%       Goldstone antennas available
        r           = norm(Mars(1:n) + r_MMX - Gold(1:n));
        range_Gold  = r + bias(1);
        Rm   = [Rm; O(1,1)];     
    end

    if sum((St_ID_Range == 3)==1)
%       Madrid antennas available
        r   = norm(Mars(1:n) + r_MMX - Mad(1:n));
        range_Mad = r + bias(1);
        Rm   = [Rm; O(1,1)];     
    end
    


    if sum((St_ID_Rate == 1)==1)
%       Camberra antennas available
        R       = norm(Mars(1:n) + r_MMX - Camb(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_MMX - Camb(n+1:d)).*(Mars(1:n) + r_MMX - Camb(1:n)));
        range_rate_Camb = r_dot + bias(2);
        Rm   = [Rm; O(2,2)];     
    end
    
    if sum((St_ID_Rate == 2)==1)
%       Goldstone antennas available
        R   = norm(Mars(1:n) + r_MMX - Gold(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_MMX - Gold(n+1:d)).*(Mars(1:n) + r_MMX - Gold(1:n)));
        range_rate_Gold = r_dot + bias(2);
        Rm   = [Rm; O(2,2)];     
    end

    if sum((St_ID_Rate == 3)==1)
%       Camberra antennas available
        R   = norm(Mars(1:n) + r_MMX - Mad(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_MMX - Mad(n+1:d)).*(Mars(1:n) + r_MMX - Mad(1:n)));
        range_rate_Mad = r_dot + bias(2);
        Rm   = [Rm; O(2,2)];     
    end
    

% -------------------------------------------------------------------------
%                               LIDAR
% -------------------------------------------------------------------------


    if Lidar_check

%      Lidar rispetto alla superficie

       ON   = Rtheta*pars.MARSIAU2perifocal;
       BO   = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
        
       rsb  = r_MMX - r_Phobos;

%      Posizione di MMX nel Phobos Body-fixed reference frame
       r_bf = BO*ON*rsb;
       
       lat  = asin(r_bf(3)/norm(r_bf));
       lon  = atan2(r_bf(2), r_bf(1));

%      Phobos radius as function of latitude and longitude
       alpha = pars.Phobos.alpha;
       beta  = pars.Phobos.beta;
       gamma = pars.Phobos.gamma;
       
       R_latlon = (alpha*beta*gamma)/sqrt(beta^2*gamma^2*cos(lon)^2 +...
           gamma^2*alpha^2*sin(lon)^2*cos(lat)^2 + alpha^2*beta^2*sin(lon)^2*sin(lat));

       lidar = norm(rsb) - R_latlon/units.lsf + bias(3);
       
       Rm   = [Rm; O(7,7)];     

    end
    

% -------------------------------------------------------------------------
%                           FEATURES SU PHOBOS
% -------------------------------------------------------------------------

    if ~isempty(Feature_ID)

%       There should be features
        [Sun, ~] = cspice_spkezr('10', et, 'MARSIAU', 'none', '499');

        [~, Y_pix] = visible_features_model(X_MMX.*units.sfVec, Phobos,...
            Sun, Xsi, theta, file_features, pars, units);
       
       if ~isempty(Y_pix)
            Y_pix = Y_pix + repmat([bias(4); bias(5); 0], 1, size(Y_pix,2));
       else
           fprintf('\nTracking delle features perso!!\n');
           return
       end
       
    else
        Y_pix = [];
    end
   



% -------------------------------------------------------------------------
%                               MARS LIMB
% -------------------------------------------------------------------------


    if Limb_check
%      There should be the Mars Limb
       [Sun, ~] = cspice_spkezr('10', et, 'MARSIAU', 'none', '499');

       Limb = Mars_LimbRange_model(X_MMX.*units.sfVec, Phobos, Sun, Xsi, theta, pars, units);
       Limb = Limb + (bias(4) + bias(5));
       
       Rm   = [Rm; O(8,8)];    
    end



% -------------------------------------------------------------------------
%                           FEATURES SU MARTE
% -------------------------------------------------------------------------


    if ~isempty(Features_Mars_ID)

%       There should be features
        [Sun, ~] = cspice_spkezr('10', et, 'MARSIAU', 'none', '499');

        [~, Y_pix_Mars] = visible_features_Mars_model(X_MMX.*units.sfVec,...
            Phobos, -Sun, Xsi, theta, Phi_M, file_features_Mars, pars, units);
       
       if ~isempty(Y_pix_Mars)
            Y_pix_Mars = Y_pix_Mars + repmat([bias(4); bias(5); 0], 1, size(Y_pix_Mars,2));
       else
           % fprintf('\nTracking delle features su Marte perso da un sigma point!!');
           % return
       end
       
    else
        Y_pix_Mars = [];
    end


% -------------------------------------------------------------------------
%                           DEIMOS LOS/RANGE
% -------------------------------------------------------------------------


    if Deimos_check.flag

%      There should be Deimos in the pic
       [Deimos, ~] = cspice_spkezr('402', et, 'MARSIAU', 'none', '499');
       [Deimos_LOS, Deimos_Limb] = Deimos_Pic_model(X_MMX.*units.sfVec, Phobos,...
           Deimos, Xsi, theta, pars, units);
       
       if Deimos_check.n == 1
            Deimos = Deimos_Limb + (bias(4) + bias(5));
       else
            Deimos = Deimos_LOS + [bias(4); bias(5)];
       end
       
       Rm   = [Rm; O(8,8)*ones(Deimos_check.n,1)];    
    end


%--------------------------------------------------------------------------
%               ROVER INTERSATELLITE LINK PHOBOS SURFACE
%--------------------------------------------------------------------------

%   Position of MMX wrt Phobos
    if ISL_check
        MMX_rel = RXsi*Rtheta*pars.MARSIAU2perifocal*(r_MMX(1:3) - r_Phobos);
        ISLink  = norm(MMX_rel - pars.X_rover/units.lsf)  + bias(6);
        Rm      = [Rm; O(9,9)];    
    end
    

%   Ready to exit
    G.meas  = [range_Camb; range_Gold; range_Mad; range_rate_Camb; range_rate_Gold; ...
         range_rate_Mad; lidar; Limb; Deimos; ISLink];
    G.cam   = Y_pix;
    G.cam_Mars = Y_pix_Mars;

    G.meas(isnan(G.meas)) = [];

     
end