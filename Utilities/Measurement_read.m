function [Yobs, R, Stat_ID_Range, Stat_ID_Rate, check_Lidar, check_Limb, check_Deimos,...
    check_ISL, Features_ID, Features_Mars_ID, IDX] = Measurement_read(Measures,O,units)


        IDX = [];
        Y_obs       = [];
        R           = [];
        Stat_ID_Range   = [];
        Stat_ID_Rate    = [];
        Features_ID     = [];
        Features_Mars_ID= [];

        if ~isempty(Measures.Range)
            which   = ~isnan(Measures.Range);
            count   = sum(~isnan(Measures.Range));

            if ~isnan(Measures.Range(1))
                Stat_ID_Range = [Stat_ID_Range; 1];
                IDX = [IDX, 1];
            end
            
            if ~isnan(Measures.Range(2))
                Stat_ID_Range = [Stat_ID_Range; 2];
                IDX = [IDX, 3];
            end
            
            if ~isnan(Measures.Range(3))
                Stat_ID_Range = [Stat_ID_Range; 3];
                IDX = [IDX, 5];
            end

            Ranges  = Measures.Range(which);
            Y_obs   = Ranges./units.lsf;
            R       = ones(count,1)*O(1,1);

        end

        if ~isempty(Measures.Rate)
            which   = ~isnan(Measures.Rate);
            count   = sum(~isnan(Measures.Rate));

            if ~isnan(Measures.Rate(1))
                Stat_ID_Rate = [Stat_ID_Rate; 1];
                IDX = [IDX, 2];
            end
            
            if ~isnan(Measures.Rate(2))
                Stat_ID_Rate = [Stat_ID_Rate; 2];
                IDX = [IDX, 4];
            end
            
            if ~isnan(Measures.Rate(3))
                Stat_ID_Rate = [Stat_ID_Rate; 3];
                IDX = [IDX, 6];
            end
            
            Rates   = Measures.Rate(which);
            Y_obs   = [Y_obs; Rates./units.vsf];
            R       = [R; ones(count,1)*O(2,2)];

        end

        check_Lidar = [];
        
        if ~isempty(Measures.Lidar)
            check_Lidar = 1;
            Lidar   = Measures.Lidar;
            Y_obs   = [Y_obs; Lidar./units.lsf];
            R       = [R; O(7,7)];
            IDX     = [IDX, 7];
        end

        check_Limb = [];

        if ~isempty(Measures.Limb)
            check_Limb = 1;
            Limb    = Measures.Limb;
            Y_obs   = [Y_obs; Limb];
            R       = [R; O(8,8)];
            IDX     = [IDX, 8];
        end

        check_Deimos.flag = 0;

        if ~isempty(Measures.Deimos)
            check_Deimos.flag = 1;
            Deimos  = Measures.Deimos;
            check_Deimos.n = max(size(Deimos));
            Y_obs   = [Y_obs; Deimos];
            R       = [R; O(8,8)*size(Deimos)'];
        end

        check_ISL = [];

        if ~isempty(Measures.ISL)
            check_ISL = 1;
            ISLink  = Measures.ISL;
            Y_obs   = [Y_obs; ISLink./units.lsf];
            R       = [R; O(9,9)];
            IDX     = [IDX, 9];
        end

        Yobs.meas  = Y_obs;

        if ~isempty(Measures.Features)
            Features_ID = Measures.Features(end,:);
            Yobs.cam   = Measures.Features;
        else
            Yobs.cam   = [];
        end

        if ~isempty(Measures.Features_Mars)
            Features_Mars_ID = Measures.Features_Mars(end,:);
            Yobs.cam_Mars   = Measures.Features_Mars;
        else
            Yobs.cam_Mars   = [];
        end

        


end