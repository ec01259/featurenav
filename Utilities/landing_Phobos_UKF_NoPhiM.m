function [value,isterminal,direction] = landing_Phobos_UKF_NoPhiM(~,X,par,units)

    d           = par.d;                    % Dimension of the problem
    nCoeff      = par.nCoeff;               % N. of gravitational parameters
    nBias       = par.nBias;

    size_St     = (d+6+nCoeff+nBias);

    for i = 0:2*size_St-1

%       Spacecraft state vector
        r = X(1+i*size_St:3+i*size_St);     % Pos. vector wrt central body
        
%       Phobos states
        RPh         = X(7+i*size_St);
        theta       = X(9+i*size_St);

        Phobos  = par.perifocal2MARSIAU*[RPh*cos(theta); RPh*sin(theta); 0];
    
        alpha = par.Phobos.alpha/units.lsf;                 % Phobos' largest semi-axis (km) 
        beta  = par.Phobos.beta/units.lsf;                  % Phobos' intermediate semi-axis (km)
        gamma = par.Phobos.gamma/units.lsf;                 % Phobos' smallest semi-axis (km)
        
        x = r - Phobos;

%       Integration is blocked if the spacecraft touch the ellipsoid
        d = x(1)^2/alpha^2 + x(2)^2/beta^2 +...
        x(3)^2/gamma^2 - 1;

        if d<=0
            fprintf('\nOne of the sigma point fell on Phobos...')
            break;
        end

    end

    value = d;
    isterminal = 1;
    direction = 0;

end