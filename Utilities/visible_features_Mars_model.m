function [Y_LOS, Y_pix] = visible_features_Mars_model(MMX, Phobos, Mars, Phi, theta,...
    Phi_M, file_features, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = visible_features_Mars_model(MMX, Phobos, Sun, Phi,...
%           Phi_M, file_features, pars, units)
%
% Identify the visible features and build the observable for analysis
%
% Input:
%     
% data          Date of the observation
% file_features File including the position of the features in the body
%               fixed reference frame
% 
% Output:
% 
% Y_LOS         Vector of features' Line Of Sight and features' ID
% Y_pix         Vector of features' position in the Pic and features' ID
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Geometry

    MMX     = pars.MARSIAU2perifocal*MMX(1:3);
    Phobos  = [pars.MARSIAU2perifocal, zeros(3,3); zeros(3,3), pars.MARSIAU2perifocal]*Phobos;
    Mars     = pars.MARSIAU2perifocal*Mars(1:3);

%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    RLibration  = [cos(Phi), sin(Phi), 0; -sin(Phi), cos(Phi), 0; 0, 0, 1];
%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    Rtheta      = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];
%   Rotation from MarsIAU reference frame to Mars-fixed reference frame
    RLibration_M = [cos(Phi_M), sin(Phi_M), 0; -sin(Phi_M), cos(Phi_M), 0; 0, 0, 1];

%   Light rays direction in the Phobos-fixed reference frame
    r_MSun  = Mars(1:3)/norm(Mars(1:3));

%   Position of MMX wrt Phobos-fixed frame
    r_sb    = MMX(1:3) - Phobos(1:3);
    R_sb    = norm(r_sb);
    r_sb_Ph = RLibration*Rtheta*r_sb;

%   MMX-Phobos direction in the Phobos-fixed reference frame
    I   = r_sb_Ph/R_sb;

%   Features defined in the Phobos-fixed reference frame
    load(file_features);
    Point_Cloud = RLibration_M*Point_Cloud';
    R_ini   = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Point_Cloud  = R_ini*Rtheta'*Point_Cloud;

%   Phobos pole position in the Phobos rotating frame
    Phobos_pole     = [0; 0; -pars.Phobos.gamma];
    Phobos_pole     = R_ini*Phobos_pole;
    Phobos_centre   = zeros(3,1);
    Phobos_centre   = R_ini*Phobos_centre;

    points = zeros(4,size(Point_Cloud,2));
    Y_pix_tot = zeros(2,size(Point_Cloud,2));
    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);

    for n = 1:size(points,2)
        points(:,n) = [Point_Cloud(:,n); n];
        pix_feat    = PM*[points(1:3,n) - Phobos(1:3); 1];
        pix_feat    = pix_feat./repmat(pix_feat(3),3,1);
        Y_pix_tot   = [round(pix_feat(1:2)), Y_pix_tot];
    end


%%  Identification of the features that are in light


%   Scan on the features to distinguish those in light
    point_in_light  = [];
    Y_pix_light     = [];

    for n = 1:size(points,2)
%       If the dot product of the incoming light ray and the feature
%       position vector in the Phobos-fixed ref frame is less than zero,
%       then it is in light
        if dot(r_MSun, points(1:3,n))<0
            point_in_light = [points(:,n), point_in_light];
            pix_feat    = PM*[points(1:3,n) - Phobos(1:3); 1];
            pix_feat    = pix_feat./repmat(pix_feat(3),3,1);
            Y_pix_light = [round(pix_feat(1:2)), Y_pix_light];
        end
    end


%%  Between those features, which ones are inside the FOV?

    visible = [];
    Y_LOS   = [];
    Y_pix   = [];
    PhP = PM*[-Phobos_pole; 1];
    PhP = PhP./repmat(PhP(3),3,1);
    PhC = PM*[-Phobos_centre; 1];
    PhC = PhC./repmat(PhC(3),3,1);
    Mars_centre = - Phobos(1:3);

    for n = 1:size(point_in_light,2)

        candidate = point_in_light(1:3,n) - Phobos(1:3);

        r_sf = r_sb_Ph - candidate;
        
        i_feat = -r_sf/norm(r_sf);
        v      = [0; 0; 1];
        z_cam  = cross(v, I)/norm(cross(v, I));
        y_cam  = cross(z_cam, I)/norm(cross(z_cam, I));

        % Visibility checks
        angle_feat  = acos(dot(I, -i_feat));
        angle_sun   = acos(dot(I, r_MSun));
        feat_centre = candidate - Mars_centre;
        Phobos_range = norm(PhP - PhC);
        Phobos_ellipsoid = @(x,y) (x-PhC(1))^2 + (y-PhC(2))^2 - Phobos_range^2;

        
        % Observable creation
        if (angle_sun>pars.FOV)&&(dot(r_sb_Ph, candidate)<0)&&(dot(feat_centre,i_feat)<0)
            LOS_feat    = [y_cam'; z_cam'] * r_sf/norm(r_sf) + [random('Normal',0, pars.ObsNoise.camera);...
                               random('Normal',0, pars.ObsNoise.camera)];

            pix_feat    = PM*[candidate; 1];
            pix_feat    = pix_feat./repmat(pix_feat(3),3,1);
            pix_feat    = [(pix_feat(1:2)+ [random('Normal',0, pars.ObsNoise.pixel);...
                random('Normal',0, pars.ObsNoise.pixel)]); point_in_light(end,n)];

            if (Phobos_ellipsoid(pix_feat(1), pix_feat(2))>0)&&...
                (all(pix_feat>0))&&(pix_feat(1)<pars.cam.nPixX)&&...
                (pix_feat(2)<pars.cam.nPixY)
                visible = [candidate, visible];
                Y_LOS   = [[LOS_feat; point_in_light(end,n)], Y_LOS];
                Y_pix   = [pix_feat, Y_pix];
                
            else
                % fprintf('\nMars feature scaricata perche dietro Phobos...')
            end

        end
    end


%%  Plot of the Resulting sceene

  % PlotGeometryAndLight_Mars(points(1:3,:) - Phobos(1:3), ...
  % point_in_light(1:3,:) - Phobos(1:3), visible,...
  % r_sb_Ph, r_MSun, pars, units);
  % Plotfeatures_Pic(Y_pix, Y_pix_light, Y_pix_tot, pars);
  % pause(.3)
  % clc
end