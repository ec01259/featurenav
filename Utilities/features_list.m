function features = features_list(Y_feat, Features_ID)

    features = [];
    for k=1:size(Features_ID,2)
        if ~isempty(Y_feat)
            common_feat = Y_feat(1:2, Y_feat(3,:)==Features_ID(k));
            if ~isempty(common_feat)
                features = [features; common_feat];
            end
        end
    end

end