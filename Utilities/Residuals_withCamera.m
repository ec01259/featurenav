function [err, prefit] = Residuals_withCamera(Y_obs, G, X_bar, X_star, ...
    Stat_ID_Range, Stat_ID_Rate, check_Lidar, check_Limb, check_Deimos,...
    check_ISL, Features_ID, Features_Mars_ID, O, IDX, et, pars, units, file_features,...
    file_features_Mars)


%       Preallocation
        err = nan(size(O,1),1);
        prefit = nan(size(O,1),1);

%       Definition of the residuals
        Y_pre  = G(et, X_bar, Stat_ID_Range, Stat_ID_Rate, check_Lidar,...
            check_Limb, check_Deimos, check_ISL, Features_ID, Features_Mars_ID, ...
            pars, units, O, file_features, file_features_Mars)';
        features        = features_list(Y_pre.cam, Features_ID);
        features_Mars   = features_list(Y_pre.cam, Features_Mars_ID);
        Y_pre   = [Y_pre.meas; features; features_Mars];

        Y_err   = G(et, X_star, Stat_ID_Range, Stat_ID_Rate, check_Lidar,...
            check_Limb, check_Deimos, check_ISL, Features_ID, Features_Mars_ID, ...
            pars, units, O, file_features, file_features_Mars)';
        features        = features_list(Y_err.cam, Features_ID);
        features_Mars   = features_list(Y_err.cam, Features_Mars_ID);
        Y_err   = [Y_err.meas; features; features_Mars];

        if isempty(Features_ID)
            err(IDX)    = Y_obs(1:size(IDX,2)) - Y_pre(1:size(IDX,2));
            prefit(IDX) = Y_obs(1:size(IDX,2)) - Y_err(1:size(IDX,2));
        else
            err(IDX)    = Y_obs(1:size(IDX,2)) - Y_err(1:size(IDX,2));
            prefit(IDX) = Y_obs(1:size(IDX,2)) - Y_pre(1:size(IDX,2));
            
            err(8)    = Y_obs(size(IDX,2)+1) - Y_err(size(IDX,2)+1);
            prefit(8) = Y_obs(size(IDX,2)+1) - Y_pre(size(IDX,2)+1);

        end


end