function [P_PhXYZ, P_Xrel, P_PhVXYZ, P_Vrel, X_Phobos] = ...
    Phobos_cartesian_covariance(X_t, P_t, pars)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [P_PhXYZ, P_PhXYZ_MMX, P_rel, P_PhVXYZ, P_Vrel, X_Phobos] = 
%   Phobos_cartesian_covariance(X_t, P_t, pars, units)
%
% Calculation of the Phobos covariance in cartesian coordinates, as well as
% the MMX-Phobos relative position covariance
%
% Input:
%     
% X_t       Analysis state vector at different epochs
% P_t       Analysis state vector's covariance matrix at different epochs
% pars      Structure of parameters
% units     Structure of units
% 
%
% Output:
% 
% P_PhXYZ       Phobos position vector covariance in cartesian coordinates
% P_PhXYZ_MMX   MMX-Phobos position vector correlation coefficients in cartesian coordinates
% P_rel         MMX-Phobos relative position covariance
% P_PhVXYZ      Phobos velocity vector covariance in cartesian coordinates
% P_Vrel        MMX-Phobos relative velocity covariance
% X_Phobos      Phobos state vector in cartesian coordinates
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%   Partials delle cartesian coordinates wrt radial distance and true
%   anomaly

%   Position
    dXdR    = pars.perifocal2MARSIAU*[cos(X_t(9,:)); sin(X_t(9,:)); zeros(size(X_t(9,:)))];
    dXdtheta    = pars.perifocal2MARSIAU*[-sin(X_t(9,:)); cos(X_t(9,:)); zeros(size(X_t(9,:)))].*X_t(7,:);

%   Velocity
    dVdR    = zeros(3,size(X_t(9,:),2));
    dVdR_dot    = zeros(3,size(X_t(9,:),2));
    dVdtheta    = zeros(3,size(X_t(9,:),2));
    dVdtheta_dot    = zeros(3,size(X_t(9,:),2));
    X_Phobos    = zeros(6,size(X_t(9,:),2));

    for i=1:size(X_t(9,:),2)

        X_Phobos(1:3,i) = pars.perifocal2MARSIAU*[cos(X_t(9,i)); sin(X_t(9,i)); zeros(size(X_t(9,i)))]*X_t(7,i);
        X_Phobos(4:6,i) = pars.perifocal2MARSIAU*([cos(X_t(9,i)), sin(X_t(9,i)), 0; -sin(X_t(9,i)), cos(X_t(9,i)), 0; 0, 0, 1]'*[X_t(8,i); 0; 0] +...
            cross([0; 0; X_t(10,i)], [cos(X_t(9,i)); sin(X_t(9,i)); 0])*X_t(7,i));
        dVdR(:,i)   = pars.perifocal2MARSIAU*cross([0; 0; X_t(10,i)], [cos(X_t(9,i)); sin(X_t(9,i)); 0]);
        dVdR_dot(:,i)   = pars.perifocal2MARSIAU*[cos(X_t(9,i)), sin(X_t(9,i)), 0; -sin(X_t(9,i)), cos(X_t(9,i)), 0; 0, 0, 1]'*([1; 0; 0]);
        dVdtheta(:,i)   = pars.perifocal2MARSIAU*([-sin(X_t(9,i)), cos(X_t(9,i)), 0; -cos(X_t(9,i)), -sin(X_t(9,i)), 0; 0, 0, 1]'*([X_t(8,i); 0; 0]) +...
            cross([0; 0; X_t(10,i)], [-sin(X_t(9,i)); cos(X_t(9,i)); 0])*X_t(7,i));
        dVdtheta_dot(:,i)   = pars.perifocal2MARSIAU*cross([0; 0; 1], [cos(X_t(9,i)); sin(X_t(9,i)); 0])*X_t(7,i);
    end


% %   Coomposizione della covariance P_PhXYZ
% 
%     P_PhXYZ     = zeros(3,3,size(X_t(9,:),2));
%     P_PhVXYZ    = zeros(3,3,size(X_t(9,:),2));
% 
%     for i=1:size(X_t(9,:),2)
%         P_PhXYZ(:,:,i) = [dXdR(:,i),dXdtheta(:,i)]*...
%             [P_t(7,7,i), P_t(7,9,i); P_t(9,7,i), P_t(9,9,i)]*...
%             [dXdR(:,i),dXdtheta(:,i)]';
%         P_PhVXYZ(:,:,i) = [dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]*...
%             P_t(7:10,7:10,i)*[dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]';
% 
%     end

% %   Correlation covariance MMX-Phobos
%     P_PhXYZ_MMX     = zeros(3,3,size(X_t(9,:),2));
%     P_MMX_PhXYZ     = zeros(3,3,size(X_t(9,:),2));
%     P_VPhXYZ_VMMX   = zeros(3,3,size(X_t(9,:),2));
%     P_VMMX_VPhXYZ   = zeros(3,3,size(X_t(9,:),2));
% 
%     for i=1:size(X_t(9,:),2)
%         P_PhXYZ_MMX(:,:,i)  = [dXdR(:,i),dXdtheta(:,i)]*[P_t(7,1:3,i); P_t(9,1:3,i)];
%         P_MMX_PhXYZ(:,:,i)  = [P_t(1:3,7,i), P_t(1:3,9,i)]*[dXdR(:,i),dXdtheta(:,i)]';
%         P_VPhXYZ_VMMX(:,:,i)    = [dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]*P_t(7:10,4:6,i);
%         P_VMMX_VPhXYZ(:,:,i)    = P_t(4:6,7:10,i)*[dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]';
%     end
% 
% %   Relative distance covariance between MMX and PHobos
%     P_Xrel = P_PhXYZ + P_t(1:3,1:3,:) - P_PhXYZ_MMX - P_MMX_PhXYZ;
% 
% %   Relative velocity covariance between MMX and PHobos
%     P_Vrel     = P_PhVXYZ + P_t(4:6,4:6,:) - P_VPhXYZ_VMMX - P_VMMX_VPhXYZ;



%   Coomposizione della covariance P_PhXYZ

    P_PhXYZ     = zeros(6,6,size(X_t(9,:),2));

    for i=1:size(X_t(9,:),2)
        P_PhXYZ(:,:,i) = [dXdR(:,i), zeros(3,1), dXdtheta(:,i), zeros(3,1); dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]*...
            P_t(7:10,7:10,i)*[dXdR(:,i), zeros(3,1), dXdtheta(:,i), zeros(3,1); dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]';
    end

    P_PhVXYZ = P_PhXYZ(4:6,4:6,:);

%   Correlation covariance MMX-Phobos
    P_PhXYZ_MMX     = zeros(6,6,size(X_t(9,:),2));
    P_MMX_PhXYZ     = zeros(6,6,size(X_t(9,:),2));

    for i=1:size(X_t(9,:),2)
        P_PhXYZ_MMX(:,:,i)  = [dXdR(:,i), zeros(3,1), dXdtheta(:,i), zeros(3,1); dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]*P_t(7:10,1:6,i);
        P_MMX_PhXYZ(:,:,i) = P_t(1:6,7:10,i)*[dXdR(:,i), zeros(3,1), dXdtheta(:,i), zeros(3,1); dVdR(:,i), dVdR_dot(:,i), dVdtheta(:,i), dVdtheta_dot(:,i)]';

    end

%   Relative distance covariance between MMX and PHobos
    P_rel = P_PhXYZ + P_t(1:6,1:6,:) - P_PhXYZ_MMX - P_MMX_PhXYZ;
    P_Xrel = P_rel(1:3,1:3,:);

%   Relative velocity covariance between MMX and PHobos
    P_Vrel = P_rel(4:6,4:6,:);

end