function [M_int, M_ext, PM] = ProjMatrix(t, pars, ~)

%   Camera intrinsic matrix
    K     = pars.camera_intrinsic;
        
%   Camera's fixed reference frame has the Z versor along the radial
%   direction, and the X and Y on the camera plane.
    k = t/norm(t);
    v = [0; 0; 1];
    r = cross(v,k)/norm(cross(v,k));
    v = cross(k,r)/norm(cross(k,r));

    R_bCam2World    = [r, v, k];
    R_World2bCam    = R_bCam2World';

%   Target's position vector WRT Camera fixed reference frame 
    T_bCam      = -R_World2bCam*t;

%   Target's position WRT image plane
    R_bCam2CV   = [1, 0, 0; 0, -1, 0; 0, 0, -1];
    T_CV        = R_bCam2CV*T_bCam;

%   Una rotazione dell'asteroide corrisponde ad una apparente rotazione 
%   inversa della camera 
    R_World2Ast     = eye(3);
    R_FicbCam2World = R_World2Ast';
    R_World2FicbCam = R_FicbCam2World';
    
%   Ma a questa rotazione va aggiunta la rotazione definita in
%   precendenza nella definizione del camera fixed reference frame 
    R_FicWorld2CV   = R_World2bCam*R_World2FicbCam;

%   E la rotazione per portarla nell'image plane
    R_World2CV      = R_bCam2CV*R_FicWorld2CV;

%   The asteroid instead is fixed
    R_Ast2World     = eye(3);

%   The rotation from the asteroid to the fake image plane is at the end  
    R_FicAst2CV     = R_Ast2World*R_World2CV;

%   Projection matrix
    M_int   = [K', zeros(3,1)];
    M_ext   = [R_FicAst2CV, T_CV; 0, 0, 0, 1];
    PM      = M_int*M_ext;

    
end