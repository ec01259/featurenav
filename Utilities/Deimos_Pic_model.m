function [Deimos_LOS, Deimos_Limb] = Deimos_Pic_model(MMX, Phobos, Deimos, Xsi, theta, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Y = Deimos_Pic(MMX, Phobos, Deimos, Sun, Phi, pars, units)
%
% Identify wether Deimos is in the FOV and how far is it
%
% Input:
%     
% MMX           MMX state vector in the MARSIAU reference frame
% Phobos        Phobos state vector in the MARSIAU reference frame
% Deimos        Deimos state vector in the MARSIAU reference frame
% Sun           Sun state vector in the MARSIAU reference frame
% pars          parameters structure
% units         units structure
% 
% Output:
% 
% Limb_Range    Range derived from Mars limb size
% 
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    MMX     = pars.MARSIAU2perifocal*MMX(1:3);
    Phobos  = [pars.MARSIAU2perifocal, zeros(3,3); zeros(3,3), pars.MARSIAU2perifocal]*Phobos;
    Deimos  = pars.MARSIAU2perifocal*Deimos(1:3);


%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    RLibration  = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
%   Rotation from Phobos radial reference frame to Phobos-fixed reference frame
    Rtheta      = [cos(theta), sin(theta), 0; -sin(theta), cos(theta), 0; 0, 0, 1];

%   Position of MMX wrt Phobos-fixed frame
    r_sb    = MMX(1:3) - Phobos(1:3);
    r_sb_Ph = RLibration*Rtheta*r_sb;


%   Mars pole position in the Phobos rotating frame
    Deimos_center   = Deimos(1:3);
    R_ini           = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Deimos_center   = R_ini*Rtheta'*RLibration'*Deimos_center - Phobos(1:3);
    Deimos_pole     = [0; 15; 0] + Deimos(1:3);
    R_ini           = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    Deimos_pole     = R_ini*Rtheta'*RLibration'*Deimos_pole - Phobos(1:3);

    [~, ~, PM]  = ProjMatrix(-r_sb_Ph, pars, units);

%   Deimos
    DC  = PM*[-Deimos_center; 1];
    DC  = DC./repmat(DC(3),3,1);
    DP  = PM*[-Deimos_pole; 1];
    DP  = DP./repmat(DP(3),3,1);

    Limb_radius = DP(1:2) - DC(1:2);
    Deimos_Limb = sqrt(Limb_radius(1)^2 + Limb_radius(2)^2);
    Deimos_LOS  = DP(1:2);


end