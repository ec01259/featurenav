function P_RAC = Radial_Along_Cross_Covariance(P_XYZ, X, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% P_RAC = Radial_Along_Cross_Covariance(P_XYZ, pars, units)
%
% Calculation of the Phobos covariance in cartesian coordinates, as well as
% the MMX-Phobos relative position covariance
%
% Input:
%     
% P_XYZ     Position vector covariance in cartesian coordinates
% X     State vector wrt which we want the covariances
% pars      Structure of parameters
% units     Structure of units
% 
%
% Output:
% 
% P_RAC     Position vector covariance in radial, along-track and
%           cross-track coordinates
% 
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%   Rotation matrix from the MARSIAU to the Phobos Orbital reference frame
    I = X(1:3)/norm(X(1:3));
    v = X(4:6)/norm(X(4:6));
    K = cross(I,v)/norm(cross(I,v));
    J = cross(K,I);
    
    R_tot = [I, J, K]';

%   Covariance rotation
    P_RAC = R_tot*P_XYZ(1:3,1:3)*R_tot';


end