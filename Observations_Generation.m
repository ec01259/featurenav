clear 
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../computer-vision/'));
    addpath(genpath('../paper/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../paper/MMX_Product/MMX_BSP_Files_GravLib/'));
    addpath(genpath('./Utilities/'))
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('MMX_QSO_049_2x2_826891269_828619269.bsp'));
    % cspice_furnsh(which('MMX_3DQSO_031_009_2x2_826891269_828619269.bsp'));
    % cspice_furnsh(which('MMX_SwingQSO_031_011_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%%  Initial conditions per la analisi

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    Ndays       = 6;
    t           = Ndays*86400;
    date_end    = data+t;

%   Model parameters
    [pars, units] = MMX_DefineNewModelParametersAndUnits;

%   Covariance analysis parameters
    pars.et0      = data;
    [pars, units] = CovarianceAnalysisParameters(pars, units);
    file_features = 'Principal_Phobos_ogni10.mat';
    file_features_Mars = 'Mars_5facets.mat';
     
    
%%  Creazione lista osservabili

    YObs = Observations_Absolute(data, date_end, file_features,...
        file_features_Mars, pars, units);
    
    % save('/Users/ec01259/Documents/pythoncodes/tapyr/YObs');
    save('YObs');