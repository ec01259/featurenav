clear
close all
clc

restoredefaultpath
addpath('./Results/');
addpath('./New Model/');
addpath('../useful_functions/');
addpath(genpath('../mice/'));
addpath(genpath('../generic_kernels/'));
addpath('../paper');
addpath('../paper/MMX_Fcn_CovarianceAnalyses/');
MMX_InitializeSPICE
cspice_furnsh(which('mar097.bsp'));
set(0,'DefaultTextInterpreter','latex');
set(0,'DefaultAxesFontSize', 16);



%% Confronto altezze

name10   = 'QSOLb_4days_oldSetup.mat';
load(name10);
Est10    = Est;
name9   = 'QSOLc_4days_oldSetup.mat';
load(name9);
Est9    = Est;
name8   = '3DQSOLb_4days_oldSetup.mat';
load(name8);
Est8    = Est;
name7   = '3DQSOLc_4days_oldSetup.mat';
load(name7);
Est7    = Est;
name6  = 'SwingQSOLb_4days_oldSetup.mat';
load(name6);
Est6   = Est;

t_obs   = Est6.t(1,:);
coeff   = 0.8;
fine    = round(coeff*size(t_obs,2));
fineCss = round(coeff*size(t_obs,2));

% Con Process noice

[SQRT_X6, SQRT_V6, P_t_C_20_6, P_t_C_22_6, mean6, std6, meanV6, stdV6,...
    meanLib6, stdLib6, meanC206, stdC206, meanC226, stdC226] = Envelopes_Plot(Est6, fine, fineCss);
[SQRT_X7, SQRT_V7, P_t_C_20_7, P_t_C_22_7, mean7, std7, meanV7, stdV7,...
    meanLib7, stdLib7, meanC207, stdC207, meanC227, stdC227] = Envelopes_Plot(Est7, fine, fineCss);
[SQRT_X8, SQRT_V8, P_t_C_20_8, P_t_C_22_8, mean8, std8, meanV8, stdV8,...
    meanLib8, stdLib8, meanC208, stdC208, meanC228, stdC228] = Envelopes_Plot(Est8, fine, fineCss);
[SQRT_X9, SQRT_V9, P_t_C_20_9, P_t_C_22_9, mean9, std9, meanV9, stdV9,...
    meanLib9, stdLib9, meanC209, stdC209, meanC229, stdC229] = Envelopes_Plot(Est9, fine, fineCss);
[SQRT_X10, SQRT_V10, P_t_C_20_10, P_t_C_22_10, mean10, std10, meanV10,...
    stdV10, meanLib10, stdLib10, meanC2010, stdC2010, meanC2210, stdC2210] = Envelopes_Plot(Est10, fine, fineCss);



PlotComparison(Est10, Est9, Est8, Est7, Est6, fine, fineCss);


%% Confronto osservabili sulla M

name5  = 'QSOM_6days_allObs.mat';
load(name5);
Est5   = Est;
name4   = 'QSOM_6days_NoRange.mat';
load(name4);
Est4    = Est;
name3   = 'QSOM_6days_NoRate.mat';
load(name3);
Est3    = Est;
name2   = 'QSOM_6days_NoLidar.mat';
load(name2);
Est2    = Est;
name1   = 'QSOM_6days_NoCam.mat';
load(name1);
Est1    = Est;

t_obs1 = Est1.t(1,:);
fine1    = round(coeff*size(t_obs1,2));
fineCss1 = round(coeff*size(t_obs1,2));

[SQRT_X1, SQRT_V1, P_t_C_20_1, P_t_C_22_1, mean1, std1, meanV1, stdV1,...
    meanLib1, stdLib1, meanC201, stdC201, meanC221, stdC221] = Envelopes_Plot(Est1, fine1, fineCss1);
[SQRT_X2, SQRT_V2, P_t_C_20_2, P_t_C_22_2, mean2, std2, meanV2, stdV2,...
    meanLib2, stdLib2, meanC202, stdC202, meanC222, stdC222] = Envelopes_Plot(Est2, fine1, fineCss1);
[SQRT_X3, SQRT_V3, P_t_C_20_3, P_t_C_22_3, mean3, std3, meanV3, stdV3,...
    meanLib3, stdLib3, meanC203, stdC203, meanC223, stdC223] = Envelopes_Plot(Est3, fine1, fineCss1);
[SQRT_X4, SQRT_V4, P_t_C_20_4, P_t_C_22_4, mean4, std4, meanV4, stdV4,...
    meanLib4, stdLib4, meanC204, stdC204, meanC224, stdC224] = Envelopes_Plot(Est4, fine1, fineCss1);
[SQRT_X5, SQRT_V5, P_t_C_20_5, P_t_C_22_5, mean5, std5, meanV5,...
    stdV5, meanLib5, stdLib5, meanC205, stdC205, meanC225, stdC225] = Envelopes_Plot(Est5, fine1, fineCss1);


 PlotComparison(Est5, Est4, Est3, Est2, Est1, fine, fineCss);


%% Confronto osservabili sulla La

name15   = 'QSOLa_6days_allObs.mat';
load(name15);
Est15   = Est;
name14   = 'QSOLa_6days_NoRange.mat';
load(name14);
Est14    = Est;
name13   = 'QSOLa_6days_NoRate.mat';
load(name13);
Est13    = Est;
name12   = 'QSOLa_6days_NoLidar.mat';
load(name12);
Est12    = Est;
name11   = 'QSOLa_6days_NoCam.mat';
load(name11);
Est11    = Est;


[SQRT_X11, SQRT_V11, P_t_C_20_11, P_t_C_22_11, mean11, std11, meanV11, stdV11,...
    meanLib11, stdLib11, meanC2011, stdC2011, meanC2211, stdC2211] = Envelopes_Plot(Est11, fine1, fineCss1);
[SQRT_X12, SQRT_V12, P_t_C_20_12, P_t_C_22_12, mean12, std12, meanV12, stdV12,...
    meanLib12, stdLib12, meanC2012, stdC2012, meanC2212, stdC2212] = Envelopes_Plot(Est12, fine1, fineCss1);
[SQRT_X13, SQRT_V13, P_t_C_20_13, P_t_C_22_13, mean13, std13, meanV13, stdV13,...
    meanLib13, stdLib13, meanC2013, stdC2013, meanC2213, stdC2213] = Envelopes_Plot(Est13, fine1, fineCss1);
[SQRT_X14, SQRT_V14, P_t_C_20_14, P_t_C_22_14, mean14, std14, meanV14, stdV14,...
    meanLib14, stdLib14, meanC2014, stdC2014, meanC2214, stdC2214] = Envelopes_Plot(Est14, fine1, fineCss1);
[SQRT_X15, SQRT_V15, P_t_C_20_15, P_t_C_22_15, mean15, std15, meanV15,...
    stdV15, meanLib15, stdLib15, meanC2015, stdC2015, meanC2215, stdC2215] = Envelopes_Plot(Est15, fine1, fineCss1);


PlotComparison(Est15, Est14, Est13, Est12, Est11, fine1, fineCss1)

%% Recap Osservabili

%   All observables

x       = [49; 94];
y       = [mean15; mean4];
ypos    = [std15; std4];
yneg    = [std15; std4];
yV      = [meanV15; meanV4];
yposV   = [stdV15; stdV4];
ynegV   = [stdV15; stdV4];

xC20    = [49; 94];
yC20    = [meanC2015; meanC204];
yposC20 = [stdC2015; stdC204];
ynegC20 = [stdC2015; stdC204];
yC22    = [meanC2215; meanC225];
yposC22 = [stdC2215; stdC225];
ynegC22 = [stdC2215; stdC225];

xLib    = [49; 94];
yLib       = [meanLib12; meanLib2].*180/pi;
yposLib    = [stdLib12; stdLib2].*180/pi;
ynegLib    = [stdLib12; stdLib2].*180/pi;

% No range

xNorho       = [49; 94];
yNorho       = [mean14; mean4];
yposNorho    = [std14; std4];
ynegNorho    = [std14; std4];
yVNorho      = [meanV14; meanV2];
yposVNorho   = [stdV14; stdV2];
ynegVNorho   = [stdV14; stdV2];

xC20Norho    = [49; 94];
yC20Norho    = [meanC2014; meanC204];
yposC20Norho = [stdC2014; stdC204];
ynegC20Norho = [stdC2014; stdC204];
yC22Norho    = [meanC2214; meanC224];
yposC22Norho = [stdC2214; stdC224];
ynegC22Norho = [stdC2214; stdC224];

xLib_Norho    = [49; 94];
yLib_Norho       = [meanLib14; meanLib4].*180/pi;
yposLib_Norho    = [stdLib14; stdLib4].*180/pi;
ynegLib_Norho    = [stdLib14; stdLib4].*180/pi;

% No range-rate

xNorate       = [49; 94];
yNorate       = [mean13; mean3];
yposNorate    = [std13; std3];
ynegNorate    = [std13; std3];
yVNorate      = [meanV13; meanV3];
yposVNorate   = [stdV13; stdV3];
ynegVNorate   = [stdV13; stdV3];

xC20Norate    = [49; 94];
yC20Norate    = [meanC2013; meanC203];
yposC20Norate = [stdC2013; stdC203];
ynegC20Norate = [stdC2013; stdC203];
yC22Norate    = [meanC2213; meanC223];
yposC22Norate = [stdC2213; stdC223];
ynegC22Norate = [stdC2213; stdC223];

xLib_Norate    = [49; 94];
yLib_Norate       = [meanLib13; meanLib3].*180/pi;
yposLib_Norate    = [stdLib13; stdLib3].*180/pi;
ynegLib_Norate    = [stdLib13; stdLib3].*180/pi;

% No Lidar

xNo_lidar       = [49; 94];
yNo_lidar       = [mean12; mean2];
yposNo_lidar    = [std12; std2];
ynegNo_lidar    = [std12; std2];
yVNo_lidar      = [meanV12; meanV2];
yposVNo_lidar   = [stdV12; stdV2];
ynegVNo_lidar   = [stdV12; stdV2];

xC20No_lidar    = [49; 94];
yC20No_lidar    = [meanC2013; meanC202];
yposC20No_lidar = [stdC2013; stdC202];
ynegC20No_lidar = [stdC2013; stdC202];
yC22No_lidar    = [meanC2212; meanC222];
yposC22No_lidar = [stdC2212; stdC222];
ynegC22No_lidar = [stdC2212; stdC222];

xLib_NoLidar    = [49; 94];
yLib_NoLidar       = [meanLib12; meanLib2].*180/pi;
yposLib_NoLidar    = [stdLib12; stdLib2].*180/pi;
ynegLib_NoLidar    = [stdLib12; stdLib2].*180/pi;


% No Camera

xNoCam       = [49; 94];
yNoCam       = [mean11; mean1];
yposNoCam    = [std11; std1];
ynegNoCam    = [std11; std1];
yVNoCam      = [meanV11; meanV1];
yposVNoCam   = [stdV11; stdV1];
ynegVNoCam   = [stdV11; stdV1];

xC20NoCam    = [49; 94];
yC20NoCam    = [meanC2011; meanC201];
yposC20NoCam = [stdC2011; stdC201];
ynegC20NoCam = [stdC2011; stdC201];
yC22NoCam    = [meanC2211; meanC222];
yposC22NoCam = [stdC2211; stdC222];
ynegC22NoCam = [stdC2211; stdC222];

xLib_NoCam    = [49; 94];
yLib_NoCam       = [meanLib11; meanLib1].*180/pi;
yposLib_NoCam    = [stdLib11; stdLib1].*180/pi;
ynegLib_NoCam    = [stdLib11; stdLib1].*180/pi;

labels = {'$QSO-La$','$QSO-M$'};
labelsLib = {'$QSO-La$','$QSO-M$'};

figure()
subplot(1,2,1)
errorbar(x,y,yneg,ypos,'-s','LineWidth',2.2,'MarkerSize',5)
hold on
errorbar(xNorho,yNorho,ynegNorho,yposNorho,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNorate,yNorate,ynegNorate,yposNorate,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNo_lidar,yNo_lidar,ynegNo_lidar,yposNo_lidar,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNoCam,yNoCam,ynegNoCam,yposNoCam,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log');
set(gca,'XTick',[49; 94]);
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('Full','No $\rho$','No $\dot{\rho}$','No Lidar','No Cam','Interpreter','latex','Fontsize',26)
ylabel('$[km]$','Fontsize',26)
xlim([3e1,12e1])
grid on
subplot(1,2,2)
errorbar(x,yV,ynegV,yposV,'-s','LineWidth',2.2,'MarkerSize',5)
hold on
errorbar(xNorho,yVNorho,ynegVNorho,yposVNorho,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNorate,yVNorate,ynegVNorate,yposVNorate,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNo_lidar,yVNo_lidar,ynegVNo_lidar,yposVNo_lidar,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNoCam,yVNoCam,ynegVNoCam,yposVNoCam,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log')
set(gca,'XTick',[49; 94]);
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('Full','No $\rho$','No $\dot{\rho}$','No Lidar','No Cam','Interpreter','latex','Fontsize',26)
ylabel('$[km/s]$','Fontsize',26)
xlim([3e1,12e1])

% Css

figure()
subplot(1,2,1)
errorbar(xC20,yC20,ynegC20,yposC20,'-s','LineWidth',2.2,'MarkerSize',5)
hold on
errorbar(xC20Norho,yC20Norho,ynegC20Norho,yposC20Norho,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xC20Norate,yC20Norate,ynegC20Norate,yposC20Norate,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xC20No_lidar,yC20No_lidar,ynegC20No_lidar,yposC20No_lidar,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xC20NoCam,yC20NoCam,ynegC20NoCam,yposC20NoCam,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log')
set(gca,'XTick',[49; 94]);
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('Full','No $\rho$','No $\dot{\rho}$','No Lidar','No Cam','Interpreter','latex','Fontsize',26)
ylabel('$3\sigma_{C_{20}}$','Fontsize',30)
xlim([3e1,12e1])
grid on
subplot(1,2,2)
errorbar(x,yC22,ynegC22,yposC22,'-s','LineWidth',2.2,'MarkerSize',5)
hold on
errorbar(xNorho,yC22Norho,ynegC22Norho,yposC22Norho,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNorate,yC22Norate,ynegC22Norate,yposC22Norate,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNo_lidar,yC22No_lidar,ynegC22No_lidar,yposC22No_lidar,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(xNoCam,yC22NoCam,ynegC22NoCam,yposC22NoCam,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log')
set(gca,'XTick',[49; 94]);
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('Full','No $\rho$','No $\dot{\rho}$','No Lidar','No Cam','Interpreter','latex','Fontsize',26)
ylabel('$3\sigma_{C_{22}}$','Fontsize',30)
xlim([3e1,12e1])

% Libration


figure()
errorbar(xLib,yLib,ynegLib,yposLib,'-s','LineWidth',2.5,'MarkerSize',5)
hold on
errorbar(xLib_Norho,yLib_Norho,ynegLib_Norho,yposLib_Norho,'-s',...
    'LineWidth',1.5,'MarkerSize',5)
errorbar(xLib_Norate,yLib_Norate,ynegLib_Norate,yposLib_Norate,'-s',...
    'LineWidth',1.5,'MarkerSize',5)
errorbar(xLib_NoLidar,yLib_NoLidar,ynegLib_NoLidar,yposLib_NoLidar,'-s',...
    'LineWidth',1.5,'MarkerSize',5)
errorbar(xLib_NoCam,yLib_NoCam,ynegLib_NoCam,yposLib_NoCam,'-s',...
    'LineWidth',1.5,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XScale','log')
set(gca,'XTick',[49; 94]);
set(gca,'XTickLabel',labelsLib,'Fontsize',30);
set(gca,'XTickLabelRotation',45)
ylabel('$[deg]$','Fontsize',30)
legend('Full','No $\rho$','No $\dot{\rho}$','No Lidar','No Cam','Interpreter','latex','Fontsize',30)
xlim([3e1,12e1])
grid on



%% Confronto geometrie sulla Lc

% load('QSOLb_9days_allObs_Ogni2Feat_alpha7e1.mat');
% EstQSO  = Est;
% load('3DQSOLb_9days_allObs_Ogni2Feat_alpha7e1.mat');
% Est3D   = Est;
% load('SwingQSOLb_9days_allObs_Ogni2Feat_alpha7e1.mat');
% EstSwing = Est;

% load('QSOLb_9days_allObs_alpha8e1.mat');
% EstQSO  = Est;
% load('3DQSOLb_9days_allObs_alpha8e1.mat');
% Est3D   = Est;
% load('SwingQSOLb_9days_allObs_alpha8e1.mat');
% EstSwing = Est;

name10   = 'QSOLb_4days_oldSetup.mat';
load(name10);
EstQSO  = Est;
name9   = '3DQSOLc_4days_oldSetup.mat';
load(name9);
Est3D   = Est;
name8   = 'SwingQSOLb_4days_oldSetup.mat';
load(name8);
EstSwing = Est;


coeff = .99;
t_obs = EstQSO.t(1,:);
fine    = round(coeff*size(t_obs,2));
fineCss = round(coeff*size(t_obs,2));


% [par,~] = MMX_DefineNewModelParametersAndUnits;
[par,~] = MMX_DefineNewModelParametersAndUnits_Right;
C_20    = par.SH.Clm(3,1);
C_22    = par.SH.Clm(3,3);

t_obs   = EstQSO.t(1,:);

[SQRT_X_QSO, SQRT_V_QSO, P_t_C_20_QSO, P_t_C_22_QSO, meanQSO, stdQSO, meanVQSO, stdVQSO,...
    meanLibQSO, stdLibQSO,] = Envelopes_Plot(EstQSO, fine, fineCss);
[SQRT_X_3D, SQRT_V_3D, P_t_C_20_3D, P_t_C_22_3D, mean3D, std3D, meanV3D, stdV3D,...
    meanLib3D, stdLib3D] = Envelopes_Plot(Est3D, fine, fineCss);
[SQRT_X_Swing, SQRT_V_Swing, P_t_C_20_Swing, P_t_C_22_Swing, meanSwing, stdSwing, meanVSwing, stdVSwing,...
    meanLibSwing, stdLibSwing] = Envelopes_Plot(EstSwing, fine, fineCss);

C_20_QSO_perc= abs(3*real(sqrt(P_t_C_20_QSO))/C_20);
C_22_QSO_perc= abs(3*real(sqrt(P_t_C_22_QSO))/C_22);

C_20_3D_perc= abs(3*real(sqrt(P_t_C_20_3D))/C_20);
C_22_3D_perc= abs(3*real(sqrt(P_t_C_22_3D))/C_22);

C_20_Swing_perc= abs(3*real(sqrt(P_t_C_20_Swing))/C_20);
C_22_Swing_perc= abs(3*real(sqrt(P_t_C_22_Swing))/C_22);

figure()
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_QSO)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_3D)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_Swing)),'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$3\sigma_{C_{22}}$','Fontsize',30)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_QSO)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_3D)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_Swing)),'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$3\sigma_{C_{20}}$','Fontsize',30)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)

figure()
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,C_20_QSO_perc,'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,C_20_3D_perc,'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,C_20_Swing_perc,'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('%','Fontsize',26)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,C_22_QSO_perc,'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,C_22_3D_perc,'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,C_22_Swing_perc,'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('%','Fontsize',26)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)



figure()
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(13,13,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(13,13,1:end-1)))),'LineWidth',1.2)
title('Libration angle $3\sigma$ envelopes','FontSize',26)
xlabel('Time [hour]','Interpreter','latex','Fontsize',26)
ylabel('$3\sigma_{\Phi_{Ph}}$ [deg]','Interpreter','latex','Fontsize',30)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)

%   MMX Position and velocity

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,SQRT_X_QSO)
hold on;
grid on;
semilogy(t_obs(1:end-1)/3600,SQRT_X_3D)
semilogy(t_obs(1:end-1)/3600,SQRT_X_Swing)
xlabel('time $[hour]$')
ylabel('$[km]$')
title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','FontSize',14)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,SQRT_V_QSO)
hold on;
grid on;
semilogy(t_obs(1:end-1)/3600,SQRT_V_3D)
semilogy(t_obs(1:end-1)/3600,SQRT_V_Swing)
xlabel('time $[hour]$')
ylabel('$[km/s]$')
title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','FontSize',14)

% Phobos states

figure()
subplot(2,4,1)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstQSO.P_t(7,7,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3D.P_t(7,7,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstSwing.P_t(7,7,1:end-1)))))
xlabel('time [hour]')
ylabel('$R_{Ph}$ [km]')
subplot(2,4,2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(9,9,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(9,9,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(9,9,1:end-1)))))
xlabel('time [hour]')
ylabel('$\theta_{Ph}$ [deg]')
subplot(2,4,3)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(11,11,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(11,11,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(11,11,1:end-1)))))
xlabel('time [hour]')
ylabel('$\Phi_{M}$ [deg]')
subplot(2,4,4)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(13,13,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(13,13,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(13,13,1:end-1)))))
xlabel('time [hour]')
ylabel('$\Phi_{Ph}$ [deg]')

subplot(2,4,5)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstQSO.P_t(8,8,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3D.P_t(8,8,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstSwing.P_t(8,8,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{R}_{Ph}$ [km/s]')
subplot(2,4,6)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(10,10,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(10,10,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(10,10,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\theta}_{Ph}$ [deg/s]')
subplot(2,4,7)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(12,12,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(12,12,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(12,12,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\Phi}_{M}$ [deg/s]')
subplot(2,4,8)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(14,14,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(14,14,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(14,14,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\Phi}_{Ph}$ [deg/s]')

