function Results(Est, pars, units)
%==========================================================================
% NewCov_ResultsPlot(Est, YObs_Full, pars)
%
% This function plots the results of the covariance analysis
%
% INPUT: Description Units
%
% Est       - Estimation of:
%               .X, States at final t
%               .x, States' deviation vector at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .err, Post-fit residuals
%               .pre, Pre-fit residuals
%               .y_t, Pre-fit residuals
%               .X_t, States at different t
%               .x_t, States deviation vector at different t
%               .Phi_t, STM at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
% YObs_Full     Vector of observations and time of observations
%
% pars       - Structure containing problem's parameters
%
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 09/2021
%
%==========================================================================

    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

    t_obs   = Est.t;


%--------------------------------------------------------------------------


%   Plot of the pre and post fit residuals
    
    % figure(1)
    % subplot(2,4,1)
    % plot(t_obs/3600,Est.pre(1,:)*units.lsf,'x','Color','b')
    % grid on;
    % hold on;
    % plot(t_obs/3600,Est.pre(3,:)*units.lsf,'x','Color','b')
    % plot(t_obs/3600,Est.pre(5,:)*units.lsf,'x','Color','b')
    % plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    % plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    % xlabel('$[hour]$')
    % ylabel('$[km]$')
    % title('Range Pre-fit residual')
    % subplot(2,4,2)
    % plot(t_obs/3600,Est.pre(2,:)*units.vsf,'x','Color','r')
    % grid on;
    % hold on;
    % plot(t_obs/3600,Est.pre(4,:)*units.vsf,'x','Color','r')
    % plot(t_obs/3600,Est.pre(6,:)*units.vsf,'x','Color','r')
    % plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    % plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    % xlabel('$[hour]$')
    % ylabel('$[km/s]$')    
    % title('RangeRate Pre-fit residual') 
    % subplot(2,4,3)
    % plot(t_obs/3600,Est.pre(7,:)*units.lsf,'x','Color','g')
    % grid on;
    % hold on;
    % plot(t_obs/3600,3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    % plot(t_obs/3600,-3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    % xlabel('$[hour]$')
    % ylabel('$[km]$')
    % title('Lidar Pre-fit residual') 
    % subplot(2,4,4)
    % plot(t_obs/3600,Est.pre(8,:),'x','Color','m')
    % grid on;
    % hold on;
    % plot(t_obs/3600,3*pars.ObsNoise.pixel*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    % plot(t_obs/3600,-3*pars.ObsNoise.pixel*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    % xlabel('$[hour]$')
    % ylabel('$[N. pixel]$')
    % title('Camera Pre-fit residual') 
    % 
    % subplot(2,4,5)
    % plot(t_obs/3600,Est.err(1,:)*units.lsf,'x','Color','b')
    % grid on;
    % hold on;
    % plot(t_obs/3600,Est.err(3,:)*units.lsf,'x','Color','b')
    % plot(t_obs/3600,Est.err(5,:)*units.lsf,'x','Color','b')
    % plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.err(1,:),2)),'.-','Color','b')
    % plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.err(1,:),2)),'.-','Color','b')
    % xlabel('$[hour]$')
    % ylabel('$[km]$')
    % title('Range post-fit residual')
    % subplot(2,4,6)
    % plot(t_obs/3600,Est.err(2,:)*units.vsf,'x','Color','r')
    % grid on;
    % hold on;
    % plot(t_obs/3600,Est.err(4,:)*units.vsf,'x','Color','r')
    % plot(t_obs/3600,Est.err(6,:)*units.vsf,'x','Color','r')
    % plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.err(2,:),2)),'.-','Color','r')
    % plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.err(2,:),2)),'.-','Color','r')
    % xlabel('$[hour]$')
    % ylabel('$[km/s]$')    
    % title('RangeRate post-fit residual') 
    % subplot(2,4,7)
    % plot(t_obs/3600,Est.err(7,:)*units.lsf,'x','Color','g')
    % grid on;
    % hold on;
    % plot(t_obs/3600,3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.err(7,:),2)),'.-','Color','g')
    % plot(t_obs/3600,-3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.err(7,:),2)),'.-','Color','g')
    % xlabel('$[hour]$')
    % ylabel('$[km]$')
    % title('Lidar post-fit residual') 
    % subplot(2,4,8)
    % plot(t_obs/3600,Est.err(8,:),'x','Color','m')
    % grid on;
    % hold on;
    % plot(t_obs/3600,3*pars.ObsNoise.pixel*ones(size(Est.err(8,:),2)),'.-','Color','m')
    % plot(t_obs/3600,-3*pars.ObsNoise.pixel*ones(size(Est.err(8,:),2)),'.-','Color','m')
    % xlabel('$[hour]$')
    % ylabel('$[N. pixel]$')
    % title('Camera post-fit residual') 


    % subplot(2,4,5)
    % histfit([Est.pre(1,:), Est.pre(3,:), Est.pre(5,:)])
    % xlabel('$[km]$')
    % title('Range Pre-fit residual') 
    % subplot(2,4,6)
    % histfit([Est.pre(2,:), Est.pre(4,:), Est.pre(6,:)])
    % xlabel('$[km/s]$')
    % title('Range Rate Pre-fit residual') 
    % subplot(2,4,7)
    % histfit(Est.pre(7,:))
    % xlabel('$[km]$')
    % title('Lidar Pre-fit residual')
    % subplot(2,4,8)
    % histfit(Est.pre(8,:))
    % xlabel('$[rad]$')
    % title('Camera Pre-fit residual')
    
%--------------------------------------------------------------------------

    
%   Square-Root of the MMX covariance error

    sqx = squeeze(Est.P_t(1,1,1:end-1));
    sqy = squeeze(Est.P_t(2,2,1:end-1));
    sqz = squeeze(Est.P_t(3,3,1:end-1));
    SQRT_X = 3*sqrt(sqx + sqy + sqz);
    sqvx = squeeze(Est.P_t(4,4,1:end-1));
    sqvy = squeeze(Est.P_t(5,5,1:end-1));
    sqvz = squeeze(Est.P_t(6,6,1:end-1));
    SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);

    if pars.flag_FILTERING == 1

        MMX_real = cspice_spkezr('-34', pars.et0+t_obs, 'MARSIAU', 'none', 'MARS');
        err = abs(MMX_real - Est.X_t(1:6,:));


    %   3sigma Envelopes and error
        figure()
        subplot(2,3,1)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(1,1,1:end-1)))),'Color','b','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,err(1,1:end-1),'*','Color','b','LineWidth',1)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km]$','FontSize',26)
        title('$x_{MMX}$ $3\sigma$ envelope and error','FontSize',30)
        subplot(2,3,2)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(2,2,1:end-1)))),'Color','r','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,err(2,1:end-1),'*','Color','r','LineWidth',1)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km]$','FontSize',26)
        title('$y_{MMX}$ $3\sigma$ envelope and error','FontSize',30)
        subplot(2,3,3)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(3,3,1:end-1)))),'Color','g','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,err(3,1:end-1),'*','Color','g','LineWidth',1)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km]$','FontSize',26)
        title('$z_{MMX}$ $3\sigma$ envelope and error','FontSize',30)
        subplot(2,3,4)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(4,4,1:end-1)))),'Color','b','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,err(4,1:end-1),'*','Color','b','LineWidth',1)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km/s]$','FontSize',26)
        title('$Vx_{MMX}$ $3\sigma$ envelope and error','FontSize',30)
        subplot(2,3,5)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(5,5,1:end-1)))),'Color','r','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,err(5,1:end-1),'*','Color','r','LineWidth',1)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km/s]$','FontSize',26)
        title('$Vy_{MMX}$ $3\sigma$ envelope and error','FontSize',30)
        subplot(2,3,6)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(6,6,1:end-1)))),'Color','g','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,err(6,1:end-1),'*','Color','g','LineWidth',1)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km/s]$','FontSize',26)
        title('$Vz_{MMX}$ $3\sigma$ envelope and error','FontSize',30)
        

    end

    %   3sigma Envelopes
        figure()
        subplot(1,2,1)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(1,1,1:end-1)))),'Color','b','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(2,2,1:end-1)))),'Color','r','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(3,3,1:end-1)))),'Color','g','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km]$','FontSize',26)
        title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
        legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
        subplot(1,2,2)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(4,4,1:end-1)))),'Color','b','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(5,5,1:end-1)))),'Color','r','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(6,6,1:end-1)))),'Color','g','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
        xlabel('time $[hour]$','FontSize',26)
        ylabel('$[km/s]$','FontSize',26)
        title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
        legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',26)



%--------------------------------------------------------------------------


%   Phobos's states uncertainties evolution
%   Phobos cartesian covaraiance

    [P_PhXYZ, P_rel_MMXPh, P_PhVXYZ,  P_Vrel_MMXPh, X_Phobos] =...
        Phobos_cartesian_covariance(Est.X_t, Est.P_t, pars);
    
    sqx_Ph = squeeze(P_PhXYZ(1,1,1:end-1));
    sqy_Ph = squeeze(P_PhXYZ(2,2,1:end-1));
    sqz_Ph = squeeze(P_PhXYZ(3,3,1:end-1));
    SQRT_X_Ph = 3*sqrt(sqx_Ph + sqy_Ph + sqz_Ph);

    sqx_VPh = squeeze(P_PhVXYZ(1,1,1:end-1));
    sqy_VPh = squeeze(P_PhVXYZ(2,2,1:end-1));
    sqz_VPh = squeeze(P_PhVXYZ(3,3,1:end-1));
    SQRT_V_Ph = 3*sqrt(sqx_VPh + sqy_VPh + sqz_VPh);

    sqx_rel = squeeze(P_rel_MMXPh(1,1,1:end-1));
    sqy_rel = squeeze(P_rel_MMXPh(2,2,1:end-1));
    sqz_rel = squeeze(P_rel_MMXPh(3,3,1:end-1));
    SQRT_X_rel = 3*sqrt(sqx_rel + sqy_rel + sqz_rel);

    sqx_Vrel = squeeze(P_Vrel_MMXPh(1,1,1:end-1));
    sqy_Vrel = squeeze(P_Vrel_MMXPh(2,2,1:end-1));
    sqz_Vrel = squeeze(P_Vrel_MMXPh(3,3,1:end-1));
    SQRT_V_rel = 3*sqrt(sqx_Vrel + sqy_Vrel + sqz_Vrel);


    if pars.flag_FILTERING == 1

        Phobos_real = cspice_spkezr('-401', pars.et0+t_obs, 'MARSIAU', 'none', 'MARS');
        err = abs(Phobos_real - X_Phobos);
    
    
        [Ph, pars]  = Phobos_States_NewModel(pars.et0,pars);
        Ph          = Ph./units.sfVec2;
        St0     = [MMX_real(:,1)./units.sfVec; Ph; pars.I2];
        tspan   = t_obs*units.tsf;
        RelTol  = 1e-13;
        AbsTol  = 1e-16; 
        opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,pars,units));
        [~,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,pars,units),tspan,St0,opt);
    
        
        if size(Est.X_t,1) == 20

            Phi_t   = X(:,13:14)'.*[1; units.tsf];
            err_Phi = abs(Phi_t - Est.X_t(11:12,:));
        
        %   3sigma Envelopes and error
            figure()
            subplot(2,4,1)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(1,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$x_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,2)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(2,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$y_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,3)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(3,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$z_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,4)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(11,11,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(1,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad]$')
            title('$\Phi_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,5)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhVXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(4,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vx_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,6)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(5,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vy_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,7)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(6,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vz_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,8)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(12,12,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(2,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad/s]$')
            title('$\dot{\Phi}_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            
        else

            Phi_t   = X(:,13:14)'.*[1; units.tsf];
            err_Phi = abs(Phi_t - Est.X_t(13:14,:));
        
        %   3sigma Envelopes and error
            figure()
            subplot(2,4,1)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(1,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$x_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,2)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(2,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$y_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,3)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(3,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km]$')
            title('$z_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,4)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(13,13,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(1,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad]$')
            title('$\Phi_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,5)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhVXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(4,1:end-1),'*','Color','b','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vx_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,6)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(5,1:end-1),'*','Color','r','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vy_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,7)
            semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err(6,1:end-1),'*','Color','g','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[km/s]$')
            title('$Vz_{Ph}$ $3\sigma$ envelope and error','FontSize',30)
            subplot(2,4,8)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(14,14,1:end-1)))),'Color','m','LineWidth',1)
            hold on;
            grid on;
            semilogy(t_obs(1:end-1)/3600,err_Phi(2,1:end-1),'*','Color','m','LineWidth',1)
            xlabel('time $[hour]$')
            ylabel('$[rad/s]$')
            title('$\dot{\Phi}_{Ph}$ $3\sigma$ envelope and error','FontSize',30)

        end

     else
    
         if size(Est.X_t,1) == 20

         %  3sigma Envelopes
            figure()
            subplot(2,3,1)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$R_{Ph}$ [km]')
            subplot(2,3,2)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(9,9,1:end-1)))),'Color','r','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\theta_{Ph}$ [rad]')
            subplot(2,3,3)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(11,11,1:end-1)))),'Color','m','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\Phi_{Ph}$ [rad]')
            
            subplot(2,3,4)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(8,8,1:end-1)))),'Color','b','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{R}_{Ph}$ [km/s]')
            subplot(2,3,5)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(10,10,1:end-1)))),'Color','r','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{\theta}_{Ph}$ [rad/s]')
            subplot(2,3,6)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(12,12,1:end-1)))),'Color','m','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{\Phi}_{Ph}$ [rad/s]')

         else


        %   3sigma Envelopes
            figure()
            subplot(2,4,1)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$R_{Ph}$ [km]')
            subplot(2,4,2)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(9,9,1:end-1)))),'Color','r','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\theta_{Ph}$ [rad]')
            subplot(2,4,3)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(11,11,1:end-1)))),'Color','g','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\Phi_{M}$ [rad]')
            subplot(2,4,4)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(13,13,1:end-1)))),'Color','m','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\Phi_{Ph}$ [rad]')
            
            subplot(2,4,5)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(8,8,1:end-1)))),'Color','b','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{R}_{Ph}$ [km/s]')
            subplot(2,4,6)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(10,10,1:end-1)))),'Color','r','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{\theta}_{Ph}$ [rad/s]')
            subplot(2,4,7)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(12,12,1:end-1)))),'Color','g','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{\Phi}_{M}$ [rad/s]')
            subplot(2,4,8)
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(14,14,1:end-1)))),'Color','m','LineWidth',1)
            grid on
            xlabel('time [hour]')
            ylabel('$\dot{\Phi}_{Ph}$ [rad/s]')


            % figure()
            % subplot(2,2,1)
            % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
            % grid on
            % xlabel('time [hour]')
            % ylabel('$R_{Ph}$ [km]') 
            % subplot(2,2,2)
            % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(13,13,1:end-1)))),'Color','m','LineWidth',1)
            % grid on
            % xlabel('time [hour]')
            % ylabel('$\Phi_{Ph}$ [rad]')
            % subplot(2,2,3)
            % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(8,8,1:end-1)))),'Color','b','LineWidth',1)
            % grid on
            % xlabel('time [hour]')
            % ylabel('$\dot{R}_{Ph}$ [km/s]')
            % subplot(2,2,4)
            % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(14,14,1:end-1)))),'Color','m','LineWidth',1)
            % grid on
            % xlabel('time [hour]')
            % ylabel('$\dot{\Phi}_{Ph}$ [rad/s]')
        
         end

     end


 %--------------------------------------------------------------------------

 
    % figure()
    % subplot(1,2,1)
    % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
    % hold on;
    % grid on;
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,SQRT_X_Ph,'Color','k','LineWidth',2)
    % xlabel('time $[hour]$','FontSize',30)
    % ylabel('$[km]$','FontSize',30)
    % title('Phobos position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    % legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
    % subplot(1,2,2)
    % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhVXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
    % hold on;
    % grid on;
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,SQRT_V_Ph,'Color','k','LineWidth',2)
    % xlabel('time $[hour]$','FontSize',30)
    % ylabel('$[km]$','FontSize',30)
    % title('Phobos velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    % legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',26)


    % figure()
    % subplot(1,3,1)
    % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
    % hold on;
    % grid on;
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,SQRT_X_Ph,'Color','k','LineWidth',2)
    % xlabel('time $[hour]$','FontSize',30)
    % ylabel('$[km]$','FontSize',30)
    % title('Phobos position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    % legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
    % subplot(1,3,2)
    % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhVXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
    % hold on;
    % grid on;
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,SQRT_V_Ph,'Color','k','LineWidth',2)
    % xlabel('time $[hour]$','FontSize',30)
    % ylabel('$[km]$','FontSize',30)
    % title('Phobos velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    % legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',26)
    % subplot(1,3,3)
    % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_rel_MMXPh(1,1,1:end-1)))),'Color','b','LineWidth',1)
    % hold on;
    % grid on;
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_MMXPh(2,2,1:end-1)))),'Color','r','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_MMXPh(3,3,1:end-1)))),'Color','g','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,SQRT_X_rel,'Color','k','LineWidth',2)
    % xlabel('time $[hour]$','FontSize',30)
    % ylabel('$[km]$','FontSize',30)
    % title('MMX-Phobos relative distance $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    % legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
    

    figure()
    subplot(2,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X_Ph,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('Phobos position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
    subplot(2,2,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_PhVXYZ(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_PhVXYZ(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V_Ph,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('Phobos velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',26)
    subplot(2,2,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_rel_MMXPh(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_MMXPh(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_MMXPh(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X_rel,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('MMX-Phobos relative distance $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
    subplot(2,2,4)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_Vrel_MMXPh(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Vrel_MMXPh(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Vrel_MMXPh(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V_rel,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    title('MMX-Phobos relative velocity $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',26)
 
    % figure()
    % subplot(1,2,1)
    % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_rel_MMXPh(1,1,1:end-1)))),'Color','b','LineWidth',1)
    % hold on;
    % grid on;
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_MMXPh(2,2,1:end-1)))),'Color','r','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_rel_MMXPh(3,3,1:end-1)))),'Color','g','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,SQRT_X_rel,'Color','k','LineWidth',2)
    % xlabel('time $[hour]$')
    % ylabel('$[km]$')
    % title('MMX-Phobos relative distance $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    % legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',26)
    % subplot(1,2,2)
    % semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_Vrel_MMXPh(1,1,1:end-1)))),'Color','b','LineWidth',1)
    % hold on;
    % grid on;
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Vrel_MMXPh(2,2,1:end-1)))),'Color','r','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Vrel_MMXPh(3,3,1:end-1)))),'Color','g','LineWidth',1)
    % semilogy(t_obs(1:end-1)/3600,SQRT_V_rel,'Color','k','LineWidth',2)
    % xlabel('time $[hour]$')
    % ylabel('$[km/s]$')
    % title('MMX-Phobos relative velocity $3\sigma$ envelopes','Interpreter','latex','FontSize',30)
    % legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',26)
 

%--------------------------------------------------------------------------



%   Covariances in the Phobos Radial-Along-Crross track directions

    P_Ph_RAC    = zeros(3,3,size(Est.X_t(9,:),2));
    P_MMX_RAC   = zeros(3,3,size(Est.X_t(9,:),2));
    P_Rel_RAC   = zeros(3,3,size(Est.X_t(9,:),2));
    
    for i = 1:size(Est.X_t(1,:),2)

        P_Ph_RAC(:,:,i)  = Radial_Along_Cross_Covariance(P_PhXYZ(:,:,i), X_Phobos(:,i), pars, units);
        P_MMX_RAC(:,:,i) = Radial_Along_Cross_Covariance(Est.P_t(1:3,1:3,i), Est.X_t(1:6,i), pars, units);
        P_Rel_RAC(:,:,i) = Radial_Along_Cross_Covariance(P_rel_MMXPh(:,:,i), Est.X_t(1:6,i)-X_Phobos(:,i), pars, units);

    end

    figure()
    subplot(1,3,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_Ph_RAC(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Ph_RAC(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Ph_RAC(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X_Ph,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('Phobos position $3\sigma$ in RAC directions wrt Phobos position','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{radial}$','$3\sigma_{along}$','$3\sigma_{cross}$','$3 RMS$','Interpreter','latex','FontSize',26)
    subplot(1,3,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_MMX_RAC(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_MMX_RAC(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_MMX_RAC(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('MMX position $3\sigma$ in RAC directions wrt Phobos position','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{radial}$','$3\sigma_{along}$','$3\sigma_{cross}$','$3 RMS$','Interpreter','latex','FontSize',26)
    subplot(1,3,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_Rel_RAC(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Rel_RAC(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_Rel_RAC(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X_rel,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('MMX position $3\sigma$ in RAC directions wrt Phobos position','Interpreter','latex','FontSize',30)
    legend('$3\sigma_{radial}$','$3\sigma_{along}$','$3\sigma_{cross}$','$3 RMS$','Interpreter','latex','FontSize',26)
    


%--------------------------------------------------------------------------


%   Harmonics 3sigma Envelopes

%     place0 = size(Est.X,1)-pars.nCoeff-pars.nBias;

if size(Est.X_t,1) == 20
    corr_label = {'$x$','$y$','$z$','$\dot{x}$','$\dot{y}$','$\dot{z}$',...
        '$R_{Ph}$','$\dot{R}_{Ph}$','$\theta_{Ph}$','$\dot{\theta}_{Ph}$',...
        '$\Phi_{Ph}$','$\dot{\Phi}_{Ph}$'};
else
    corr_label = {'$x$','$y$','$z$','$\dot{x}$','$\dot{y}$','$\dot{z}$',...
        '$R_{Ph}$','$\dot{R}_{Ph}$','$\theta_{Ph}$','$\dot{\theta}_{Ph}$',...
        '$\Phi_{M}$','$\dot{\Phi}_{M}$','$\Phi_{Ph}$','$\dot{\Phi}_{Ph}$'};
end

%   Moments of inertia
%     figure()
%     subplot(1,3,1)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(15,15,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
    corr_label{end+1} = ['$I_{PhX}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhX}$','Interpreter','latex','FontSize',26)
%     subplot(1,3,2)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(16,16,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
    corr_label{end+1} = ['$I_{PhY}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhY}$','Interpreter','latex','FontSize',26)
%     subplot(1,3,3)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(16,16,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
    corr_label{end+1} = ['$I_{PhZ}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhZ}$','Interpreter','latex','FontSize',26)

 
%--------------------------------------------------------------------------


%   Biases

    place0 = size(Est.X,1)-pars.nBias;
    corr_label(end+1) = {'$\rho$'};
    corr_label(end+1) = {'$\dot{\rho}$'};
    corr_label(end+1) = {'$LIDAR_b$'};

    figure()
    subplot(1,pars.nBias,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+1,place0+1,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\rho$'),'Color','b');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    subplot(1,pars.nBias,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+2,place0+2,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\dot{\rho}$'),'Color','r');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    subplot(1,pars.nBias,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+3,place0+3,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$LIDAR_b$'),'Color','g');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')

    subplot(1,pars.nBias,4)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+4,place0+4,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$Features_x$'),'Color','m');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[pix]$')

    subplot(1,pars.nBias,5)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+5,place0+5,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$Features_y$'),'Color','k');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[pix]$')

    subplot(1,pars.nBias,6)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+6,place0+6,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$Inter-Satellite Link$'),'Color','c');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')




%--------------------------------------------------------------------------



%   Harmonics Coefficients


if size(Est.X_t,1) == 20

        P_t_I2x  = squeeze(Est.P_t(13,13,1:end-1));
        P_t_I2y  = squeeze(Est.P_t(14,14,1:end-1));
        P_t_I2z  = squeeze(Est.P_t(15,15,1:end-1));
        Cov_I2xI2y = squeeze(Est.P_t(13,14,1:end-1));
        Cov_I2yI2z = squeeze(Est.P_t(14,15,1:end-1));
        Cov_I2xI2z = squeeze(Est.P_t(13,15,1:end-1));
    
        P_t_C_22 = .25*(P_t_I2y + P_t_I2x - 2*Cov_I2xI2y);
        P_t_C_20 = .5*(4*P_t_I2z + P_t_I2y + P_t_I2x - 4*Cov_I2xI2z - 4*Cov_I2yI2z + 2*Cov_I2xI2y);
    
       
    if pars.flag_FILTERING == 1
    
        C_22_t  = .25*(Est.X_t(14,:) - Est.X_t(13,:));
        C_20_t  = -.5*(2*Est.X_t(15,:) - Est.X_t(14,:) - Est.X_t(13,:));
    
        err_Css = abs([pars.SH.Clm(3,1)*pars.SH.Norm(3,1); pars.SH.Clm(3,3)*pars.SH.Norm(3,3)] - [C_20_t; C_22_t]);
    
        figure()
        subplot(1,2,2)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20)),'LineWidth',1,'Color','b');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,err_Css(1,1:end-1),'*','Color','b');
        xlabel('time $[hour]$')
        legend('$C_{22}$','Interpreter','latex','FontSize',26)
        subplot(1,2,1)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22)),'LineWidth',1,'Color','r');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,err_Css(2,1:end-1),'*','Color','r');
        xlabel('time $[hour]$')
        legend('$C_{20}$','Interpreter','latex','FontSize',26)
    
    
        figure()
        subplot(1,3,1)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(13,13,1:end-1)))),'LineWidth',1,'Color','b');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,abs(Est.X_t(13,1:end-1)-pars.I2(1)),'*','Color','b');
        xlabel('time $[hour]$')
        legend('$I_{PhX}$','Interpreter','latex','FontSize',26)
        subplot(1,3,2)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(14,14,1:end-1)))),'LineWidth',1,'Color','r');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,abs(Est.X_t(14,1:end-1)-pars.I2(2)),'*','Color','r');
        xlabel('time $[hour]$')
        legend('$I_{PhY}$','Interpreter','latex','FontSize',26)
        subplot(1,3,3)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(15,15,1:end-1)))),'LineWidth',1,'Color','g');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,abs(Est.X_t(15,1:end-1)-pars.I2(3)),'*','Color','g');
        xlabel('time $[hour]$')
        legend('$I_{PhZ}$','Interpreter','latex','FontSize',26)
    
    else
    
        figure()
        subplot(1,2,2)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20)),'LineWidth',1,'Color','b');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        legend('$C_{22}$','Interpreter','latex','FontSize',26)
        subplot(1,2,1)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22)),'LineWidth',1,'Color','r');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        legend('$C_{20}$','Interpreter','latex','FontSize',26)
    
        
    end

    
else

        P_t_I2x  = squeeze(Est.P_t(15,15,1:end-1));
        P_t_I2y  = squeeze(Est.P_t(16,16,1:end-1));
        P_t_I2z  = squeeze(Est.P_t(17,17,1:end-1));
        Cov_I2xI2y = squeeze(Est.P_t(15,16,1:end-1));
        Cov_I2yI2z = squeeze(Est.P_t(16,17,1:end-1));
        Cov_I2xI2z = squeeze(Est.P_t(15,17,1:end-1));
    
        P_t_C_22 = .25*(P_t_I2y + P_t_I2x - 2*Cov_I2xI2y);
        P_t_C_20 = .5*(4*P_t_I2z + P_t_I2y + P_t_I2x - 4*Cov_I2xI2z - 4*Cov_I2yI2z + 2*Cov_I2xI2y);
    
       
    if pars.flag_FILTERING == 1
    
        C_22_t  = .25*(Est.X_t(16,:) - Est.X_t(15,:));
        C_20_t  = -.5*(2*Est.X_t(17,:) - Est.X_t(16,:) - Est.X_t(15,:));
    
        err_Css = abs([pars.SH.Clm(3,1)*pars.SH.Norm(3,1); pars.SH.Clm(3,3)*pars.SH.Norm(3,3)] - [C_20_t; C_22_t]);
    
        figure()
        subplot(1,2,2)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20)),'LineWidth',1,'Color','b');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,err_Css(1,1:end-1),'*','Color','b');
        xlabel('time $[hour]$')
        legend('$C_{22}$','Interpreter','latex','FontSize',26)
        subplot(1,2,1)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22)),'LineWidth',1,'Color','r');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,err_Css(2,1:end-1),'*','Color','r');
        xlabel('time $[hour]$')
        legend('$C_{20}$','Interpreter','latex','FontSize',26)
    
    
        figure()
        subplot(1,3,1)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(15,15,1:end-1)))),'LineWidth',1,'Color','b');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,abs(Est.X_t(15,1:end-1)-pars.I2(1)),'*','Color','b');
        xlabel('time $[hour]$')
        legend('$I_{PhX}$','Interpreter','latex','FontSize',26)
        subplot(1,3,2)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(16,16,1:end-1)))),'LineWidth',1,'Color','r');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,abs(Est.X_t(16,1:end-1)-pars.I2(2)),'*','Color','r');
        xlabel('time $[hour]$')
        legend('$I_{PhY}$','Interpreter','latex','FontSize',26)
        subplot(1,3,3)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(17,17,1:end-1)))),'LineWidth',1,'Color','g');
        grid on;
        hold on;
        semilogy(t_obs(1:end-1)/3600,abs(Est.X_t(17,1:end-1)-pars.I2(3)),'*','Color','g');
        xlabel('time $[hour]$')
        legend('$I_{PhZ}$','Interpreter','latex','FontSize',26)
    
    else
    
        figure()
        subplot(1,2,2)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20)),'LineWidth',1,'Color','b');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        legend('$C_{22}$','Interpreter','latex','FontSize',26)
        subplot(1,2,1)
        semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22)),'LineWidth',1,'Color','r');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        legend('$C_{20}$','Interpreter','latex','FontSize',26)
    
        % figure()
        % subplot(1,2,1)
        % semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20)),'LineWidth',1,'Color','r');
        % grid on;
        % hold on;
        % xlabel('time $[hour]$')
        % legend('$C_{20}$','Interpreter','latex','FontSize',26)
        % subplot(1,2,2)
        % semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22)),'LineWidth',1,'Color','b');
        % grid on
        % hold on;
        % xlabel('time $[hour]$')
        % legend('$C_{22}$','Interpreter','latex','FontSize',26)
        
    end

end

%--------------------------------------------------------------------------


%   Correlations coefficients

    % [~,corr] = readCovMatrix(real(Est.P));
    % 
    % figure();
    % imagesc(real(corr))
    % title('Correlation Coefficients','FontSize',16);
    % set(gca,'FontSize',16);
    % colormap(hot);
    % colorbar;
    % set(gca,'TickLabelInterpreter','latex');
    % set(gca,'XTick',(1:size(Est.X,1)));
    % set(gca,'XTickLabel',corr_label);
    % set(gca,'YTick',(1:size(Est.X,1)));
    % set(gca,'YTickLabel',corr_label);
    % axis square;
    % freezeColors;


end