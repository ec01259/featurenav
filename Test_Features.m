clear 
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../computer-vision/'));
    addpath(genpath('../paper/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('../paper/MMX_Fcn_CovarianceAnalyses/'));
    addpath(genpath('../paper/MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('MMX_QSO_049_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%%  Initial conditions per la analisi

%   Time of the analysis
    data        = '2026-03-21 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    Ndays       = 1;
    t           = Ndays*86400;
    date_end    = data+t;

%   Model parameters
    [pars, units] = MMX_DefineNewModelParametersAndUnits;

%   Covariance analysis parameters
    pars.et0      = data;
    [pars, units] = CovarianceAnalysisParameters(pars, units);
    file_features = 'Principal_Phobos_Ogni10.mat';
    
    for i=data:100:date_end

%   Positin of my target bodies at that date
        Sun     = cspice_spkezr('10', i, 'J2000', 'none', 'MARS');
        MMX     = cspice_spkezr('-34', i, 'J2000', 'none', 'MARS');
        Phobos  = cspice_spkezr('-401', i, 'J2000', 'none', 'MARS');
        Phi     = 0;

        [Y_LOS, Y_pix] = visible_features(MMX, Phobos, Sun, Phi, file_features, pars, units);

    end