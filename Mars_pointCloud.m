clear
close all
clc 


[X,Y,Z] = sphere(20);
radius  = 3.3895;
X = X*radius;
Y = Y*radius;
Z = Z*radius;

Point_Cloud   = [X(1,1),Y(1,1),Z(1,1),1];
count       = 1;

for i = 1:size(X,1)
    for j = 1:size(X,2)
        vertex  = [X(i,j),Y(i,j),Z(i,j)];
        if ~ismember(vertex,Point_Cloud(:,1:3),"rows")
            Point_Cloud   = [Point_Cloud; vertex, count];
            count = count+1; 
        end
    end
end

figure()
plot3(Point_Cloud(:,1),Point_Cloud(:,2),Point_Cloud(:,3), 'o')

save('Mars_20facets.mat',"Point_Cloud")